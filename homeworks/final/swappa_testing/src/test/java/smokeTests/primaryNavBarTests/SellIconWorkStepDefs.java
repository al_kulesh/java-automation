package smokeTests.primaryNavBarTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.primaryNavBar.PrimaryNavBarSellIconService;
import services.pageServices.SellOnSwappaPageService;

//Тест-кейс №11. Работоспособность иконки "Sell" и кнопки "Start Selling"
public class SellIconWorkStepDefs {
    private final PrimaryNavBarSellIconService primaryNavBarSellIconService;
    private final SellOnSwappaPageService sellOnSwappaPageService;

    public SellIconWorkStepDefs(
            PrimaryNavBarSellIconService primaryNavBarSellIconService,
            SellOnSwappaPageService sellOnSwappaPageService
    ) {
        this.primaryNavBarSellIconService = primaryNavBarSellIconService;
        this.sellOnSwappaPageService = sellOnSwappaPageService;
    }

    @Given("home page is open: click \"Sell\" icon")
    public void clickSellIcon() {
        primaryNavBarSellIconService.clickSellIcon();
    }

    @Then("slide sell is open: \"slide_sell\" element")
    public void slideSellIcon() {
        SoftAssertions softAssertions = new SoftAssertions();
        primaryNavBarSellIconService.slideSellIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("click \"Start Selling\" button")
    public void startSellingButtonClick() {
        primaryNavBarSellIconService.startSellButtonClick();
    }

    @Then("sell page is open: \"Search to sell\" search-bar")
    public void sellOnSwappaPageIsShown() {
        SoftAssertions softAssertion = new SoftAssertions();
        sellOnSwappaPageService.searchBarisDisplayed(softAssertion);
        softAssertion.assertAll();
    }

}
