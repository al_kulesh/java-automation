package smokeTests.primaryNavBarTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.pageServices.RegisterPageService;
import services.navBarServices.primaryNavBar.PrimaryNavBarLoginIconService;

//Тест-кейс №14. Работоспособность иконки "Register", кнопки "Register", заполняемость полей  "E-mail", "First Name", "Last Name", Password"
public class RegisterPageStepDefs {
    private final PrimaryNavBarLoginIconService loginIconService;
    private final RegisterPageService registerPageService;

    public RegisterPageStepDefs(
            PrimaryNavBarLoginIconService loginIconService,
            RegisterPageService registerPageService
    ) {
        this.loginIconService = loginIconService;
        this.registerPageService = registerPageService;
    }

    @And("click \"Register\" button")
    public void registerButtonClick() {
        loginIconService.registerButtonClick();
    }

    @Then("register page is open: \"Email\" input, \"First Name\" input, \"Last Name\" input, \"Password\" input")
    public void registerPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        registerPageService.registerPageIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("type text in \"Email\" input, type text in \"First Name\" input,  type text in \"Last Name\" input,  type text in \"Password\" input")
    public void fillRegisterForm() {
        registerPageService.fillRegisterForm("al.kulesh.box@mail.ru", "Alexander", "Kulesh", "111111");
    }
}
