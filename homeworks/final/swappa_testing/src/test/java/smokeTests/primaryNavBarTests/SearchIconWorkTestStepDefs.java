package smokeTests.primaryNavBarTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.primaryNavBar.PrimaryNavBarSearchIconService;
import services.pageServices.OneModelCatalogPageService;

//Тест-кейс №3. Работоспособность иконки "Search" и search-bar
public class SearchIconWorkTestStepDefs {
    private final OneModelCatalogPageService oneModelCatalogPageService;
    private final PrimaryNavBarSearchIconService primaryNavBarSearchIconService;

    public SearchIconWorkTestStepDefs(
            OneModelCatalogPageService oneModelCatalogPageService,
            PrimaryNavBarSearchIconService primaryNavBarSearchIconService
    ) {
        this.oneModelCatalogPageService = oneModelCatalogPageService;
        this.primaryNavBarSearchIconService = primaryNavBarSearchIconService;
    }

    @Given("home page is open: click \"Search\" icon click")
    public void clickSearchIcon() {
        primaryNavBarSearchIconService.clickSearchIcon();
    }

    @Then("slide search is open: \"slide-search\" element")
    public void slideSearchIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        primaryNavBarSearchIconService.slideSearchIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("type in \"Search bar\" input, click \"Search\" button")
    public void searchBarWork() {
        primaryNavBarSearchIconService.searchBarWork();
    }


    @Then("search result page is open: \"Search result label\" label")
    public void searchedPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        oneModelCatalogPageService.navBarSearchResultPageIsShown(softAssertions);
        softAssertions.assertAll();
    }
}
