package smokeTests.primaryNavBarTests;

import enums.Subject;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.primaryNavBar.PrimaryNavBarHelpIconService;
import services.pageServices.HelpPageService;

//Тест-кейс №12. Работоспособность иконки и кнопки "Submit Help Request"
public class HelpWorkStepDefs {
    private final PrimaryNavBarHelpIconService primaryNavBarHelpIconService;
    private final HelpPageService helpPageService;

    public HelpWorkStepDefs(
            PrimaryNavBarHelpIconService primaryNavBarHelpIconService,
            HelpPageService helpPageService
    ) {
        this.primaryNavBarHelpIconService = primaryNavBarHelpIconService;
        this.helpPageService = helpPageService;
    }

    @Given("home page is open: click \"Help\" icon")
    public void clickHelpIcon() {
        primaryNavBarHelpIconService.helpButtonClick();
    }

    @Then("slide help is open: \"slide_help\" element")
    public void slideSearchIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        primaryNavBarHelpIconService.slideSellIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("click \"Submit Help Request\" button")
    public void clickSubmitHelpRequestButton() {
        primaryNavBarHelpIconService.clickSubmitHelpRequestButton();
    }

    @Then("help page is open: \"Email\" input, \"Subject\" selector, \"Message\" input, \"Link\" input, \"Captcha\" checkbox")
    public void helpPageIsShown() {
        SoftAssertions softAssertion = new SoftAssertions();
        helpPageService.helpPageIsShown(softAssertion);
        softAssertion.assertAll();
    }

    @And("type text in \"Email\" input, choose option from \"Subject\" selector, type text in \"Message\" input, check \"Captcha\" checkbox")
    public void fillHelpForm() {
        helpPageService.helpFormFill("mail", Subject.OTHER, "some text", "https://swappa.com");
    }
}
