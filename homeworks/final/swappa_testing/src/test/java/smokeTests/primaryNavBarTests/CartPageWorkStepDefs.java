package smokeTests.primaryNavBarTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.primaryNavBar.PrimaryNavBarCartIconService;

//Тест-кейс №15. Работоспособность иконки "Сart", кнопки "View Cart"
public class CartPageWorkStepDefs {
    private final PrimaryNavBarCartIconService primaryNavBarCartIconService;

    public CartPageWorkStepDefs(PrimaryNavBarCartIconService primaryNavBarCartIconService) {
        this.primaryNavBarCartIconService = primaryNavBarCartIconService;
    }

    @Given("home page is open: click \"Cart\" icon")
    public void cartIconClick() {
        primaryNavBarCartIconService.cartIconClick();
    }

    @Then("slide cart is open: \"slide_cart\" element")
    public void slideSellIsShown(){
        SoftAssertions softAssertions = new SoftAssertions();
        primaryNavBarCartIconService.slideCartIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("click \"Find a good deal\" button")
    public void startSellButtonClick() {
        primaryNavBarCartIconService.viewCartButtonClick();
    }
}
