package smokeTests.primaryNavBarTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.primaryNavBar.PrimaryNavBarSwappaLogoService;
import services.pageServices.PageService;

//Тест-кейс №16. Работоспособность логитипа "Swappa"
public class SwappaLogoStepDefs {
    private final PageService pageService;
    private final PrimaryNavBarSwappaLogoService primaryNavBarSwappaLogoService;

    public SwappaLogoStepDefs(
            PageService pageService,
            PrimaryNavBarSwappaLogoService primaryNavBarSwappaLogoService
    ) {
        this.pageService = pageService;
        this.primaryNavBarSwappaLogoService = primaryNavBarSwappaLogoService;
    }

    @Given("home page is open click random dropdown link : \"Phones\" dropdown link, \"Computers\" dropdown link, \"Tablets\" dropdown link")
    public void openRandomPage() {
        pageService.openRandomPage();
    }

    @Then("random page is open: \"Breadcrumbs\" baseElement , \"Announcements\" baseElement, \"Section_billboard\" baseElement, \"Section_trailer\" baseElement")
    public void randomPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        pageService.isPageShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("click \"Swappa\" logo")
    public void swappaLogoClick() {
        primaryNavBarSwappaLogoService.swappaLogoClick();
    }

}
