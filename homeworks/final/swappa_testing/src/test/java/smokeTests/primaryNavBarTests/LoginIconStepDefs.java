package smokeTests.primaryNavBarTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.primaryNavBar.PrimaryNavBarLoginIconService;

//Тест-кейс №13. Работоспособность иконки "Login" и заполняемость полей "Email Address", "Password"
public class LoginIconStepDefs {
    private final PrimaryNavBarLoginIconService loginIconService;

    public LoginIconStepDefs(PrimaryNavBarLoginIconService loginIconService) {
        this.loginIconService = loginIconService;
    }

    @Given("home page is open: click \"Login\" icon")
    public void loginLinkClick() {
        loginIconService.loginIconClick();
    }

    @Then("slide login is open: \"Email\" input,\"Password\" input, \"Log In\" button")
    public void loginSlideIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        loginIconService.loginSlideIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("type text in \"Email\" input, type text in \"Password\" input")
    public void fillLoginForm() {
        loginIconService.fillLoginForm("al.kulesh.box@mail.ru", "111111");
    }
}
