package smokeTests.homepageTests;

import io.cucumber.java.en.Given;
import org.assertj.core.api.SoftAssertions;
import services.pageServices.HomePageService;


//Тест-кейс №2. Наличие основных блоков главной страницы: header, main-container, footer
public class FindingBlocksHomePageStepDefs {
    private final HomePageService homePageService;

    public FindingBlocksHomePageStepDefs(HomePageService homePageService) {
        this.homePageService = homePageService;
    }

    @Given("home page is open: \"Header\" baseElement, \"Container\" baseElement, \"Footer\" baseElement")
    public void test() {
        SoftAssertions softAssertions = new SoftAssertions();
        homePageService.mainBlocksIsShown(softAssertions);
        softAssertions.assertAll();
    }
}
