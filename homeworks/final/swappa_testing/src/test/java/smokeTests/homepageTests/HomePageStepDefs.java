package smokeTests.homepageTests;

import io.cucumber.java.en.Given;
import org.assertj.core.api.SoftAssertions;
import services.pageServices.HomePageService;

//Тест-кейс №1. Открыть swappa.com
public class HomePageStepDefs {
    private final HomePageService homePageService;

    public HomePageStepDefs(HomePageService homePageService) {
        this.homePageService = homePageService;
    }

    @Given("home page is open: \"Welcome to the safest marketplace for used tech\" label")
    public void homePageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        homePageService.buyAndSellLabelIsDisplayed(softAssertions);
        softAssertions.assertAll();
    }
}
