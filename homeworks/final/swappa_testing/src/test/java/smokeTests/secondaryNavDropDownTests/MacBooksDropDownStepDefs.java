package smokeTests.secondaryNavDropDownTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.secondaryNavBar.SecondaryNavBarMacBookService;

//Тест-кейс №5. Работоспособность ссылки с выдвижным списком "MacBooks", и ссылкок "MacBook Pro", "MacBook Air" из выдвижного списка
public class MacBooksDropDownStepDefs {
    private final SecondaryNavBarMacBookService secondaryNavBarMacBookService;

    public MacBooksDropDownStepDefs(SecondaryNavBarMacBookService secondaryNavBarMacBookService) {
        this.secondaryNavBarMacBookService = secondaryNavBarMacBookService;
    }

    @Given("home page is open: click dropdown link \"MacBooks\"")
    public void clickPlanLink() {
        secondaryNavBarMacBookService.clickMacBooksLink();
    }

    @Then("macbook page is open: \"Apple MacBooks\" label")
    public void AllPlansPageIsShown() {
        SoftAssertions softAssertion = new SoftAssertions();
        secondaryNavBarMacBookService.macBooksPageIsShown(softAssertion);
    }

    @And("place the cursor at dropdown link \"MacBooks\" and click \"MacBook Pro\" link from dropdown")
    public void clickMacProSectionFromDropDown() {
        secondaryNavBarMacBookService.clickMacProSection();
    }

    @Then("macbook pro page from dropdown \"MacBooks\" is open: \"MacBook Pro\" label")
    public void macProPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarMacBookService.macProPageIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("place the cursor at dropdown link \"MacBooks\" and click \"MacBook Air\" link from dropdown")
    public void clickMacAirSectionFromDropDown() {
        secondaryNavBarMacBookService.clickMacAirSection();
    }

    @Then("macbook air page from dropdown \"MacBooks\" is open: \"MacBook Air\" label")
    public void macAirPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarMacBookService.macAirPageIsShown(softAssertions);
        softAssertions.assertAll();
    }
}
