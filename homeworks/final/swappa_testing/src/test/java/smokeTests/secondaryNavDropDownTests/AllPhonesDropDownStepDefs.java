package smokeTests.secondaryNavDropDownTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.secondaryNavBar.SecondaryNavBarAllPhonesService;

//Тест-кейс №4. Работоспособность ссылки с выдвижным списком "Phones", и ссылкок "Verizon", "T-Mobile" из выдвижного списка
public class AllPhonesDropDownStepDefs {
    private final SecondaryNavBarAllPhonesService secondaryNavBarAllPhonesService;

    public AllPhonesDropDownStepDefs(SecondaryNavBarAllPhonesService secondaryNavBarAllPhonesService) {
        this.secondaryNavBarAllPhonesService = secondaryNavBarAllPhonesService;
    }

    @Given("home page is open: click dropdown link \"Phones\"")
    public void clickIphoneLink() {
        secondaryNavBarAllPhonesService.clickAllPhonesLink();
    }

    @Then("phones page is open: \"Phones For Sale\" label")
    public void iPhonesPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarAllPhonesService.allPhonePageIsShown(softAssertions);
    }

    @And("place the cursor at dropdown link \"Phones\" and click \"Verizon\" link from dropdown")
    public void clickVerizonSectionFromDropDown() {
        secondaryNavBarAllPhonesService.clickVerizonSection();
    }

    @Then("verizon page from dropdown \"Phones\" is open: \"Verizon\" label")
    public void verizonPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarAllPhonesService.verizonPageIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("place the cursor at dropdown link \"Phones\" and click \"T-Mobile\" link from dropdown")
    public void clickTMobileFromDropDown() {
        secondaryNavBarAllPhonesService.clickTMobileSection();
    }

    @Then("verizon page from dropdown \"Phones\" is open: \"T-Mobile\" label")
    public void tMobilePageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarAllPhonesService.tMobilePageIsShown(softAssertions);
        softAssertions.assertAll();
    }
}
