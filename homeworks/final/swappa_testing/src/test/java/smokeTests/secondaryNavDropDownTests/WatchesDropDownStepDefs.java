package smokeTests.secondaryNavDropDownTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.secondaryNavBar.SecondaryNavBarWatchesService;

//Тест-кейс №7. Работоспособность ссылки с выдвижным списком "Watches", и ссылкок "Apple Watch", "Android Watch" из выдвижного списка
public class WatchesDropDownStepDefs {
    private final SecondaryNavBarWatchesService secondaryNavBarWatchesService;

    public WatchesDropDownStepDefs(SecondaryNavBarWatchesService secondaryNavBarWatchesService) {
        this.secondaryNavBarWatchesService = secondaryNavBarWatchesService;
    }

    @Given("home page is open: click dropdown link \"Watches\"")
    public void clickPlanLink() {
        secondaryNavBarWatchesService.clickWatchesSectionLink();
    }

    @Then("watches page is open: \"Watches and Wearables\" label")
    public void AllPlansPageIsShown() {
        SoftAssertions softAssertion = new SoftAssertions();
        secondaryNavBarWatchesService.watchesPageIsShown(softAssertion);
    }

    @And("place the cursor at dropdown link \"Watches\" and click \"Apple Watch\" link from dropdown")
    public void clickAppleWatchSectionFromDropDown() {
        secondaryNavBarWatchesService.clickAppleWatchSection();
    }

    @Then("apple watch page from dropdown \"Watches\" is open: \"Apple Watch Sale\" label")
    public void appleWatchPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarWatchesService.appleWatchPageIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("place the cursor at dropdown link \"Watches\" and click \"Android Watch\" link from dropdown")
    public void clickAndroidWatchSectionFromDropDown() {
        secondaryNavBarWatchesService.clickAndroidWatchSection();
    }

    @Then("android watch page from dropdown \"Watches\" is open: \"Android Watch\" label")
    public void androidWatchPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarWatchesService.androidWatchPageIsShown(softAssertions);
        softAssertions.assertAll();
    }
}
