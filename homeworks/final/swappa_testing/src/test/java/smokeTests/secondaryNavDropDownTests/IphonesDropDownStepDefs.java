package smokeTests.secondaryNavDropDownTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.secondaryNavBar.SecondaryNavBarIphonesService;

//Тест-кейс №3. Работоспособность ссылки с выдвижным списком "iPhones", и ссылкок "iPhone 8 Plus", "iPhone 12" из выдвижного списка
public class IphonesDropDownStepDefs {
    private final SecondaryNavBarIphonesService secondaryNavBarIphonesService;

    public IphonesDropDownStepDefs(SecondaryNavBarIphonesService secondaryNavBarIphonesService) {
        this.secondaryNavBarIphonesService = secondaryNavBarIphonesService;
    }

    @Given("home page is open: click dropdown link \"iPhones\"")
    public void clickIphoneLink() {
        secondaryNavBarIphonesService.clickIPhonesLink();
    }

    @Then("iPhones page is open: \"iPhones for sale\" label")
    public void iPhonesPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarIphonesService.allIphonePageIsShown(softAssertions);
    }

    @And("place the cursor at dropdown link \"iPhones\" and click \"iPhone 8 Plus\" link from dropdown")
    public void clickIphone8PlusSectionFromDropDown() {
        secondaryNavBarIphonesService.clickIphone8PlusSection();
    }

    @Then("iPhone 8 plus page from dropdown \"iPhones\" is open: \"Apple Iphone 8 Plus\" label")
    public void iPhone8PlusPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarIphonesService.iPhone8PlusPageIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("place the cursor at dropdown link \"iPhones\" and click \"iPhone 12\" link from dropdown")
    public void clickIphone12SectionFromDropDown() {
        secondaryNavBarIphonesService.clickIphone12Section();
    }

    @Then("iPhone 12 page from dropdown \"iPhones\" is open: \"Apple Iphone 12\" label")
    public void iPhone12PageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarIphonesService.iPhone12PageIsShown(softAssertions);
        softAssertions.assertAll();
    }
}
