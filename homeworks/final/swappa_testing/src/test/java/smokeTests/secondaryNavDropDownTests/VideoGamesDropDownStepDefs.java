package smokeTests.secondaryNavDropDownTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.secondaryNavBar.SecondaryNavBarVideoGamesService;

//Тест-кейс №9. Работоспособность ссылки с выдвижным списком "Video Games", и ссылкок "PlayStation 4", "Games" из выдвижного списка
public class VideoGamesDropDownStepDefs {
    private final SecondaryNavBarVideoGamesService secondaryNavBarVideoGamesService;

    public VideoGamesDropDownStepDefs(SecondaryNavBarVideoGamesService secondaryNavBarVideoGamesService) {
        this.secondaryNavBarVideoGamesService = secondaryNavBarVideoGamesService;
    }

    @Given("home page is open: click dropdown link \"Video Games\"")
    public void clickPlanLink() {
        secondaryNavBarVideoGamesService.clickVideoGamesLink();
    }

    @Then("video games page is open: \"Gaming\" label")
    public void videoGamesPageIsShown() {
        SoftAssertions softAssertion = new SoftAssertions();
        secondaryNavBarVideoGamesService.videoGamesPageIsShown(softAssertion);
    }

    @And("place the cursor at dropdown link \"Video Games\" and click \"PlayStation 4\" link from dropdown")
    public void clickAppleWatchSectionFromDropDown() {
        secondaryNavBarVideoGamesService.clickPlayStation4Section();
    }

    @Then("playstation 4 page from dropdown \"Video Games\" is open: \"PlayStation 4\" label")
    public void playStation4PageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarVideoGamesService.playStation4PageIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("place the cursor at dropdown link \"Video Games\" and click \"Games\" link from dropdown")
    public void clickGamesSectionFromDropDown() {
        secondaryNavBarVideoGamesService.clickGamesSection();
    }

    @Then("games page from dropdown \"Video Games\" is open: \"Games\" label")
    public void gamesPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarVideoGamesService.gamesPageIsShown(softAssertions);
        softAssertions.assertAll();
    }
}
