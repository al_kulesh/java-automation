package smokeTests.secondaryNavDropDownTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.secondaryNavBar.SecondaryNavBarTabletsService;

//Тест-кейс №8. Работоспособность ссылки с выдвижным списком "Tablets", и ссылки "iPad" из выдвижного списка
public class TabletsDropDownStepDefs {
    private final SecondaryNavBarTabletsService secondaryNavBarTabletsService;

    public TabletsDropDownStepDefs(SecondaryNavBarTabletsService secondaryNavBarTabletsService) {
        this.secondaryNavBarTabletsService = secondaryNavBarTabletsService;
    }

    @Given("home page is open: click dropdown link \"Tablets\"")
    public void clickPlanLink() {
        secondaryNavBarTabletsService.clickTabletsLink();
    }

    @Then("tablets page is open: \"Tablets For Sale\" label")
    public void AllTabletsPageIsShown() {
        SoftAssertions softAssertion = new SoftAssertions();
        secondaryNavBarTabletsService.tablesPageIsShown(softAssertion);
    }

    @And("place the cursor at dropdown link \"Tablets\" and click \"iPad\" link from dropdown")
    public void clickIPadSectionFromDropDown() {
        secondaryNavBarTabletsService.clickIPadSection();
    }

    @Then("ipad page from dropdown \"Tablets\" is open: \"iPads For Sale\" label")
    public void iPadPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarTabletsService.iPadPageIsShown(softAssertions);
        softAssertions.assertAll();
    }
}
