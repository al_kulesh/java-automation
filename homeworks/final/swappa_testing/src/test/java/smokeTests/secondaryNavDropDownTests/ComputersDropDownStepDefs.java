package smokeTests.secondaryNavDropDownTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.secondaryNavBar.SecondaryNavBarComputersService;

//Тест-кейс №6. Работоспособность ссылки с выдвижным списком "Computers", и ссылкок "ChromeBooks", "Windows" из выдвижного списка
public class ComputersDropDownStepDefs {
    private final SecondaryNavBarComputersService secondaryNavBarComputersService;

    public ComputersDropDownStepDefs(SecondaryNavBarComputersService secondaryNavBarComputersService) {
        this.secondaryNavBarComputersService = secondaryNavBarComputersService;
    }

    @Given("home page is open: click dropdown link \"Computers\"")
    public void clickPlanLink() {
        secondaryNavBarComputersService.clickComputersLink();
    }

    @Then("computer page is open: \"All Things Computer\" label")
    public void AllPlansPageIsShown() {
        SoftAssertions softAssertion = new SoftAssertions();
        secondaryNavBarComputersService.computersPageIsShown(softAssertion);
    }

    @And("place the cursor at dropdown link \"Computers\" and click \"ChromeBooks\" link from dropdown")
    public void clickChromeBooksSectionFromDropDown() {
        secondaryNavBarComputersService.clickChromeSection();
    }

    @Then("chrome books page from dropdown \"Computers\" is open: \"ChromeBook\" label")
    public void chromeBooksPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarComputersService.chromeBookPageIsShown(softAssertions);
        softAssertions.assertAll();
    }

    @And("place the cursor at dropdown link \"Computers\" and click \"Windows\" link from dropdown")
    public void clickWindowsSectionFromDropDown() {
        secondaryNavBarComputersService.clickWindowsSection();
    }

    @Then("windows page from dropdown \"Computers\" is open: \"Windows laptops\" label")
    public void windowsPageIsShown() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarComputersService.windowsPageIsShown(softAssertions);
        softAssertions.assertAll();
    }
}
