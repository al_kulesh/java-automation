package smokeTests.catalogTests;

import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.secondaryNavBar.SecondaryNavBarAllPhonesService;

//Тест-кейс №18. Наличие брендов смартфонов со списком моделей в катологе на вкладке "Phones"
public class AllPhonesCatalogStepDefs {
    private final SecondaryNavBarAllPhonesService allPhonesService;

    public AllPhonesCatalogStepDefs(SecondaryNavBarAllPhonesService allPhonesService){
        this.allPhonesService = allPhonesService;
    }

    @Then("phones page is open: phone brand cards with list of models displayed")
    public void phoneBrandsAreDisplayed() {
        SoftAssertions softAssertions = new SoftAssertions();
        allPhonesService.phoneBrandsAreDisplayed(softAssertions);
        softAssertions.assertAll();
    }
}
