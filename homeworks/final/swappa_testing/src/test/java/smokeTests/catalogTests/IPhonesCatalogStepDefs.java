package smokeTests.catalogTests;

import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.secondaryNavBar.SecondaryNavBarIphonesService;

//Тест-кейс №17. Наличие всех моделей iPhone в катологе на вкладке "iPhones"
public class IPhonesCatalogStepDefs {
    private final SecondaryNavBarIphonesService secondaryNavBarIphonesService;

    public IPhonesCatalogStepDefs(SecondaryNavBarIphonesService secondaryNavBarIphonesService) {
        this.secondaryNavBarIphonesService = secondaryNavBarIphonesService;
    }

    @Then("iPhones page is open: all iPhone model are present in catalog")
    public void allIphoneModelsAreDisplayed() {
        SoftAssertions softAssertions = new SoftAssertions();
        secondaryNavBarIphonesService.allIphoneModelsAreDisplayed(softAssertions);
        softAssertions.assertAll();
    }
}
