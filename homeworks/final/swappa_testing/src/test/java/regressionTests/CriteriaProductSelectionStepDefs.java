package regressionTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.navBarServices.secondaryNavBar.SecondaryNavBarAllPhonesService;
import services.pageServices.AllModelsPageService;
import services.pageServices.ModelPageService;
import services.pageServices.ModelCriteriaChoosePageService;

//Тест-кейс №19. Подбор товара по определенным критериям
public class CriteriaProductSelectionStepDefs {
    private final SecondaryNavBarAllPhonesService secondaryNavBarAllPhonesService;
    private final AllModelsPageService allModelsPageService;
    private final ModelPageService modelPageService;
    private final ModelCriteriaChoosePageService modelCriteriaChoosePageService;

    public CriteriaProductSelectionStepDefs(
            SecondaryNavBarAllPhonesService secondaryNavBarAllPhonesService,
            AllModelsPageService allModelsPageService,
            ModelPageService modelPageService,
            ModelCriteriaChoosePageService modelCriteriaChoosePageService) {
        this.secondaryNavBarAllPhonesService = secondaryNavBarAllPhonesService;
        this.allModelsPageService = allModelsPageService;
        this.modelPageService = modelPageService;
        this.modelCriteriaChoosePageService = modelCriteriaChoosePageService;
    }

    @Given("home page is open: place the cursor at dropdown link \"Phones\" and click link \"Samsung\" from dropdown")
    public void selectFromDropDown() {
        secondaryNavBarAllPhonesService.samsungSectionSelectFromDropDown();
    }

    @Then("samsung page is open: \"Samsung Galaxy Phones\" label")
    public void samsungGalaxyPhonesLabelIsDisplayed() {
        SoftAssertions softAssertion = new SoftAssertions();
        allModelsPageService.samsungLabelIsDisplayed(softAssertion);
        softAssertion.assertAll();
    }

    @And("choose \"Samsung Galaxy S10\" from catalogue")
    public void chooseModel() {
        allModelsPageService.galaxyS10ButtonClick();
    }

    @Then("samsung galaxy s10 page is open: \"Samsung Galaxy S10\" label")
    public void samsungGalaxyS10LabelIsDisplayed() {
        SoftAssertions softAssertion = new SoftAssertions();
        modelPageService.samsungLabelIsDisplayed(softAssertion);
        softAssertion.assertAll();
    }

    @And("click \"Unlocked\" button on the samsung galaxy s10 page")
    public void choosePlan() {
        modelPageService.planUnlockedClick();
    }

    @Then("criteria choose page is open: \"Express shipping\" checkbox, \"Condition\" checkbox, \"Model\" checkbox, \"Color\" checkbox, \"Storage\" checkbox, \"Sort by\" radio")
    public void checkBoxAndRadioAreDisplayed() {
        SoftAssertions softAssertion = new SoftAssertions();
        modelCriteriaChoosePageService.checkBoxAndRadioAreDisplayed(softAssertion);
        softAssertion.assertAll();
    }

    @And("check \"Express shipping\" checkbox, check \"Condition\" checkbox, check \"Model\" checkbox, check \"Color\" checkbox, check \"Storage\" checkbox, check \"Sort by\" radio")
    public void modelCriteriaFilterBy() throws InterruptedException {
        modelCriteriaChoosePageService.filterBy();
        Thread.sleep(5000);
    }

    @Then("choose random filtered listing and check labels: \"Condition\" label, \"Plan\" label, \"Model\" label, \"Color\" label")
    public void checkListingLabels() {
        SoftAssertions softAssertion = new SoftAssertions();
        modelCriteriaChoosePageService.checkRandomFilteredProduct(softAssertion);
        softAssertion.assertAll();
    }
}
