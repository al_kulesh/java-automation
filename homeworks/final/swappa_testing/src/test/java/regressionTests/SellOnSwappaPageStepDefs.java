package regressionTests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import services.pageServices.LoginPageService;
import services.pageServices.ModelPageService;
import services.pageServices.OneModelCatalogPageService;
import services.pageServices.SellOnSwappaPageService;

//Тест-кейс №20. Работоспособность search-bar "Search to sell", выбор модели из каталога, и заполнение полей на странице логирования
public class SellOnSwappaPageStepDefs {
    private final LoginPageService loginPageService;
    private final ModelPageService modelPageService;
    private final OneModelCatalogPageService oneModelCatalogPageService;
    private final SellOnSwappaPageService sellOnSwappaPageService;

    public SellOnSwappaPageStepDefs(
            LoginPageService loginPageService,
            ModelPageService modelPageService,
            OneModelCatalogPageService oneModelCatalogPageService,
            SellOnSwappaPageService sellOnSwappaPageService
    ) {
        this.loginPageService = loginPageService;
        this.modelPageService = modelPageService;
        this.oneModelCatalogPageService = oneModelCatalogPageService;
        this.sellOnSwappaPageService = sellOnSwappaPageService;
    }

    @And("type in \"Search to Sell\" input, click \"Search\" button")
    public void searchBarWork() {
        sellOnSwappaPageService.searchBarWork("Iphone 7");
    }

    @Then("search to sell result page is open: \"Iphone 7\" label")
    public void resultPageIsShown() {
        SoftAssertions softAssertion = new SoftAssertions();
        oneModelCatalogPageService.sellOnSwappaNavBarSearchResultPageIsShown(softAssertion);
        softAssertion.assertAll();
    }

    @And("choose \"Iphone 7\" from catalog")
    public void chooseIphone7() {
        oneModelCatalogPageService.chooseIphone7();
    }

    @Then("sell iPhone 7 page is open: \"Sell iPhone 7\" label")
    public void sellIPhone7LabelIsDisplayed() {
        SoftAssertions softAssertions = new SoftAssertions();
        modelPageService.sellIPhone7LabelIsDisplayed(softAssertions);
        softAssertions.assertAll();
    }
}
