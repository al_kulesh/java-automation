@regression
Feature: Criteria product selection feature

  Scenario: choose product using filtration with certain parameters, check that filtration works
    Given home page is open: place the cursor at dropdown link "Phones" and click link "Samsung" from dropdown
    Then samsung page is open: "Samsung Galaxy Phones" label
    And choose "Samsung Galaxy S10" from catalogue
    Then samsung galaxy s10 page is open: "Samsung Galaxy S10" label
    And click "Unlocked" button on the samsung galaxy s10 page
    Then criteria choose page is open: "Express shipping" checkbox, "Condition" checkbox, "Model" checkbox, "Color" checkbox, "Storage" checkbox, "Sort by" radio
    And check "Express shipping" checkbox, check "Condition" checkbox, check "Model" checkbox, check "Color" checkbox, check "Storage" checkbox, check "Sort by" radio
    Then choose random filtered listing and check labels: "Condition" label, "Plan" label, "Model" label, "Color" label
