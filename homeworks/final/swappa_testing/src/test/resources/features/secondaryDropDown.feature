@smoke
Feature: Secondary dropdown feature

  Scenario: check that the dropdown link "iPhones", link "iPhone 8 Plus" and link "iPhone 12" from dropdown works
    Given home page is open: click dropdown link "iPhones"
    Then iPhones page is open: "iPhones for sale" label
    And place the cursor at dropdown link "iPhones" and click "iPhone 8 Plus" link from dropdown
    Then iPhone 8 plus page from dropdown "iPhones" is open: "Apple Iphone 8 Plus" label
    And place the cursor at dropdown link "iPhones" and click "iPhone 12" link from dropdown
    Then iPhone 12 page from dropdown "iPhones" is open: "Apple Iphone 12" label

  Scenario: check that the dropdown link "Phones", link "Verizon" and link "T-Mobile" from dropdown works
    Given home page is open: click dropdown link "Phones"
    Then phones page is open: "Phones For Sale" label
    And place the cursor at dropdown link "Phones" and click "Verizon" link from dropdown
    Then verizon page from dropdown "Phones" is open: "Verizon" label
    And place the cursor at dropdown link "Phones" and click "T-Mobile" link from dropdown
    Then verizon page from dropdown "Phones" is open: "T-Mobile" label

  Scenario: check that the dropdown link "MacBooks", link "MacBook Pro" and link "MacBook Air" from dropdown works
    Given home page is open: click dropdown link "MacBooks"
    Then macbook page is open: "Apple MacBooks" label
    And place the cursor at dropdown link "MacBooks" and click "MacBook Pro" link from dropdown
    Then macbook pro page from dropdown "MacBooks" is open: "MacBook Pro" label
    And place the cursor at dropdown link "MacBooks" and click "MacBook Air" link from dropdown
    Then macbook air page from dropdown "MacBooks" is open: "MacBook Air" label

  Scenario: check that the dropdown link "Computers", link "ChromeBooks" and link "Windows" from dropdown works
    Given home page is open: click dropdown link "Computers"
    Then computer page is open: "All Things Computer" label
    And place the cursor at dropdown link "Computers" and click "ChromeBooks" link from dropdown
    Then chrome books page from dropdown "Computers" is open: "ChromeBook" label
    And place the cursor at dropdown link "Computers" and click "Windows" link from dropdown
    Then windows page from dropdown "Computers" is open: "Windows laptops" label

  Scenario: check that the dropdown link "Watches", link "Apple Watch" and link "Android Watch" from dropdown works
    Given home page is open: click dropdown link "Watches"
    Then watches page is open: "Watches and Wearables" label
    And place the cursor at dropdown link "Watches" and click "Apple Watch" link from dropdown
    Then apple watch page from dropdown "Watches" is open: "Apple Watch Sale" label
    And place the cursor at dropdown link "Watches" and click "Android Watch" link from dropdown
    Then android watch page from dropdown "Watches" is open: "Android Watch" label

  Scenario: check that the dropdown link "Tablets", and link "iPad" from dropdown works
    Given home page is open: click dropdown link "Tablets"
    Then tablets page is open: "Tablets For Sale" label
    And place the cursor at dropdown link "Tablets" and click "iPad" link from dropdown
    Then ipad page from dropdown "Tablets" is open: "iPads For Sale" label

  Scenario: check that the dropdown link "Video Games", link "PlayStation 4" and link "Games" from dropdown works
    Given home page is open: click dropdown link "Video Games"
    Then video games page is open: "Gaming" label
    And place the cursor at dropdown link "Video Games" and click "PlayStation 4" link from dropdown
    Then playstation 4 page from dropdown "Video Games" is open: "PlayStation 4" label
    And place the cursor at dropdown link "Video Games" and click "Games" link from dropdown
    Then games page from dropdown "Video Games" is open: "Games" label

  Scenario: check that all iPhone models displayed on catalog, when dropdown link "iPhones" clicked
    Given home page is open: click dropdown link "iPhones"
    Then iPhones page is open: all iPhone model are present in catalog

  Scenario: check that phone brand cards with list of models displayed, when dropdown link "Phones" clicked
    Given home page is open: click dropdown link "Phones"
    Then phones page is open: phone brand cards with list of models displayed