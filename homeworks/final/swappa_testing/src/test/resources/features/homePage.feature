@smoke
Feature: Homepage feature

    Scenario: check that the home page is shown after opening it from browser
        Given home page is open: "Welcome to the safest marketplace for used tech" label

    Scenario: check that the main blocks of home page are shown after opening it from browser
        Given home page is open: "Header" baseElement, "Container" baseElement, "Footer" baseElement