Feature: Primary nav-bar feature
  @smoke
  Scenario: check that the "Search" icon, search-bar works correctly and search result page is shown
    Given home page is open: click "Search" icon click
    Then slide search is open: "slide-search" element
    And type in "Search bar" input, click "Search" button
    Then search result page is open: "Search result label" label
  @smoke
  Scenario: check that "Sell" icon works, "Start Selling" button works and sell page is shown
    Given home page is open: click "Sell" icon
    Then slide sell is open: "slide_sell" element
    And click "Start Selling" button
    Then sell page is open: "Search to sell" search-bar
  @smoke
  Scenario: check that "Help" icon works, help page is shown and all fields are filled
    Given home page is open: click "Help" icon
    Then slide help is open: "slide_help" element
    And click "Submit Help Request" button
    Then help page is open: "Email" input, "Subject" selector, "Message" input, "Link" input, "Captcha" checkbox
    And type text in "Email" input, choose option from "Subject" selector, type text in "Message" input, check "Captcha" checkbox
  @smoke
  Scenario: check that "Login" icon works, slide login is shown and all fields are filled
    Given home page is open: click "Login" icon
    Then slide login is open: "Email" input,"Password" input, "Log In" button
    And type text in "Email" input, type text in "Password" input
  @smoke
  Scenario: check that "Register" link works, register page is shown and all fields are filled
    Given home page is open: click "Login" icon
    Then slide login is open: "Email" input,"Password" input, "Log In" button
    And click "Register" button
    Then register page is open: "Email" input, "First Name" input, "Last Name" input, "Password" input
    And type text in "Email" input, type text in "First Name" input,  type text in "Last Name" input,  type text in "Password" input
  @smoke
  Scenario: check that "Cart" icon works, Cart page is shown
    Given home page is open: click "Cart" icon
    Then slide cart is open: "slide_cart" element
    And click "Find a good deal" button
    Then slide search is open: "slide-search" element
  @smoke
  Scenario: check that "Swappa" logo works, home page is shown
    Given home page is open click random dropdown link : "Phones" dropdown link, "Computers" dropdown link, "Tablets" dropdown link
    Then random page is open: "Breadcrumbs" baseElement , "Announcements" baseElement, "Section_billboard" baseElement, "Section_trailer" baseElement
    And click "Swappa" logo
    Then home page is open: "Welcome to the safest marketplace for used tech" label

  @regression
  Scenario: check that "Search to Sell" search bar works, choosing model from catalog is working
    Given home page is open: click "Sell" icon
    Then slide sell is open: "slide_sell" element
    And click "Start Selling" button
    Then sell page is open: "Search to sell" search-bar
    And type in "Search to Sell" input, click "Search" button
    Then search to sell result page is open: "Iphone 7" label
    And choose "Iphone 7" from catalog
    Then sell iPhone 7 page is open: "Sell iPhone 7" label