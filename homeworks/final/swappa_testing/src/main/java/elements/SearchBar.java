package elements;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

public class SearchBar extends BaseElement{
    public SearchBar(By by) {
        super(by);
    }

    public SearchBar(SelenideElement wrappedElement) {
        super(wrappedElement);
    }

    public void typeText(String text){
        this.logAction("Search-bar text input");
        this.getWrappedElement().sendKeys(text);
    }
}
