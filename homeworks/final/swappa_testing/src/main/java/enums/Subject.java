package enums;

public enum Subject {
    GENERAL("General"), BUYING("Buying"), SELLING("Selling"), GAMING("Gaming"),
    PAYMENTS("Payments"), INTERNATIONAL("International"), QUESTION("Question"), FEEDBACK("Feedback"),
    B2B_EXCHANGE("B2B Exchange"), BONEYARD("Boneyard"), OTHER("Other");

    private final String subjectName;

    Subject(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectName() {
        return subjectName;
    }
}
