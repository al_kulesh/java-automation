package pages;

import com.codeborne.selenide.SelenideElement;
import elements.Input;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class RegisterPage {
    private final SelenideElement formRegister = $(By.id("formRegister"));
    private final Input email = new Input(formRegister.$(By.id("id_email")));
    private final Input firstName = new Input(formRegister.$(By.id("id_first_name")));
    private final Input lastName = new Input(formRegister.$(By.id("id_last_name")));
    private final Input password = new Input(formRegister.$(By.id("id_password1")));

    public Input getEmail() {
        return email;
    }

    public Input getFirstName() {
        return firstName;
    }

    public Input getLastName() {
        return lastName;
    }

    public Input getPassword() {
        return password;
    }
}
