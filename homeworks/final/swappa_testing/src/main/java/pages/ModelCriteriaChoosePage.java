package pages;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Checkbox;
import elements.Collection;
import elements.Radio;
import org.openqa.selenium.By;
import pages.blocks.listings.ListingsElement;

import static com.codeborne.selenide.Selenide.$;

public class ModelCriteriaChoosePage {
    private final Button filterListings = new Button($(By.xpath("//*[@id=\"product-buy-app\"]/div/div/div[2]/div[1]/div/div[2]/div[2]/a")));
    private final Button filterClose = new Button($(By.xpath("//*[@id=\"slide_filter\"]/div[1]/button")));
    private final SelenideElement root = $(By.xpath("//*[@id=\"slide_filter\"]/div[2]/div[1]"));
    private final Checkbox conditionFilter = new Checkbox(root.$(By.xpath("div[1]/div/label[3]/input")));
    private final Checkbox colorFilter = new Checkbox(root.$(By.xpath("div[2]/div/label[1]/input")));
    private final Checkbox storageFilter = new Checkbox(root.$(By.xpath("div[3]/div/label[1]/input")));
    private final Checkbox expressShipping = new Checkbox(root.$(By.xpath("div[4]/div/label/input")));
    private final Radio sortBy = new Radio(root.$(By.xpath("div[5]/div/label[3]/input")));
    final Collection<ListingsElement> listingsElementCollection = new Collection<>(ListingsElement.class, By.xpath("//*[@id=\"product-buy-app\"]/div/div/div[2]/div[2]/div/div[2]"));

    public Collection<ListingsElement> getListingsElementCollection() {
        return listingsElementCollection;
    }

    public Button getFilterListings() {
        return filterListings;
    }

    public Button getFilterClose() {
        return filterClose;
    }

    public Checkbox getConditionFilter() {
        return conditionFilter;
    }

    public Checkbox getColorFilter() {
        return colorFilter;
    }

    public Checkbox getStorageFilter() {
        return storageFilter;
    }

    public Checkbox getExpressShipping() {
        return expressShipping;
    }

    public Radio getSortBy() {
        return sortBy;
    }

}
