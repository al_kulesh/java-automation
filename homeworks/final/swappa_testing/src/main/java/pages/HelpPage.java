package pages;

import com.codeborne.selenide.SelenideElement;
import elements.*;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class HelpPage {
    private final SelenideElement helpForm = $(By.id("helpForm"));
    private final Input email = new Input(helpForm.$(By.id("id_email")));
    private final Selector subject = new Selector(helpForm.$(By.id("id_subject")));
    private final Input message = new Input(helpForm.$(By.id("id_message")));
    private final Input link = new Input(helpForm.$(By.id("id_link")));
    private final Radio captcha = new Radio(helpForm.$(By.id("id_captcha")));

    public Input getEmail() {
        return email;
    }

    public Selector getSubject() {
        return subject;
    }

    public Input getMessage() {
        return message;
    }

    public Input getLink() {
        return link;
    }

    public Radio getCaptcha() {
        return captcha;
    }

}
