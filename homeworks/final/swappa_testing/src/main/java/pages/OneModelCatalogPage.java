package pages;

import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class OneModelCatalogPage {
    private final Label searchResultLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[2]/p/b[text()='Samsung Galaxy S9']")));
    private final Label sellSearchResultLabel = new Label($(By.xpath("//*[@id=\"main_container\"]/section[6]/div/div[1]/div/a[text()='\n" +
            "\t\tApple iPhone 7\n" +
            "\t']")));
    private final Button iPhone7Btn = new Button($(By.xpath("//*[@id=\"main_container\"]/section[6]/div/div[1]/div/a[text()='\n" +
            "\t\tApple iPhone 7\n" +
            "\t']")));

    public Label getSearchResultLabel(){ return searchResultLabel;}

    public Label getSellSearchResultLabel() {
        return sellSearchResultLabel;
    }

    public Button getiPhone7Btn() {
        return iPhone7Btn;
    }
}
