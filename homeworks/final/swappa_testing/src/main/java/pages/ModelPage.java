package pages;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ModelPage {
    private final SelenideElement root = $(By.id("test_big_price"));
    private final Button unlocked = new Button(root.$(By.xpath("div[2]/div[3]/a")));
    private final Label samsungLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='\n" +
            "\t\t\t\tSamsung Galaxy S10\n" +
            "\t\t\t']")));
    private final Label sellIPhone7Label = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div/h1[text()='Sell iPhone 7 ']")));
    private final Button loginToSellBtn = new Button($(By.xpath("//*[@id=\"section_billboard\"]/div/div[2]/div/div/div[1]/p[2]/a")));

    public Button getUnlocked() {
        return unlocked;
    }

    public Label getSamsungLabel() {
        return samsungLabel;
    }

    public Label getSellIPhone7Label() {
        return sellIPhone7Label;
    }

    public Button getLoginToSellBtn() {
        return loginToSellBtn;
    }
}
