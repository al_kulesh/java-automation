package pages;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class AllModelsPage {
    private final SelenideElement root = $(By.id("section_billboard"));
    private final Button galaxyS10 = new Button(root.$(By.xpath("div[2]/div[9]/div/a[2]")));
    private final Label samsungPhonesLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='Samsung Galaxy Phones']")));

    public Button getGalaxyS10() {
        return galaxyS10;
    }

    public Label getSamsungPhonesLabel() {
        return samsungPhonesLabel;
    }
}
