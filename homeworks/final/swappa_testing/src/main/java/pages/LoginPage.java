package pages;

import elements.Input;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage {
    private final Input email = new Input($(By.id("id_username")));
    private final Input password = new Input($(By.id("id_password")));

    public Input getEmail() {
        return email;
    }

    public Input getPassword() {
        return password;
    }
}

