package pages;

import elements.BaseElement;
import elements.Label;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class HomePage {
    private final BaseElement header = new BaseElement($(byXpath("/html/body/header")));
    private final BaseElement mainContainer = new BaseElement($(byXpath("//*[@id=\"main_container\"]")));
    private final BaseElement footer = new BaseElement($(byXpath("/html/body/footer")));
    private final Label welcomeLabel = new Label($(byXpath("//*[@id=\"section_top\"]/h2[text()='\n" +
            "\t\tWelcome to the safest marketplace for used tech\n" +
            "\t']")));

    public BaseElement getHeader() {
        return header;
    }

    public BaseElement getMainContainer() {
        return mainContainer;
    }

    public BaseElement getFooter() {
        return footer;
    }

    public Label getWelcomeLabel() {
        return welcomeLabel;
    }
}

