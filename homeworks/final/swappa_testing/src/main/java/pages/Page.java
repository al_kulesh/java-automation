package pages;

import elements.BaseElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class Page {
    private final BaseElement breadcrumbs = new BaseElement($(By.id("section_breadcrumbs")));
    private final BaseElement announcements = new BaseElement($(By.id("announcements")));
    private final BaseElement section_trailer = new BaseElement($(By.id("section_trailer")));

    public BaseElement getBreadcrumbs() {
        return breadcrumbs;
    }

    public BaseElement getAnnouncements() {
        return announcements;
    }

    public BaseElement getSection_trailer() {
        return section_trailer;
    }
}
