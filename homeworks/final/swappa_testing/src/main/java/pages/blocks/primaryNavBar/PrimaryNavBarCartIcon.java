package pages.blocks.primaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.BaseElement;
import elements.Button;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class PrimaryNavBarCartIcon {
    private final SelenideElement root = $(By.id("primaryNav"));
    private final Button cartIcon = new Button(root.$(By.xpath("nav/div/div/div[3]/ul/li[2]/a")));
    private final SelenideElement slideCartRoot = $(By.id("slide_cart"));
    private final BaseElement slideCart = new BaseElement($(slideCartRoot));
    private final Button viewCartBtn = new Button(slideCartRoot.$(By.xpath("div[3]/div/div[2]/a")));

    public Button getCartIcon() {
        return cartIcon;
    }

    public BaseElement getSlideCart() {
        return slideCart;
    }

    public Button getViewCartBtn() {
        return viewCartBtn;
    }
}
