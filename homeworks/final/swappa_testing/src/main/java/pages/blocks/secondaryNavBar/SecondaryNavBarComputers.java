package pages.blocks.secondaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SecondaryNavBarComputers {
    private final SelenideElement computersRoot = $(By.xpath("//*[@id=\"secondaryNav\"]/nav/div/ul/li[4]"));
    private final SelenideElement computersSection = computersRoot.$(By.xpath("a"));
    private final Button computersBtn = new Button(computersRoot.$(By.xpath("a")));
    private final Button chromeBooksDropDownBtn = new Button(computersRoot.$(By.xpath("ul/li[5]/a")));
    private final Button windowsDropDownBtn = new Button(computersRoot.$(By.xpath("ul/li[6]/a")));//*[@id="section_top"]/div/h1
    private final Label computersPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/h1[text()='All Things Computer']")));
    private final Label chromeBooksPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='Chromebook']")));
    private final Label windowsPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='Windows Laptops']")));

    public SelenideElement getComputersSection() {
        return computersSection;
    }

    public Button getComputersBtn() {
        return computersBtn;
    }

    public Label getComputersPageLabel() {
        return computersPageLabel;
    }

    public Button getChromeBooksDropDownBtn() {
        return chromeBooksDropDownBtn;
    }

    public Button getWindowsDropDownBtn() {
        return windowsDropDownBtn;
    }

    public Label getChromeBooksPageLabel() {
        return chromeBooksPageLabel;
    }

    public Label getWindowsPageLabel() {
        return windowsPageLabel;
    }
}
