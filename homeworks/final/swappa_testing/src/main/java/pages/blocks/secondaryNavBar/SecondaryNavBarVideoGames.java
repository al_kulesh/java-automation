package pages.blocks.secondaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;


public class SecondaryNavBarVideoGames {
    private final SelenideElement videoGamesRoot = $(By.xpath("//*[@id=\"secondaryNav\"]/nav/div/ul/li[7]"));
    private final SelenideElement videoGamesSection = videoGamesRoot.$(By.xpath("a"));
    private final Button videoGamesBtn = new Button(videoGamesRoot.$(By.xpath("a")));
    private final Button playStation4DropDownBtn = new Button(videoGamesRoot.$(By.xpath("ul/li[2]/a")));
    private final Button gamesDropDownBtn = new Button(videoGamesRoot.$(By.xpath("ul/li[5]/a")));
    private final Label videoGamesPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='Gaming']")));
    private final Label playStation4PageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='PlayStation 4']")));
    private final Label gamesPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/h1[text()='\n" +
            "\t\t\tGames\n" +
            "\t\t']")));

    public SelenideElement getVideoGamesSection() {
        return videoGamesSection;
    }

    public Button getVideoGamesBtn() {
        return videoGamesBtn;
    }

    public Label getVideoGamesPageLabel() {
        return videoGamesPageLabel;
    }

    public Button getPlayStation4DropDownBtn() {
        return playStation4DropDownBtn;
    }

    public Button getGamesDropDownBtn() {
        return gamesDropDownBtn;
    }

    public Label getPlayStation4PageLabel() {
        return playStation4PageLabel;
    }

    public Label getGamesPageLabel() {
        return gamesPageLabel;
    }
}
