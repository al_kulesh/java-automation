package pages.blocks.secondaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Collection;
import elements.Label;
import org.openqa.selenium.By;
import pages.blocks.catalogs.IPhonesCatalog;

import static com.codeborne.selenide.Selenide.$;

public class SecondaryNavBarIphones {
    private final SelenideElement iPhonesRoot = $(By.xpath("//*[@id=\"secondaryNav\"]/nav/div/ul/li[1]"));
    private final SelenideElement iPhonesSectionAll = iPhonesRoot.$(By.xpath("a"));
    private final Button iPhonesBtn = new Button(iPhonesRoot.$(By.xpath("a")));
    private final Button iPhones8PlusDropDownBtn = new Button(iPhonesRoot.$(By.xpath("ul/li[5]/a")));//*[@id="section_main"]/div/div[1]/div
    private final Button iPhones12DropDownBtn = new Button(iPhonesRoot.$(By.xpath("ul/li[9]/a")));
    private final Label iPhonesForSalePageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='iPhones For Sale']")));
    private final Label iPhone8PlusPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='\n" +
            "\t\t\t\tApple iPhone 8 Plus\n" +
            "\t\t\t']")));
    private final Label iPhone12PageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='\n" +
            "\t\t\t\tApple iPhone 12\n" +
            "\t\t\t']")));
    private final Collection<IPhonesCatalog> allIPhonesSectionCollection = new Collection<>(IPhonesCatalog.class, By.xpath("//*[@id=\"section_main\"]/div/div"));

    public Button getiPhonesBtn() {
        return iPhonesBtn;
    }

    public SelenideElement getiPhonesSectionAll() {
        return iPhonesSectionAll;
    }

    public Label getiPhonesForSalePageLabel() {
        return iPhonesForSalePageLabel;
    }

    public Collection<IPhonesCatalog> getAllIPhonesSectionCollection() {
        return allIPhonesSectionCollection;
    }

    public Button getiPhones8PlusDropDownBtn() {
        return iPhones8PlusDropDownBtn;
    }

    public Button getiPhones12DropDownBtn() {
        return iPhones12DropDownBtn;
    }

    public Label getiPhone8PlusPageLabel() {
        return iPhone8PlusPageLabel;
    }

    public Label getiPhone12PageLabel() {
        return iPhone12PageLabel;
    }
}
