package pages.blocks.primaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.BaseElement;
import elements.Button;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class PrimaryNavBarHelpIcon {
    private final SelenideElement root = $(By.id("primaryNav"));
    private final Button helpIcon = new Button(root.$(By.xpath("nav/div/div/div[1]/ul/li[2]/a")));
    private final SelenideElement slideHelpRoot = $(By.id("slide_help"));
    private final BaseElement slideHelp = new BaseElement($(slideHelpRoot));
    private final Button helpRequestBtn = new Button(slideHelpRoot.$(By.xpath("div[2]/a[2]")));

    public Button getHelpIcon() {
        return helpIcon;
    }

    public SelenideElement getSlideHelpRoot() {
        return slideHelpRoot;
    }

    public BaseElement getSlideHelp() {
        return slideHelp;
    }

    public Button getHelpRequestBtn() {
        return helpRequestBtn;
    }
}
