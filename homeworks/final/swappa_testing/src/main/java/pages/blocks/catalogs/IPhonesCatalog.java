package pages.blocks.catalogs;

import com.codeborne.selenide.SelenideElement;
import elements.BaseElement;
import elements.Label;
import org.openqa.selenium.By;

public class IPhonesCatalog {
    private final Label label;

    public IPhonesCatalog(SelenideElement root) {
        this.label = new Label(root.$(By.xpath("div/a/h3")));
    }

    public Label getLabel() {
        return label;
    }
}
