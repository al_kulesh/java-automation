package pages.blocks.secondaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SecondaryNavBarMacBooks {
    private final SelenideElement macsRoot = $(By.xpath("//*[@id=\"secondaryNav\"]/nav/div/ul/li[3]"));
    private final SelenideElement macsSection = macsRoot.$(By.xpath("a"));
    private final Button macsBtn = new Button(macsRoot.$(By.xpath("a")));
    private final Button macBookProDropDownBtn = new Button(macsRoot.$(By.xpath("ul/li[4]/a")));
    private final Button macBookAirDropDownBtn = new Button(macsRoot.$(By.xpath("ul/li[5]/a")));
    private final Label allMacBooksPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='Apple MacBooks']")));
    private final Label macBookProPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='MacBook Pro']")));
    private final Label macBookAirPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='MacBook Air']")));

    public SelenideElement getMacsSection() {
        return macsSection;
    }

    public Button getMacsBtn() {
        return macsBtn;
    }

    public Label getAllMacBooksPageLabel() {
        return allMacBooksPageLabel;
    }

    public Button getMacBookProDropDownBtn() {
        return macBookProDropDownBtn;
    }

    public Button getMacBookAirDropDownBtn() {
        return macBookAirDropDownBtn;
    }

    public Label getMacBookProPageLabel() {
        return macBookProPageLabel;
    }

    public Label getMacBookAirPageLabel() {
        return macBookAirPageLabel;
    }
}
