package pages.blocks.secondaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Collection;
import elements.Label;
import org.openqa.selenium.By;
import pages.blocks.catalogs.AllPhonesCatalog;

import static com.codeborne.selenide.Selenide.$;

public class SecondaryNavBarAllPhones {
    private final SelenideElement allPhonesRoot = $(By.xpath("//*[@id=\"secondaryNav\"]/nav/div/ul/li[2]"));
    private final SelenideElement allPhonesSection = allPhonesRoot.$(By.xpath("a"));
    private final Button allPhonesBtn = new Button(allPhonesRoot.$(By.xpath("a")));
    private final Button samsungDropDownBtn = new Button(allPhonesRoot.$(By.xpath("ul/li[3]/a")));
    private final Button verizonDropDownBtn = new Button(allPhonesRoot.$(By.xpath("ul/li[5]/a")));
    private final Button tMobileDropDownBtn = new Button(allPhonesRoot.$(By.xpath("ul/li[7]/a")));
    private final Label phonesForSaleLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/h1[text()='Phones For Sale']")));
    private final Label verizonPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[2]/h1/span[text()='Verizon']")));
    private final Label tMobilePageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[2]/h1/span[text()='T-Mobile']")));

    private final Collection<AllPhonesCatalog> allPhonesSectionCollection = new Collection<>(AllPhonesCatalog.class, By.xpath("//*[@id=\"section_billboard\"]/div[2]/div"));

    public SelenideElement getAllPhonesSection() {
        return allPhonesSection;
    }

    public Button getAllPhonesBtn() {
        return allPhonesBtn;
    }

    public Label getPhonesForSaleLabel() {
        return phonesForSaleLabel;
    }

    public Button getSamsungDropDownBtn() {
        return samsungDropDownBtn;
    }

    public Collection<AllPhonesCatalog> getAllPhonesSectionCollection() {
        return allPhonesSectionCollection;
    }

    public Button getVerizonDropDownBtn() {
        return verizonDropDownBtn;
    }

    public Button gettMobileDropDownBtn() {
        return tMobileDropDownBtn;
    }

    public Label getVerizonPageLabel() {
        return verizonPageLabel;
    }

    public Label gettMobilePageLabel() {
        return tMobilePageLabel;
    }
}
