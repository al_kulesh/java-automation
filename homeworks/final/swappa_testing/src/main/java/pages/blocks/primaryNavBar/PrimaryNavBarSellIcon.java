package pages.blocks.primaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.BaseElement;
import elements.Button;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class PrimaryNavBarSellIcon {
    private final SelenideElement root = $(By.id("primaryNav"));
    private final Button sellIcon = new Button(root.$(By.xpath("nav/div/div/div[1]/ul/li[3]/a")));
    private final SelenideElement slideSellRoot = $(By.id("slide_sell"));
    private final BaseElement slideSell = new BaseElement($(slideSellRoot));
    private final Button startSellingBtn = new Button(slideSellRoot.$(By.xpath("div[2]/p/a")));

    public Button getSellIcon() {
        return sellIcon;
    }

    public BaseElement getSlideSell() {
        return slideSell;
    }

    public Button getStartSellingBtn() {
        return startSellingBtn;
    }
}
