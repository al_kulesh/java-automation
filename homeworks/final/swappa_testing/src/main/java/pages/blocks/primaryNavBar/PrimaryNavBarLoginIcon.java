package pages.blocks.primaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.BaseElement;
import elements.Button;
import elements.Input;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class PrimaryNavBarLoginIcon {
    private final SelenideElement root = $(By.id("primaryNav"));
    private final Button loginIcon = new Button(root.$(By.xpath("nav/div/div/div[3]/ul/li[1]/a")));
    private final SelenideElement slideLoginRoot = $(By.id("slide_login"));
    private final BaseElement slideLogin = new BaseElement($(slideLoginRoot));
    private final Input email = new Input(By.id("id_username"));
    private final Input password = new Input(By.id("id_password"));
    private final Button registerBtn = new Button(slideLoginRoot.$(By.xpath("div[2]/div[2]/div[2]/a")));

    public Button getLoginIcon() {
        return loginIcon;
    }

    public BaseElement getSlideLogin() {
        return slideLogin;
    }

    public Input getEmail() {
        return email;
    }

    public Input getPassword() {
        return password;
    }

    public Button getRegisterBtn() {
        return registerBtn;
    }
}
