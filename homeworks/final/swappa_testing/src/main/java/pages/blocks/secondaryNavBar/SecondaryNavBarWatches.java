package pages.blocks.secondaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SecondaryNavBarWatches {
    private final SelenideElement watchesRoot = $(By.xpath("//*[@id=\"secondaryNav\"]/nav/div/ul/li[5]"));
    private final SelenideElement watchesSection = watchesRoot.$(By.xpath("a"));
    private final Button watchesBtn = new Button(watchesRoot.$(By.xpath("a")));
    private final Button appleWatchDropDownBtn = new Button(watchesRoot.$(By.xpath("ul/li[1]/a")));
    private final Button androidWatchDropDownBtn = new Button(watchesRoot.$(By.xpath("ul/li[2]/a")));
    private final Label AllWatchesPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='Watches and Wearables']")));
    private final Label appleWatchPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='Apple Watch Sale']")));
    private final Label androidWatchPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='Android Watch']")));

    public SelenideElement getWatchesSection() {
        return watchesSection;
    }

    public Button getWatchesBtn() {
        return watchesBtn;
    }

    public Label getAllWatchesPageLabel() {
        return AllWatchesPageLabel;
    }

    public Button getAppleWatchDropDownBtn() {
        return appleWatchDropDownBtn;
    }

    public Button getAndroidWatchDropDownBtn() {
        return androidWatchDropDownBtn;
    }

    public Label getAppleWatchPageLabel() {
        return appleWatchPageLabel;
    }

    public Label getAndroidWatchPageLabel() {
        return androidWatchPageLabel;
    }
}
