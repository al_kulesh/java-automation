package pages.blocks.primaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.BaseElement;
import elements.Button;
import elements.SearchBar;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class PrimaryNavBarSearchIcon {
    private final SelenideElement root = $(By.id("primaryNav"));
    private final Button searchIcon = new Button(root.$(By.xpath("nav/div/div/div[3]/ul/li[3]/a")));
    private final SelenideElement slideSearchRoot = $(By.id("slide_search_form"));
    private final BaseElement slideSearch = new BaseElement($(slideSearchRoot));
    private final SearchBar searchBar = new SearchBar(slideSearchRoot.$(By.xpath("div/input")));
    private final Button searchBtn = new Button(slideSearchRoot.$(By.xpath("div/button")));

    public Button getSearchIcon() {
        return searchIcon;
    }

    public BaseElement getSlideSearch() {
        return slideSearch;
    }

    public SearchBar getSearchBar() {
        return searchBar;
    }

    public Button getSearchBtn() {
        return searchBtn;
    }
}
