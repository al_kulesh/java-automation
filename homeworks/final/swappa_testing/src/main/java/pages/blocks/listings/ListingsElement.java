package pages.blocks.listings;

import com.codeborne.selenide.SelenideElement;
import elements.Label;
import org.openqa.selenium.By;

import static utils.Assertions.isVisible;

import org.assertj.core.api.SoftAssertions;


public class ListingsElement {
    private final Label conditionLabel;
    private final Label colorLabel;
    private final Label storageLabel;

    public ListingsElement(SelenideElement root) {
        this.conditionLabel = new Label(root.$(By.xpath("div/div[2]/div[2]/ul/li[text()=' Good ']")));
        this.colorLabel = new Label(root.$(By.xpath("div/div[2]/div[2]/ul/li[text()=' Black ']")));
        this.storageLabel = new Label(root.$(By.xpath("div/div[2]/div[2]/ul/li[text()=' 128 GB ']")));
    }

    public void labelsIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, conditionLabel, "Condition label");
        isVisible(softAssertion, colorLabel, "Color label");
        isVisible(softAssertion, storageLabel, "Storage label");
    }
}
