package pages.blocks.secondaryNavBar;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SecondaryNavBarTablets {
    private final SelenideElement tabletsRoot = $(By.xpath("//*[@id=\"secondaryNav\"]/nav/div/ul/li[6]"));
    private final SelenideElement tabletsSection = tabletsRoot.$(By.xpath("a"));
    private final Button tabletsBtn = new Button(tabletsRoot.$(By.xpath("a")));
    private final Button iPadDropDownBtn = new Button(tabletsRoot.$(By.xpath("ul/li[1]/a")));
    private final Label allTabletsPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/h1[text()='Tablets For Sale']")));
    private final Label iPadPageLabel = new Label($(By.xpath("//*[@id=\"section_top\"]/div/div[1]/h1[text()='iPads For Sale']")));

    public SelenideElement getTabletsSection() {
        return tabletsSection;
    }

    public Button getTabletsBtn() {
        return tabletsBtn;
    }

    public Label getAllTabletsPageLabel() {
        return allTabletsPageLabel;
    }

    public Button getiPadDropDownBtn() {
        return iPadDropDownBtn;
    }

    public Label getiPadPageLabel() {
        return iPadPageLabel;
    }
}
