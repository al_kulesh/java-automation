package pages.blocks.primaryNavBar;

import elements.Button;
import org.openqa.selenium.By;


public class PrimaryNavBarSwappaLogo {
    private final Button swappaLogo = new Button(By.xpath("//*[@id=\"primaryNav\"]/nav/div/div/div[2]/a"));

    public Button getSwappaLogo() {
        return swappaLogo;
    }
}