package pages.blocks.catalogs;

import com.codeborne.selenide.SelenideElement;
import elements.Label;
import org.openqa.selenium.By;

public class AllPhonesCatalog {
    private final Label label;

    public AllPhonesCatalog(SelenideElement root) {
        this.label = new Label(root.$(By.xpath("div/h3/a")));
    }

    public Label getLabel() {
        return label;
    }
}
