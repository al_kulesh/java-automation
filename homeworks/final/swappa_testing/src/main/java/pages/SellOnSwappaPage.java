package pages;

import elements.Button;
import elements.SearchBar;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SellOnSwappaPage {
    private final SearchBar sellSearchBar = new SearchBar($(By.id("id_search")));
    private final Button sellSearchBarFindButton = new Button($(By.id("form_search_button")));

    public SearchBar getSellSearchBar() {
        return sellSearchBar;
    }

    public Button getSellSearchBarFindButton() {
        return sellSearchBarFindButton;
    }
}
