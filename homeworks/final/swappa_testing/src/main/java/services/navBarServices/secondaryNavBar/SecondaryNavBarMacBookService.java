package services.navBarServices.secondaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.secondaryNavBar.SecondaryNavBarMacBooks;

import static com.codeborne.selenide.Selenide.actions;
import static utils.Assertions.isVisible;

public class SecondaryNavBarMacBookService {
    private final SecondaryNavBarMacBooks secondaryNavBarMacBooks = new SecondaryNavBarMacBooks();

    public void clickMacBooksLink() {
        secondaryNavBarMacBooks.getMacsBtn().click();
    }

    public void macBooksPageIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, secondaryNavBarMacBooks.getAllMacBooksPageLabel(), "Apple MacBooks label");
    }

    public void clickMacProSection() {
        actions().moveToElement(secondaryNavBarMacBooks.getMacsSection()).perform();
        secondaryNavBarMacBooks.getMacBookProDropDownBtn().click();
    }

    public void macProPageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarMacBooks.getMacBookProPageLabel(), "MacBook Pro label");
    }

    public void clickMacAirSection() {
        actions().moveToElement(secondaryNavBarMacBooks.getMacsSection()).perform();
        secondaryNavBarMacBooks.getMacBookAirDropDownBtn().click();
    }

    public void macAirPageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarMacBooks.getMacBookAirPageLabel(), "MacBook Air label");
    }
}
