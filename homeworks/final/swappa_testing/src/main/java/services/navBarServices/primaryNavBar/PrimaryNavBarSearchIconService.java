package services.navBarServices.primaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.primaryNavBar.PrimaryNavBarSearchIcon;

import static utils.Assertions.isVisible;

public class PrimaryNavBarSearchIconService {
    private final PrimaryNavBarSearchIcon primaryNavBarSearchIcon = new PrimaryNavBarSearchIcon();
    public void clickSearchIcon(){
        primaryNavBarSearchIcon.getSearchIcon().click();
    }

    public void slideSearchIsShown(SoftAssertions softAssertion){
        isVisible(softAssertion, primaryNavBarSearchIcon.getSlideSearch(), "Slide Search");
    }

    public void searchBarWork(){
        primaryNavBarSearchIcon.getSearchBar().typeText("Samsung Galaxy S9");
        primaryNavBarSearchIcon.getSearchBtn().click();
    }
}
