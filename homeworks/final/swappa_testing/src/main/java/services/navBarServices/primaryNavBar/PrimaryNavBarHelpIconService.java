package services.navBarServices.primaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.primaryNavBar.PrimaryNavBarHelpIcon;

import static utils.Assertions.isVisible;

public class PrimaryNavBarHelpIconService {
    private final PrimaryNavBarHelpIcon primaryNavBarHelpIcon = new PrimaryNavBarHelpIcon();

    public void helpButtonClick() {
        primaryNavBarHelpIcon.getHelpIcon().click();
    }

    public void slideSellIsShown(SoftAssertions softAssertion){
        isVisible(softAssertion, primaryNavBarHelpIcon.getSlideHelp(), "Slide Help");
    }
    public void clickSubmitHelpRequestButton() {
        primaryNavBarHelpIcon.getHelpRequestBtn().click();
    }
}
