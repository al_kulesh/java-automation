package services.navBarServices.primaryNavBar;

import pages.blocks.primaryNavBar.PrimaryNavBarSwappaLogo;

public class PrimaryNavBarSwappaLogoService {
    private final PrimaryNavBarSwappaLogo primaryNavBarSwappaLogo = new PrimaryNavBarSwappaLogo();

    public void swappaLogoClick() {
        primaryNavBarSwappaLogo.getSwappaLogo().click();
    }

}
