package services.navBarServices.primaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.primaryNavBar.PrimaryNavBarLoginIcon;

import static com.codeborne.selenide.Selenide.sleep;
import static utils.Assertions.isVisible;

public class PrimaryNavBarLoginIconService {
    private final PrimaryNavBarLoginIcon primaryNavBarLoginIcon = new PrimaryNavBarLoginIcon();

    public void loginIconClick() {
        primaryNavBarLoginIcon.getLoginIcon().click();
    }

    public void loginSlideIsShown(SoftAssertions softAssertion){
        isVisible(softAssertion, primaryNavBarLoginIcon.getSlideLogin(), "Slide Login");
    }

    public void fillLoginForm(String email, String password) {
        sleep(2000);
        primaryNavBarLoginIcon.getEmail().clearAndType(email);
        sleep(2000);
        primaryNavBarLoginIcon.getPassword().clearAndType(password);
    }

    public void registerButtonClick(){
        primaryNavBarLoginIcon.getRegisterBtn().click();
    }
}
