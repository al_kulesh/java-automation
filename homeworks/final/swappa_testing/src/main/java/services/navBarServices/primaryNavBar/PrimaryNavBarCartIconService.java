package services.navBarServices.primaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.primaryNavBar.PrimaryNavBarCartIcon;

import static utils.Assertions.isVisible;

public class PrimaryNavBarCartIconService {
    private final PrimaryNavBarCartIcon primaryNavBarCartIcon = new PrimaryNavBarCartIcon();

    public void cartIconClick() {
        primaryNavBarCartIcon.getCartIcon().click();
    }

    public void slideCartIsShown(SoftAssertions softAssertion){
        isVisible(softAssertion, primaryNavBarCartIcon.getSlideCart(), "Slide Cart");
    }
    public void viewCartButtonClick() {
        primaryNavBarCartIcon.getViewCartBtn().click();
    }
}
