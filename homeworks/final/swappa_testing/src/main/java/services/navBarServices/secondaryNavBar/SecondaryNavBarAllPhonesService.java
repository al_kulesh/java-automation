package services.navBarServices.secondaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.secondaryNavBar.SecondaryNavBarAllPhones;

import static com.codeborne.selenide.Selenide.actions;
import static utils.Assertions.isVisible;

public class SecondaryNavBarAllPhonesService {
    private final SecondaryNavBarAllPhones secondaryNavBarAllPhones = new SecondaryNavBarAllPhones();

    public void clickAllPhonesLink() {
        secondaryNavBarAllPhones.getAllPhonesBtn().click();
    }

    public void phoneBrandsAreDisplayed(SoftAssertions softAssertion) {
        final int size = secondaryNavBarAllPhones.getAllPhonesSectionCollection().size();
        for (int i = 0; i < size; i++) {
            isVisible(softAssertion, secondaryNavBarAllPhones.getAllPhonesSectionCollection().getModel(i).getLabel(), "Phones Catalog label");
        }
    }

    public void allPhonePageIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, secondaryNavBarAllPhones.getPhonesForSaleLabel(), "Phones for sale label");
    }

    public void samsungSectionSelectFromDropDown() {
        actions().moveToElement(secondaryNavBarAllPhones.getAllPhonesSection()).perform();
        secondaryNavBarAllPhones.getSamsungDropDownBtn().click();
    }

    public void clickVerizonSection() {
        actions().moveToElement(secondaryNavBarAllPhones.getAllPhonesSection()).perform();
        secondaryNavBarAllPhones.getVerizonDropDownBtn().click();
    }

    public void verizonPageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarAllPhones.getVerizonPageLabel(), "Verizon label");
    }

    public void clickTMobileSection() {
        actions().moveToElement(secondaryNavBarAllPhones.getAllPhonesSection()).perform();
        secondaryNavBarAllPhones.gettMobileDropDownBtn().click();
    }

    public void tMobilePageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarAllPhones.gettMobilePageLabel(), "T-Mobile label");
    }
}
