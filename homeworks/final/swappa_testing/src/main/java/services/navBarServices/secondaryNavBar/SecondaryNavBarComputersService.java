package services.navBarServices.secondaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.secondaryNavBar.SecondaryNavBarComputers;

import static com.codeborne.selenide.Selenide.actions;
import static utils.Assertions.isVisible;

public class SecondaryNavBarComputersService {
    private final SecondaryNavBarComputers secondaryNavBarComputers = new SecondaryNavBarComputers();

    public void clickComputersLink() {
        secondaryNavBarComputers.getComputersBtn().click();
    }

    public void computersPageIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, secondaryNavBarComputers.getComputersPageLabel(), "All Things Computer label");
    }

    public void clickChromeSection() {
        actions().moveToElement(secondaryNavBarComputers.getComputersSection()).perform();
        secondaryNavBarComputers.getChromeBooksDropDownBtn().click();
    }

    public void chromeBookPageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarComputers.getChromeBooksPageLabel(), "Chromebook label");
    }

    public void clickWindowsSection() {
        actions().moveToElement(secondaryNavBarComputers.getComputersSection()).perform();
        secondaryNavBarComputers.getWindowsDropDownBtn().click();
    }

    public void windowsPageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarComputers.getWindowsPageLabel(), "Windows label");
    }
}
