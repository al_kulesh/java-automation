package services.navBarServices.primaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.primaryNavBar.PrimaryNavBarSellIcon;

import static utils.Assertions.isVisible;

public class PrimaryNavBarSellIconService {
    private final PrimaryNavBarSellIcon primaryNavBarSellIcon = new PrimaryNavBarSellIcon();

    public void clickSellIcon(){
        primaryNavBarSellIcon.getSellIcon().click();
    }

    public void slideSellIsShown(SoftAssertions softAssertion){
        isVisible(softAssertion, primaryNavBarSellIcon.getSlideSell(), "Slide Sell");
    }
    public void startSellButtonClick() {
        primaryNavBarSellIcon.getStartSellingBtn().click();
    }

}
