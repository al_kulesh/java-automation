package services.navBarServices.secondaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.secondaryNavBar.SecondaryNavBarWatches;

import static com.codeborne.selenide.Selenide.actions;
import static utils.Assertions.isVisible;

public class SecondaryNavBarWatchesService {
    private final SecondaryNavBarWatches secondaryNavBarWatches = new SecondaryNavBarWatches();

    public void clickWatchesSectionLink() {
        secondaryNavBarWatches.getWatchesBtn().click();
    }

    public void watchesPageIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, secondaryNavBarWatches.getAllWatchesPageLabel(), "Watches and Wearables label");
    }

    public void clickAppleWatchSection() {
        actions().moveToElement(secondaryNavBarWatches.getWatchesSection()).perform();
        secondaryNavBarWatches.getAppleWatchDropDownBtn().click();
    }

    public void appleWatchPageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarWatches.getAppleWatchPageLabel(), "Apple Watch label");
    }

    public void clickAndroidWatchSection() {
        actions().moveToElement(secondaryNavBarWatches.getWatchesSection()).perform();
        secondaryNavBarWatches.getAndroidWatchDropDownBtn().click();
    }

    public void androidWatchPageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarWatches.getAndroidWatchPageLabel(), "Android Watch label");
    }
}