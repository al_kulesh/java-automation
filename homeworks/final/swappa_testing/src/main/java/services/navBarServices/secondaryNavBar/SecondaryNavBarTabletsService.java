package services.navBarServices.secondaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.secondaryNavBar.SecondaryNavBarTablets;

import static com.codeborne.selenide.Selenide.actions;
import static utils.Assertions.isVisible;

public class SecondaryNavBarTabletsService {
    private final SecondaryNavBarTablets secondaryNavBarTablets = new SecondaryNavBarTablets();

    public void clickTabletsLink() {
        secondaryNavBarTablets.getTabletsBtn().click();
    }

    public void tablesPageIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, secondaryNavBarTablets.getAllTabletsPageLabel(), "Tablets label");
    }
    public void clickIPadSection() {
        actions().moveToElement(secondaryNavBarTablets.getTabletsSection()).perform();
        secondaryNavBarTablets.getiPadDropDownBtn().click();
    }
    public void iPadPageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarTablets.getiPadPageLabel(), "Ipad label");
    }
}
