package services.navBarServices.secondaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.secondaryNavBar.SecondaryNavBarIphones;

import static com.codeborne.selenide.Selenide.actions;
import static utils.Assertions.isVisible;


public class SecondaryNavBarIphonesService {
    private final SecondaryNavBarIphones secondaryNavBarIphones = new SecondaryNavBarIphones();

    public void clickIPhonesLink() {
        secondaryNavBarIphones.getiPhonesBtn().click();
    }

    public void allIphoneModelsAreDisplayed(SoftAssertions softAssertion) {
        final int size = secondaryNavBarIphones.getAllIPhonesSectionCollection().size();
        for (int i = 0; i < size; i++) {
            isVisible(softAssertion, secondaryNavBarIphones.getAllIPhonesSectionCollection().getModel(i).getLabel(), "iPhones Catalog label");
        }
    }

    public void allIphonePageIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, secondaryNavBarIphones.getiPhonesForSalePageLabel(), "iPhones for sale label");
    }

    public void clickIphone8PlusSection() {
        actions().moveToElement(secondaryNavBarIphones.getiPhonesSectionAll()).perform();
        secondaryNavBarIphones.getiPhones8PlusDropDownBtn().click();
    }

    public void iPhone8PlusPageIsShown(SoftAssertions softAssertions){
        isVisible(softAssertions, secondaryNavBarIphones.getiPhone8PlusPageLabel(), "Apple iPhone 8 Plus label");
    }

    public void clickIphone12Section() {
        actions().moveToElement(secondaryNavBarIphones.getiPhonesSectionAll()).perform();
        secondaryNavBarIphones.getiPhones12DropDownBtn().click();
    }

    public void iPhone12PageIsShown(SoftAssertions softAssertions){
        isVisible(softAssertions, secondaryNavBarIphones.getiPhone12PageLabel(), "Apple iPhone 8 Plus label");
    }

}