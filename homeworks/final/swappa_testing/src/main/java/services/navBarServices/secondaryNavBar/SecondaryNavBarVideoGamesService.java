package services.navBarServices.secondaryNavBar;

import org.assertj.core.api.SoftAssertions;
import pages.blocks.secondaryNavBar.SecondaryNavBarVideoGames;

import static com.codeborne.selenide.Selenide.actions;
import static utils.Assertions.isVisible;

public class SecondaryNavBarVideoGamesService {
    private final SecondaryNavBarVideoGames secondaryNavBarVideoGames = new SecondaryNavBarVideoGames();

    public void clickVideoGamesLink() {
        secondaryNavBarVideoGames.getVideoGamesBtn().click();
    }

    public void videoGamesPageIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, secondaryNavBarVideoGames.getVideoGamesPageLabel(), "Gaming label");
    }

    public void clickPlayStation4Section() {
        actions().moveToElement(secondaryNavBarVideoGames.getVideoGamesSection()).perform();
        secondaryNavBarVideoGames.getPlayStation4DropDownBtn().click();
    }

    public void playStation4PageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarVideoGames.getPlayStation4PageLabel(), "PlayStation 4 label");
    }

    public void clickGamesSection() {
        actions().moveToElement(secondaryNavBarVideoGames.getVideoGamesSection()).perform();
        secondaryNavBarVideoGames.getGamesDropDownBtn().click();
    }

    public void gamesPageIsShown(SoftAssertions softAssertions) {
        isVisible(softAssertions, secondaryNavBarVideoGames.getGamesPageLabel(), "Games label");
    }
}
