package services.pageServices;

import org.assertj.core.api.SoftAssertions;
import pages.AllModelsPage;

import static utils.Assertions.isVisible;

public class AllModelsPageService {
    private final AllModelsPage allModelsPage = new AllModelsPage();

    public void galaxyS10ButtonClick() {
        allModelsPage.getGalaxyS10().click();
    }

    public void samsungLabelIsDisplayed(SoftAssertions softAssertion) {
        isVisible(softAssertion, allModelsPage.getSamsungPhonesLabel(), "Samsung Galaxy Phones label");
    }
}
