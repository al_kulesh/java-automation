package services.pageServices;

import org.assertj.core.api.SoftAssertions;
import pages.ModelPage;

import static utils.Assertions.isVisible;


public class ModelPageService {
    private final ModelPage modelPage = new ModelPage();

    public void planUnlockedClick() {
        modelPage.getUnlocked().click();
    }

    public void samsungLabelIsDisplayed(SoftAssertions softAssertion){
        isVisible(softAssertion, modelPage.getSamsungLabel(), "Samsung Galaxy S10 label");
    }

    public void sellIPhone7LabelIsDisplayed(SoftAssertions softAssertion){
        isVisible(softAssertion, modelPage.getSellIPhone7Label(), "Sell iPhone 7 label");
    }

    public void loginToSellClick(){
        modelPage.getLoginToSellBtn().click();
    }
}
