package services.pageServices;

import org.assertj.core.api.SoftAssertions;
import pages.SellOnSwappaPage;

import static utils.Assertions.isVisible;

public class SellOnSwappaPageService {
    private final SellOnSwappaPage sellOnSwappaPage = new SellOnSwappaPage();

    public void searchBarisDisplayed(SoftAssertions softAssertion){
        isVisible(softAssertion, sellOnSwappaPage.getSellSearchBar(), "Sell on swappa Search-bar");
    }

    public void searchBarWork(String input) {
        sellOnSwappaPage.getSellSearchBar().typeText(input);
        sellOnSwappaPage.getSellSearchBarFindButton().click();
    }
}
