package services.pageServices;

import elements.Button;
import org.assertj.core.api.SoftAssertions;
import pages.Page;
import pages.blocks.secondaryNavBar.SecondaryNavBarAllPhones;
import pages.blocks.secondaryNavBar.SecondaryNavBarComputers;
import pages.blocks.secondaryNavBar.SecondaryNavBarTablets;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static utils.Assertions.isVisible;

public class PageService {
    private final Page page = new Page();
    private final SecondaryNavBarAllPhones secondaryNavBarAllPhones = new SecondaryNavBarAllPhones();
    private final SecondaryNavBarComputers secondaryNavBarComputers = new SecondaryNavBarComputers();
    private final SecondaryNavBarTablets secondaryNavBarTablets = new SecondaryNavBarTablets();

    public void isPageShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, page.getBreadcrumbs(), "Breadcrumbs");
        isVisible(softAssertion, page.getAnnouncements(), "Announcements");
        isVisible(softAssertion, page.getSection_trailer(), "Section_trailer");
    }

    public void openRandomPage() {
        List<Button> pageList = new ArrayList<>();
        Random random = new Random();
        pageList.add(secondaryNavBarAllPhones.getAllPhonesBtn());
        pageList.add(secondaryNavBarComputers.getComputersBtn());
        pageList.add(secondaryNavBarTablets.getTabletsBtn());
        final int size = pageList.size();
        pageList.get(random.nextInt(size)).click();
    }
}
