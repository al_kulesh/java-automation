package services.pageServices;

import org.assertj.core.api.SoftAssertions;
import pages.LoginPage;

import static utils.Assertions.isVisible;

public class LoginPageService {
    private final LoginPage loginPage = new LoginPage();

    public void loginPageIsShown(SoftAssertions softAssertion){
        isVisible(softAssertion, loginPage.getEmail(), "Email");
        isVisible(softAssertion, loginPage.getEmail(), "Password");
    }

    public void fillLoginForm(String email, String password) {
        loginPage.getEmail().clearAndType(email);
        loginPage.getPassword().clearAndType(password);
    }
}
