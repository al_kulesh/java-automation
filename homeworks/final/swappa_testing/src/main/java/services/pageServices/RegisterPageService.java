package services.pageServices;

import org.assertj.core.api.SoftAssertions;
import pages.RegisterPage;

import static utils.Assertions.isVisible;

public class RegisterPageService {
    private final RegisterPage registerPage = new RegisterPage();

    public void registerPageIsShown(SoftAssertions softAssertions){
        isVisible(softAssertions, registerPage.getEmail(), "Email");
        isVisible(softAssertions, registerPage.getFirstName(), "First Name");
        isVisible(softAssertions, registerPage.getLastName(), "Last Name");
        isVisible(softAssertions, registerPage.getPassword(), "Password");
    }

    public void fillRegisterForm(String email, String firstName, String lastName, String password) {
        registerPage.getEmail().clearAndType(email);
        registerPage.getFirstName().clearAndType(firstName);
        registerPage.getLastName().clearAndType(lastName);
        registerPage.getPassword().clearAndType(password);
    }
}
