package services.pageServices;

import org.assertj.core.api.SoftAssertions;
import pages.HomePage;

import static utils.Assertions.isVisible;

public class HomePageService {
    private final HomePage homePage = new HomePage();

    public void mainBlocksIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, homePage.getHeader(), "Header");
        isVisible(softAssertion, homePage.getMainContainer(), "Container");
        isVisible(softAssertion, homePage.getFooter(), "Footer");
    }

    public void buyAndSellLabelIsDisplayed(SoftAssertions softAssertion){
        isVisible(softAssertion, homePage.getWelcomeLabel(), "Welcome to the safest marketplace for used tech");
    }
}
