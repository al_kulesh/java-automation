package services.pageServices;

import static utils.Assertions.isVisible;
import org.assertj.core.api.SoftAssertions;
import pages.OneModelCatalogPage;

public class OneModelCatalogPageService {
    OneModelCatalogPage oneModelCatalogPage = new OneModelCatalogPage();

    public void navBarSearchResultPageIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, oneModelCatalogPage.getSearchResultLabel(), "Samsung Galaxy S9 search result");
    }

    public void  sellOnSwappaNavBarSearchResultPageIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, oneModelCatalogPage.getSellSearchResultLabel(), "Iphone 7 search result");
    }

    public void chooseIphone7(){
        oneModelCatalogPage.getiPhone7Btn().click();
    }
}
