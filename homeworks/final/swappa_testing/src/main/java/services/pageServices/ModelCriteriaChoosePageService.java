package services.pageServices;

import org.assertj.core.api.SoftAssertions;
import pages.ModelCriteriaChoosePage;

import java.util.Random;

import static com.codeborne.selenide.Selenide.sleep;
import static utils.Assertions.isVisible;


public class ModelCriteriaChoosePageService {
    private final ModelCriteriaChoosePage modelCriteriaChoosePage = new ModelCriteriaChoosePage();

    public void filterBy()  {
        sleep(2000);
        modelCriteriaChoosePage.getExpressShipping().check();
        sleep(2000);
        modelCriteriaChoosePage.getConditionFilter().check();
        sleep(2000);
        modelCriteriaChoosePage.getColorFilter().check();
        sleep(2000);
        modelCriteriaChoosePage.getStorageFilter().check();
        sleep(2000);
        modelCriteriaChoosePage.getSortBy().check();
        modelCriteriaChoosePage.getFilterClose().click();
    }

    public void checkBoxAndRadioAreDisplayed(SoftAssertions softAssertion){
        modelCriteriaChoosePage.getFilterListings().click();
        sleep(2000);
        isVisible(softAssertion, modelCriteriaChoosePage.getExpressShipping(), "Express Shipping checkbox");
        isVisible(softAssertion, modelCriteriaChoosePage.getConditionFilter(), "Condition checkbox");
        isVisible(softAssertion, modelCriteriaChoosePage.getColorFilter(), "Color checkbox");
        isVisible(softAssertion, modelCriteriaChoosePage.getStorageFilter(), "Storage checkbox");
        isVisible(softAssertion, modelCriteriaChoosePage.getSortBy(), "Sort by radio");
    }

    public void checkRandomFilteredProduct(SoftAssertions softAssertion) {
        Random random = new Random();
        final int size = modelCriteriaChoosePage.getListingsElementCollection().size();
        if (size > 0) {
            modelCriteriaChoosePage.getListingsElementCollection().getModel(random.nextInt(size)).labelsIsShown(softAssertion);
        } else {
            System.out.println("No listings were found");
        }
    }
}