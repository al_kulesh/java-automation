package services.pageServices;

import enums.Subject;
import org.assertj.core.api.SoftAssertions;
import pages.HelpPage;

import static utils.Assertions.isVisible;

public class HelpPageService {
    private final HelpPage helpPage = new HelpPage();

    public void helpPageIsShown(SoftAssertions softAssertion) {
        isVisible(softAssertion, helpPage.getEmail(), "Email");
        isVisible(softAssertion, helpPage.getSubject(), "Subject");
        isVisible(softAssertion, helpPage.getMessage(), "Message");
        isVisible(softAssertion, helpPage.getLink(), "Link");
        isVisible(softAssertion, helpPage.getCaptcha(), "Captcha");
    }

    public void helpFormFill(String email, Subject subject, String message, String link) {
        helpPage.getEmail().clearAndType(email);
        helpPage.getSubject().select(subject.getSubjectName());
        helpPage.getMessage().clearAndType(message);
        helpPage.getLink().clearAndType(link);
        helpPage.getCaptcha().check();
    }
}
