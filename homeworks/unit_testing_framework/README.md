# Задание
 
## Максимально покрыть тестами библиотеку калькулятора

# Основные классы/пакеты
- [CalculatorTestingNg](CalculatorTestingNg) - пакет содержащий набор тестов
- [PrimitiveTestsSum.java](CalculatorTestingNg/src/test/java/tests/PrimitiveTestsSum.java) - набор примитивных тестов для методов суммирования
- [PrimitiveTestsSubtraction.java](CalculatorTestingNg/src/test/java/tests/PrimitiveTestsSubtraction.java) - набор примитивных тестов для методов вычитания
- [PrimitiveTestsMultiplication.java](CalculatorTestingNg/src/test/java/tests/PrimitiveTestsMultiplication.java) - набор примитивных тестов для методов умножения 
- [PrimitiveTestsDivision.java](CalculatorTestingNg/src/test/java/tests/PrimitiveTestsDivision.java) - набор примитивных тестов для методов деления
- [SomeReflectionTests.java](CalculatorTestingNg/src/test/java/tests/SomeReflectionTests.java) - несколько тестов сделанных при помощи рефлексии
- [MyMethodListener.java](CalculatorTestingNg/src/test/java/tests/PrimitiveTestsListener.java) - Listner для примитивных тестов
- [DataProviderTests.java](CalculatorTestingNg/src/test/java/tests/DataProviderTests.java) - набор тестов использующих Dataprovider, приведенных к int

# Скриншоты успешного выполнения
- [Primitive sum tests work](images/PrimitiveSumTest.png)
- [Primitive subtraction tests work](images/PrimitiveSubtractionTest.png)
- [Primitive multiplication tests work](images/PrimitiveMultiplicationTest.png)
- [Primitive division tests work](images/PrimitiveDivisionTest.png)
- [Some reflection tests work](images/SomeReflectionTests.png)
- [Dataprovider sum tests work](images/DataProvidetSum.png)
- [Dataprovider subtraction tests work](images/DataProvidetSubtraction.png)
- [Dataprovider multiplication tests work](images/DataProvidetMultiplication.png)
- [Dataprovider division tests work](images/DataProvidetDivision.png)