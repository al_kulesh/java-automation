package tests;

import dzmitry.klokau.testing.Calculator;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class DataProviderTests {
    Calculator calculator = new Calculator();

    @DataProvider(name = "createSomeDataSum")
    public Object[][] createSomeDataSum() {
        return new Object[][]{
                {"SumIntInt", (int) calculator.sum(1, 2), 3},
                {"SumDoubleDouble", (int) calculator.sum(1, 2), 3},
                {"SumFloatFloat", (int) calculator.sum(1, 2), 3},
                {"SumStringString", (int) calculator.sum("1", "2"), 3},
                {"SumShortShort", (int) calculator.sum(1, 2), 3},
                {"SumCharChar", (int) calculator.sum('1', '2'), 3},
                {"SumByteByte", (int) calculator.sum(1, 2), 3},
                {"SumLongLong", (int) calculator.sum(1, 2), 3},
                {"SumIntDouble", (int) calculator.sum(1, 2), 3},
                {"SumIntFloat", (int) calculator.sum(1, 2), 3},
                {"SumIntString", (int) calculator.sum(1, "2"), 3},
                {"SumIntShort", (int) calculator.sum(1, 2), 3},
                {"SumIntChar", (int) calculator.sum(1, '2'), 3},
                {"SumIntByte", (int) calculator.sum(1, 2), 3},
                {"SumIntLong", (int) calculator.sum(1, 2), 3},
                {"SumDoubleInt", (int) calculator.sum(1, 2), 3},
                {"SumFloatInt", (int) calculator.sum(1, 2), 3},
                {"SumStringInt", (int) calculator.sum("1", 2), 3},
                {"SumShortInt", (int) calculator.sum(1, 2), 3},
                {"SumCharInt", (int) calculator.sum('1', 2), 3},
                {"SumByteInt", (int) calculator.sum(1, 2), 3},
                {"SumLongInt", (int) calculator.sum(1, 2), 3},
                {"SumDoubleFloat", (int) calculator.sum(1, 2), 3},
                {"SumDoubleString", (int) calculator.sum(1, "2"), 3},
                {"SumDoubleShort", (int) calculator.sum(1, 2), 3},
                {"SumDoubleChar", (int) calculator.sum(1, '2'), 3},
                {"SumDoubleByte", (int) calculator.sum(1, 2), 3},
                {"SumDoubleLong", (int) calculator.sum(1, 2), 3},
                {"SumLongDouble", (int) calculator.sum(1, 2), 3},
                {"SumFloatDouble", (int) calculator.sum(1, 2), 3},
                {"SumStringDouble", (int) calculator.sum("1", 2), 3},
                {"SumShortDouble", (int) calculator.sum(1, 2), 3},
                {"SumCharDouble", (int) calculator.sum('1', 2), 3},
                {"SumByteDouble", (int) calculator.sum(1, 2), 3},
                {"SumFloatString", (int) calculator.sum(1, "2"), 3},
                {"SumFloatShort", (int) calculator.sum(1, 2), 3},
                {"SumFloatChar", (int) calculator.sum(1, '2'), 3},
                {"SumFloatByte", (int) calculator.sum(1, 2), 3},
                {"SumFloatLong", (int) calculator.sum(1, 2), 3},
                {"SumLongFloat", (int) calculator.sum(1, 2), 3},
                {"SumStringFloat", (int) calculator.sum("1", 2), 3},
                {"SumShortFloat", (int) calculator.sum(1, 2), 3},
                {"SumCharFloat", (int) calculator.sum(1, '2'), 3},
                {"SumByteFloat", (int) calculator.sum(1, 2), 3},
                {"SumStringShort", (int) calculator.sum("1", 2), 3},
                {"SumStringChar", (int) calculator.sum("1", '2'), 3},
                {"SumStringByte", (int) calculator.sum("1", 2), 3},
                {"SumStringLong", (int) calculator.sum("1", 2), 3},
                {"SumLongString", (int) calculator.sum(1, "2"), 3},
                {"SumShortString", (int) calculator.sum(1, "2"), 3},
                {"SumCharString", (int) calculator.sum('1', "2"), 3},
                {"SumByteString", (int) calculator.sum(1, "2"), 3},
                {"SumShortChar", (int) calculator.sum(1, '2'), 3},
                {"SumShortByte", (int) calculator.sum(1, 2), 3},
                {"SumShortLong", (int) calculator.sum(1, 2), 3},
                {"SumLongShort", (int) calculator.sum(1, 2), 3},
                {"SumCharShort", (int) calculator.sum('1', 2), 3},
                {"SumByteShort", (int) calculator.sum(1, 2), 3},
                {"SumCharByte", (int) calculator.sum('1', 2), 3},
                {"SumCharLong", (int) calculator.sum('1', 2), 3},
                {"SumLongChar", (int) calculator.sum(1, '2'), 3},
                {"SumByteChar", (int) calculator.sum(1, '2'), 3},
        };
    }

    @Test(dataProvider = "createSomeDataSum")
    public void testSum(String methodName, int a, int expectedValue) {
        System.out.println(methodName);
        assertEquals(expectedValue, a);
    }

    @DataProvider(name = "createSomeDataSubtraction")
    public Object[][] createSomeDataSubtraction() {
        return new Object[][]{
                {"SubtractionIntInt", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionDoubleDouble", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionFloatFloat", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionStringString", (int) calculator.subtraction("5", "2"), 3},
                {"SubtractionShortShort", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionCharChar", (int) calculator.subtraction('5', '2'), 3},
                {"SubtractionByteByte", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionLongLong", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionIntDouble", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionIntFloat", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionIntString", (int) calculator.subtraction(5, "2"), 3},
                {"SubtractionIntShort", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionIntChar", (int) calculator.subtraction(5, '2'), 3},
                {"SubtractionIntByte", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionIntLong", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionDoubleInt", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionFloatInt", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionStringInt", (int) calculator.subtraction("5", 2), 3},
                {"SubtractionShortInt", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionCharInt", (int) calculator.subtraction('5', 2), 3},
                {"SubtractionByteInt", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionLongInt", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionDoubleFloat", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionDoubleString", (int) calculator.subtraction(5, "2"), 3},
                {"SubtractionDoubleShort", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionDoubleChar", (int) calculator.subtraction(5, '2'), 3},
                {"SubtractionDoubleByte", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionDoubleLong", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionLongDouble", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionFloatDouble", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionStringDouble", (int) calculator.subtraction("5", 2), 3},
                {"SubtractionShortDouble", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionCharDouble", (int) calculator.subtraction('5', 2), 3},
                {"SubtractionByteDouble", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionFloatString", (int) calculator.subtraction(5, "2"), 3},
                {"SubtractionFloatShort", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionFloatChar", (int) calculator.subtraction(5, '2'), 3},
                {"SubtractionFloatByte", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionFloatLong", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionLongFloat", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionStringFloat", (int) calculator.subtraction("5", 2), 3},
                {"SubtractionShortFloat", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionCharFloat", (int) calculator.subtraction(5, '2'), 3},
                {"SubtractionByteFloat", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionStringShort", (int) calculator.subtraction("5", 2), 3},
                {"SubtractionStringChar", (int) calculator.subtraction("5", '2'), 3},
                {"SubtractionStringByte", (int) calculator.subtraction("5", 2), 3},
                {"SubtractionStringLong", (int) calculator.subtraction("5", 2), 3},
                {"SubtractionLongString", (int) calculator.subtraction(5, "2"), 3},
                {"SubtractionShortString", (int) calculator.subtraction(5, "2"), 3},
                {"SubtractionCharString", (int) calculator.subtraction('5', "2"), 3},
                {"SubtractionByteString", (int) calculator.subtraction(5, "2"), 3},
                {"SubtractionShortChar", (int) calculator.subtraction(5, '2'), 3},
                {"SubtractionShortByte", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionShortLong", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionLongShort", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionCharShort", (int) calculator.subtraction('5', 2), 3},
                {"SubtractionByteShort", (int) calculator.subtraction(5, 2), 3},
                {"SubtractionCharByte", (int) calculator.subtraction('5', 2), 3},
                {"SubtractionCharLong", (int) calculator.subtraction('5', 2), 3},
                {"SubtractionLongChar", (int) calculator.subtraction(5, '2'), 3},
                {"SubtractionByteChar", (int) calculator.subtraction(5, '2'), 3},
        };
    }

    @Test(dataProvider = "createSomeDataSubtraction")
    public void testSubtraction(String methodName, int a, int expectedValue) {
        System.out.println(methodName);
        assertEquals(expectedValue, a);
    }

    @DataProvider(name = "createSomeDataMultiplication")
    public Object[][] createSomeDataMultiplication() {
        return new Object[][]{
                {"MultiplicationShortInt", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationCharInt", (int) calculator.multiplication('5', 2), 10},
                {"MultiplicationByteInt", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationLongInt", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationDoubleFloat", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationDoubleString", (int) calculator.multiplication("5", 2), 10},
                {"MultiplicationDoubleShort", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationDoubleChar", (int) calculator.multiplication(5, '2'), 10},
                {"MultiplicationDoubleByte", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationDoubleLong", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationLongDouble", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationFloatDouble", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationStringDouble", (int) calculator.multiplication("5", 2), 10},
                {"MultiplicationShortDouble", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationCharDouble", (int) calculator.multiplication('5', 2), 10},
                {"MultiplicationByteDouble", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationFloatString", (int) calculator.multiplication(5, "2"), 10},
                {"MultiplicationFloatShort", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationFloatChar", (int) calculator.multiplication(5, '2'), 10},
                {"MultiplicationFloatByte", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationFloatLong", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationLongFloat", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationStringFloat", (int) calculator.multiplication("5", 2), 10},
                {"MultiplicationShortFloat", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationCharFloat", (int) calculator.multiplication('5', 2), 10},
                {"MultiplicationByteFloat", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationStringShort", (int) calculator.multiplication("5", 2), 10},
                {"MultiplicationStringChar", (int) calculator.multiplication("5", '2'), 10},
                {"MultiplicationStringByte", (int) calculator.multiplication("5", 2), 10},
                {"MultiplicationStringLong", (int) calculator.multiplication("5", 2), 10},
                {"MultiplicationLongString", (int) calculator.multiplication(5, "2"), 10},
                {"MultiplicationShortString", (int) calculator.multiplication(5, "2"), 10},
                {"MultiplicationCharString", (int) calculator.multiplication('5', "2"), 10},
                {"MultiplicationByteString", (int) calculator.multiplication(5, "2"), 10},
                {"MultiplicationShortChar", (int) calculator.multiplication(5, '2'), 10},
                {"MultiplicationShortByte", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationShortLong", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationLongShort", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationCharShort", (int) calculator.multiplication('5', 2), 10},
                {"MultiplicationByteShort", (int) calculator.multiplication(5, 2), 10},
                {"MultiplicationCharByte", (int) calculator.multiplication('5', 2), 10},
                {"MultiplicationCharLong", (int) calculator.multiplication('5', 2), 10},
                {"MultiplicationLongChar", (int) calculator.multiplication(5, '2'), 10},
                {"MultiplicationByteChar", (int) calculator.multiplication(5, '2'), 10},
        };
    }

    @Test(dataProvider = "createSomeDataMultiplication")
    public void testMultiplication(String methodName, int a, int expectedValue) {
        System.out.println(methodName);
        assertEquals(expectedValue, a);
    }

    @DataProvider(name = "createSomeDataDivision")
    public Object[][] createSomeDataDivision() {
        return new Object[][]{
                {"DivisionShortInt", (int) calculator.division(5, 2), 10},
                {"DivisionCharInt", (int) calculator.division('5', 2), 10},
                {"DivisionByteInt", (int) calculator.division(5, 2), 10},
                {"DivisionLongInt", (int) calculator.division(5, 2), 10},
                {"DivisionDoubleFloat", (int) calculator.division(5, 2), 10},
                {"DivisionDoubleString", (int) calculator.division("5", 2), 10},
                {"DivisionDoubleShort", (int) calculator.division(5, 2), 10},
                {"DivisionDoubleChar", (int) calculator.division(5, '2'), 10},
                {"DivisionDoubleByte", (int) calculator.division(5, 2), 10},
                {"DivisionDoubleLong", (int) calculator.division(5, 2), 10},
                {"DivisionLongDouble", (int) calculator.division(5, 2), 10},
                {"DivisionFloatDouble", (int) calculator.division(5, 2), 10},
                {"DivisionStringDouble", (int) calculator.division("5", 2), 10},
                {"DivisionShortDouble", (int) calculator.division(5, 2), 10},
                {"DivisionCharDouble", (int) calculator.division('5', 2), 10},
                {"DivisionByteDouble", (int) calculator.division(5, 2), 10},
                {"DivisionFloatString", (int) calculator.division(5, "2"), 10},
                {"DivisionFloatShort", (int) calculator.division(5, 2), 10},
                {"DivisionFloatChar", (int) calculator.division(5, '2'), 10},
                {"DivisionFloatByte", (int) calculator.division(5, 2), 10},
                {"DivisionFloatLong", (int) calculator.division(5, 2), 10},
                {"DivisionLongFloat", (int) calculator.division(5, 2), 10},
                {"DivisionStringFloat", (int) calculator.division("5", 2), 10},
                {"DivisionShortFloat", (int) calculator.division(5, 2), 10},
                {"DivisionCharFloat", (int) calculator.division('5', 2), 10},
                {"DivisionByteFloat", (int) calculator.division(5, 2), 10},
                {"DivisionStringShort", (int) calculator.division("5", 2), 10},
                {"DivisionStringChar", (int) calculator.division("5", '2'), 10},
                {"DivisionStringByte", (int) calculator.division("5", 2), 10},
                {"DivisionStringLong", (int) calculator.division("5", 2), 10},
                {"DivisionLongString", (int) calculator.division(5, "2"), 10},
                {"DivisionShortString", (int) calculator.division(5, "2"), 10},
                {"DivisionCharString", (int) calculator.division('5', "2"), 10},
                {"DivisionByteString", (int) calculator.division(5, "2"), 10},
                {"DivisionShortChar", (int) calculator.division(5, '2'), 10},
                {"DivisionShortByte", (int) calculator.division(5, 2), 10},
                {"DivisionShortLong", (int) calculator.division(5, 2), 10},
                {"DivisionLongShort", (int) calculator.division(5, 2), 10},
                {"DivisionCharShort", (int) calculator.division('5', 2), 10},
                {"DivisionByteShort", (int) calculator.division(5, 2), 10},
                {"DivisionCharByte", (int) calculator.division('5', 2), 10},
                {"DivisionCharLong", (int) calculator.division('5', 2), 10},
                {"DivisionLongChar", (int) calculator.division(5, '2'), 10},
                {"DivisionByteChar", (int) calculator.division(5, '2'), 10},
        };
    }

    @Test(dataProvider = "createSomeDataDivision")
    public void testDivision(String methodName, int a, int expectedValue) {
        System.out.println(methodName);
        assertEquals(expectedValue, a);
    }
}
