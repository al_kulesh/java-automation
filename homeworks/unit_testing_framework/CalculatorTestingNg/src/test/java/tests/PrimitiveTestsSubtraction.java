package tests;

import dzmitry.klokau.testing.Calculator;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

@Listeners({PrimitiveTestsListener.class})
public class PrimitiveTestsSubtraction {
    Calculator calculator = new Calculator();

    @Test
    public void subtractionTestIntInt() {
        int num = calculator.subtraction(4, 1);
        System.out.println("subtractionTestIntInt");
        assertEquals(num, 3);
    }

    @Test
    public void subtractionTestDoubleDouble() {
        double num = calculator.subtraction(3.4, 2.3);
        System.out.println("subtractionTestDoubleDouble");
        assertEquals(num, 1.1);
    }

    @Test
    public void subtractionTestFloatFloat() {
        float num = calculator.subtraction(3.4, 2.1);
        System.out.println("subtractionTestFloatFloat");
        assertEquals(num, 1.3);
    }

    @Test
    public void subtractionTestStringString() {
        float num = calculator.subtraction("5.5", "3.3");
        System.out.println("subtractionTestStringString");
        assertEquals(num, 2.2);
    }

    @Test
    public void subtractionTestShortShort() {
        int num = calculator.subtraction(4, 3);
        System.out.println("subtractionTestShortShort");
        assertEquals(num, 1);
    }

    @Test
    public void subtractionTestCharChar() {
        int num = calculator.subtraction('6', '3');
        System.out.println("subtractionTestCharChar");
        assertEquals(num, 3);
    }

    @Test
    public void subtractionTestByteByte() {
        int num = calculator.subtraction(4, 2);
        System.out.println("subtractionTestByteByte");
        assertEquals(num, 2);
    }

    @Test
    public void subtractionTestLongLong() {
        long num = calculator.subtraction(6, 4);
        System.out.println("subtractionTestLongLong");
        assertEquals(num, 2);
        ;
    }

    @Test
    public void subtractionTestIntDouble() {
        double num = calculator.subtraction(7, 4.2);
        System.out.println("subtractionTestIntDouble");
        assertEquals(num, 2.8);
    }

    @Test
    public void subtractionTestIntFloat() {
        double num = calculator.subtraction(5, 4.2);
        System.out.println("subtractionTestIntFloat");
        assertEquals(num, 0.8);
    }

    @Test
    public void subtractionTestIntString() {
        double num = calculator.subtraction(6, "3");
        System.out.println("subtractionTestIntString");
        assertEquals(num, 3);
    }

    @Test
    public void subtractionTestIntShort() {
        double num = calculator.subtraction(6, 1);
        System.out.println("subtractionTestIntShort");
        assertEquals(num, 5);
    }

    @Test
    public void subtractionTestIntChar() {
        double num = calculator.subtraction(4, '4');
        System.out.println("subtractionTestIntChar");
        assertEquals(num, 0);
    }

    @Test
    public void subtractionTestIntByte() {
        double num = calculator.subtraction(4, 3);
        System.out.println("subtractionTestIntByte");
        assertEquals(num, 1);
    }

    @Test
    public void subtractionTestIntLong() {
        double num = calculator.subtraction(5, 3);
        System.out.println("subtractionTestIntLong");
        assertEquals(num, 2);
    }

    @Test
    public void subtractionTestDoubleInt() {
        double num = calculator.subtraction(5.6, 3);
        System.out.println("subtractionTestDoubleInt");
        assertEquals(num, 2.6);
    }

    @Test
    public void subtractionTestFloatInt() {
        double num = calculator.subtraction(5.6, 3);
        System.out.println("subtractionTestFloatInt");
        assertEquals(num, 2.6);
    }

    @Test
    public void subtractionTestStringInt() {
        double num = calculator.subtraction("7", 3);
        System.out.println("subtractionTestStringInt");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestShortInt() {
        double num = calculator.subtraction(6, 3);
        System.out.println("subtractionTestShortInt");
        assertEquals(num, 3);
    }

    @Test
    public void subtractionTestCharInt() {
        double num = calculator.subtraction('6', 2);
        System.out.println("subtractionTestCharInt");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestByteInt() {
        double num = calculator.subtraction(7, 2);
        System.out.println("subtractionTestByteInt");
        assertEquals(num, 5);
    }

    @Test
    public void subtractionTestLongInt() {
        double num = calculator.subtraction(7, 2);
        System.out.println("subtractionTestLongInt");
        assertEquals(num, 5);
    }

    @Test
    public void subtractionTestDoubleFloat() {
        double num = calculator.subtraction(7.6, 1.5);
        System.out.println("subtractionDoubleFloat");
        assertEquals(num, 6.1);
    }

    @Test
    public void subtractionTestDoubleString() {
        double num = calculator.subtraction(7.6, "4");
        System.out.println("subtractionTestDoubleString");
        assertEquals(num, 3.6);
    }

    @Test
    public void subtractionTestDoubleShort() {
        double num = calculator.subtraction(7.6, 5);
        System.out.println("subtractionTestDoubleShort");
        assertEquals(num, 2.6);
    }

    @Test
    public void subtractionTestDoubleChar() {
        double num = calculator.subtraction(7.6, '4');
        System.out.println("subtractionTestDoubleChar");
        assertEquals(num, 3.6);
    }

    @Test
    public void subtractionTestDoubleByte() {
        double num = calculator.subtraction(7.6, 2);
        System.out.println("subtractionTestDoubleByte");
        assertEquals(num, 5.6);
    }

    @Test
    public void subtractionTestDoubleLong() {
        double num = calculator.subtraction(7.6, 6);
        System.out.println("subtractionTestDoubleLong");
        assertEquals(num, 1.6);
    }

    @Test
    public void subtractionTestLongDouble() {
        double num = calculator.subtraction(9, 6.2);
        System.out.println("subtractionTestLongDouble");
        assertEquals(num, 2.8);
    }

    @Test
    public void subtractionTestFloatDouble() {
        double num = calculator.subtraction(7.6, 6.2);
        System.out.println("subtractionTestFloatDouble");
        assertEquals(num, 1.4);
    }

    @Test
    public void subtractionTestStringDouble() {
        double num = calculator.subtraction("8", 6.2);
        System.out.println("subtractionTestStringDouble");
        assertEquals(num, 1.8);
    }

    @Test
    public void subtractionTestShortDouble() {
        double num = calculator.subtraction(9, 6.2);
        System.out.println("subtractionTestShortDouble");
        assertEquals(num, 2.8);
    }

    @Test
    public void subtractionTestCharDouble() {
        double num = calculator.subtraction('8', 6.2);
        System.out.println("subtractionTestCharDouble");
        assertEquals(num, 1.8);
    }

    @Test
    public void subtractionTestByteDouble() {
        double num = calculator.subtraction(9, 6.2);
        System.out.println("subtractionTestByteDouble");
        assertEquals(num, 2.8);
    }

    @Test
    public void subtractionTestFloatString() {
        double num = calculator.subtraction(9.8, "7");
        System.out.println("subtractionTestFloatString");
        assertEquals(num, 2.8);
    }

    @Test
    public void subtractionTestFloatShort() {
        double num = calculator.subtraction(9.8, 5);
        System.out.println("subtractionTestFloatShort");
        assertEquals(num, 4.8);
    }

    @Test
    public void subtractionTestFloatChar() {
        double num = calculator.subtraction(9.8, '5');
        System.out.println("subtractionTestFloatShort");
        assertEquals(num, 4.8);
    }

    @Test
    public void subtractionTestFloatByte() {
        double num = calculator.subtraction(9.8, 5);
        System.out.println("subtractionTestFloatByte");
        assertEquals(num, 4.8);
    }

    @Test
    public void subtractionTestFloatLong() {
        double num = calculator.subtraction(9.8, 5);
        System.out.println("subtractionTestFloatLong");
        assertEquals(num, 4.8);
    }

    @Test
    public void subtractionTestLongFloat() {
        double num = calculator.subtraction(9.8, 5);
        System.out.println("subtractionTestLongFloat");
        assertEquals(num, 4.8);
    }

    @Test
    public void subtractionTestStringFloat() {
        double num = calculator.subtraction("9", 5.2);
        System.out.println("subtractionTestStringFloat");
        assertEquals(num, 3.8);
    }

    @Test
    public void subtractionTestShortFloat() {
        double num = calculator.subtraction(9, 5.2);
        System.out.println("subtractionTestShortFloat");
        assertEquals(num, 3.8);
    }

    @Test
    public void subtractionTestCharFloat() {
        double num = calculator.subtraction('9', 5.2);
        System.out.println("subtractionTestCharFloat");
        assertEquals(num, 3.8);
    }

    @Test
    public void subtractionTestByteFloat() {
        double num = calculator.subtraction(9, 5.2);
        System.out.println("subtractionTestByteFloat");
        assertEquals(num, 3.8);
    }

    @Test
    public void subtractionTestStringShort() {
        double num = calculator.subtraction("9", 5);
        System.out.println("subtractionTestStringShort");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestStringChar() {
        double num = calculator.subtraction("9", '5');
        System.out.println("subtractionTestStringChar");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestStringByte() {
        double num = calculator.subtraction("9", 5);
        System.out.println("subtractionTestStringByte");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestStringLong() {
        double num = calculator.subtraction("9", 5);
        System.out.println("subtractionTestStringLong");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestLongString() {
        double num = calculator.subtraction(9, "5");
        System.out.println("subtractionLongString");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestShortString() {
        double num = calculator.subtraction(9, "5");
        System.out.println("subtractionTestShortString");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestCharString() {
        double num = calculator.subtraction('9', "5");
        System.out.println("subtractionTestCharString");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestByteString() {
        double num = calculator.subtraction(9, "5");
        System.out.println("subtractionTestByteString");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestShortChar() {
        double num = calculator.subtraction(9, '5');
        System.out.println("subtractionTestShortChar");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestShortByte() {
        double num = calculator.subtraction(9, 5);
        System.out.println("subtractionTestShortByte");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestShortLong() {
        double num = calculator.subtraction(9, 5);
        System.out.println("subtractionTestShortLong");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestLongShort() {
        double num = calculator.subtraction(9, 5);
        System.out.println("subtractionTestLongShort");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestCharShort() {
        double num = calculator.subtraction('9', 5);
        System.out.println("subtractionTestCharShort");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestByteShort() {
        double num = calculator.subtraction(9, 5);
        System.out.println("subtractionTestByteShort");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestCharByte() {
        double num = calculator.subtraction('9', 5);
        System.out.println("subtractionTestCharByte");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestCharLong() {
        double num = calculator.subtraction('9', 5);
        System.out.println("subtractionTestCharLong");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestLongChar() {
        double num = calculator.subtraction(9, '5');
        System.out.println("subtractionTestLongChar");
        assertEquals(num, 4);
    }

    @Test
    public void subtractionTestByteChar() {
        double num = calculator.subtraction(9, '5');
        System.out.println("subtractionTestByteChar");
        assertEquals(num, 4);
    }
}
