package tests;

import static org.testng.Assert.*;

import dzmitry.klokau.testing.Calculator;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


@Listeners({PrimitiveTestsListener.class})
public class PrimitiveTestsSum {
    Calculator calculator = new Calculator();

    @Test
    public void sumTestIntInt() {
        int num = calculator.sum(3, 3);
        System.out.println("sumTestIntInt");
        assertEquals(num, 6);
    }

    @Test
    public void sumTestDoubleDouble() {
        double num = calculator.sum(3.11, 4.02);
        System.out.println("sumTestDoubleDouble");
        assertEquals(num, 7);

    }

    @Test
    public void sumTestFloatFloat() {
        float num = calculator.sum(4.315, 4.131);
        System.out.println("sumTestFloatFloat");
        assertEquals(num, 8);
    }

    @Test
    public void sumTestStringString() {
        float num = calculator.sum("1.133", "2.022");
        System.out.println("sumTestStringString");
        assertEquals(num, 3);
    }

    @Test
    public void sumTestShortShort() {
        int num = calculator.sum(1, 2);
        System.out.println("sumTestShortShort");
        assertEquals(num, 3);
    }

    @Test
    public void sumTestCharChar() {
        int num = calculator.sum('1', '3');
        System.out.println("sumTestCharChar");
        assertEquals(num, 4);
    }

    @Test
    public void sumTestByteByte() {
        int num = calculator.sum(1, 6);
        System.out.println("sumTestByteByte");
        assertEquals(num, 7);
    }

    @Test
    public void sumTestLongLong() {
        long num = calculator.sum(2, 1);
        System.out.println("sumTestLongLong");
        assertEquals(num, 3);
    }

    @Test
    public void sumTestIntDouble() {
        double num = calculator.sum(2, 1.21);
        System.out.println("sumTestIntDouble");
        assertEquals(num, 3.21);
    }

    @Test
    public void sumTestIntFloat() {
        double num = calculator.sum(3, 1.22);
        System.out.println("sumTestIntFloat");
        assertEquals(num, 4.22);
    }

    @Test
    public void sumTestIntString() {
        double num = calculator.sum(2, "1");
        System.out.println("sumTestIntString");
        assertEquals(num, 3);
    }

    @Test
    public void sumTestIntShort() {
        double num = calculator.sum(1, 4);
        System.out.println("sumTestIntShort");
        assertEquals(num, 5);
    }

    @Test
    public void sumTestIntChar() {
        double num = calculator.sum(1, '2');
        System.out.println("sumTestIntChar");
        assertEquals(num, '3');
    }

    @Test
    public void sumTestIntByte() {
        double num = calculator.sum(2, 3);
        System.out.println("sumTestIntByte");
        assertEquals(num, 5);
    }

    @Test
    public void sumTestIntLong() {
        double num = calculator.sum(3, 3);
        System.out.println("sumTestIntLong");
        assertEquals(num, 6);
    }

    @Test
    public void sumTestDoubleInt() {
        double num = calculator.sum(3.41, 3);
        System.out.println("sumTestDoubleInt");
        assertEquals(num, 6.41);
    }

    @Test
    public void sumTestFloatInt() {
        double num = calculator.sum(3.41, 3);
        System.out.println("sumTestFloatInt");
        assertEquals(num, 6.41);
    }

    @Test
    public void sumTestStringInt() {
        double num = calculator.sum("2", 2);
        System.out.println("sumTestStringInt");
        assertEquals(num, 4);
    }

    @Test
    public void sumTestShortInt() {
        double num = calculator.sum(12, 33);
        System.out.println("sumTestShortInt");
        assertEquals(num, 45);
    }

    @Test
    public void sumTestCharInt() {
        double num = calculator.sum('4', 2);
        System.out.println("sumTestCharInt");
        assertEquals(num, 6);
    }

    @Test
    public void sumTestByteInt() {
        double num = calculator.sum(1, 1);
        System.out.println("sumTestByteInt");
        assertEquals(num, 2);
    }

    @Test
    public void sumTestLongInt() {
        double num = calculator.sum(45, 33);
        System.out.println("sumTestLongInt");
        assertEquals(num, 78);
    }

    @Test
    public void sumTestDoubleFloat() {
        double num = calculator.sum(5.5, 1.4);
        System.out.println("sumTestDoubleFloat");
        assertEquals(num, 6.9);
    }

    @Test
    public void sumTestDoubleString() {
        double num = calculator.sum(4.5, 3);
        System.out.println("sumTestDoubleFloat");
        assertEquals(num, 7.5);
    }

    @Test
    public void sumTestDoubleShort() {
        double num = calculator.sum(4.2, 2);
        System.out.println("sumTestDoubleShort");
        assertEquals(num, 6.2);
    }

    @Test
    public void sumTestDoubleChar() {
        double num = calculator.sum(3.4, '3');
        System.out.println("sumTestDoubleChar");
        assertEquals(num, 6.4);
    }

    @Test
    public void sumTestDoubleByte() {
        double num = calculator.sum(4.5, 5);
        System.out.println("sumTestDoubleByte");
        assertEquals(num, 9.5);
    }

    @Test
    public void sumTestDoubleLong() {
        double num = calculator.sum(6.5, 2);
        System.out.println("sumTestDoubleLong");
        assertEquals(num, 8.5);
    }

    @Test
    public void sumTestFloatDouble() {
        double num = calculator.sum(3.5, 1.3);
        System.out.println("sumTestFloatDouble");
        assertEquals(num, 4.8);
    }

    @Test
    public void sumTestLongDouble() {
        double num = calculator.sum(3, 1.4);
        System.out.println("sumTestLongDouble");
        assertEquals(num, 4.4);
    }

    @Test
    public void sumTestStringDouble() {
        double num = calculator.sum("2", 3.5);
        System.out.println("sumTestStringDouble");
        assertEquals(num, 5.5);
    }

    @Test
    public void sumTestShortDouble() {
        double num = calculator.sum(4, 6.6);
        System.out.println("sumTestShortDouble");
        assertEquals(num, 10.6);
    }

    @Test
    public void sumTestCharDouble() {
        double num = calculator.sum('2', 5.5);
        System.out.println("sumTestCharDouble");
        assertEquals(num, 7.5);
    }

    @Test
    public void sumTestByteDouble() {
        double num = calculator.sum(2, 3.5);
        System.out.println("sumTestByteDouble");
        assertEquals(num, 5.5);
    }

    @Test
    public void sumTestFloatString() {
        double num = calculator.sum(4.5, "2");
        System.out.println("sumTestByteDouble");
        assertEquals(num, 6.5);
    }

    @Test
    public void sumTestFloatShort() {
        double num = calculator.sum(2.5, 5);
        System.out.println("sumTestFloatShort");
        assertEquals(num, 7.5);
    }

    @Test
    public void sumTestFloatChar() {
        double num = calculator.sum(3.5, '2');
        System.out.println("sumTestFloatShort");
        assertEquals(num, 5.5);
    }

    @Test
    public void sumTestFloatByte() {
        double num = calculator.sum(5.6, 3);
        System.out.println("sumTestFloatShort");
        assertEquals(num, 8.6);
    }

    @Test
    public void sumTestFloatLong() {
        double num = calculator.sum(5.6, 3);
        System.out.println("sumTestFloatLong");
        assertEquals(num, 8.6);
    }

    @Test
    public void sumTestLongFloat() {
        double num = calculator.sum(4, 2.2);
        System.out.println("sumTestLongFloat");
        assertEquals(num, 6.2);
    }

    @Test
    public void sumTestStringFloat() {
        double num = calculator.sum("4", 2.2);
        System.out.println("sumTestStringFloat");
        assertEquals(num, 6.2);
    }

    @Test
    public void sumTestShortFloat() {
        double num = calculator.sum(3, 3.5);
        System.out.println("sumTestShortFloat");
        assertEquals(num, 6.5);
    }

    @Test
    public void sumTestCharFloat() {
        double num = calculator.sum('2', 5.2);
        System.out.println("sumTestCharFloat");
        assertEquals(num, 7.2);
    }

    @Test
    public void sumTestByteFloat() {
        double num = calculator.sum(3, 5.2);
        System.out.println("sumTestByteFloat");
        assertEquals(num, 8.2);
    }

    @Test
    public void sumTestStringShort() {
        double num = calculator.sum("4", 3);
        System.out.println("sumTestStringShort");
        assertEquals(num, 7);
    }

    @Test
    public void sumTestStringChar() {
        double num = calculator.sum("4", '3');
        System.out.println("sumTestStringChar");
        assertEquals(num, 7);
    }

    @Test
    public void sumTestStringLong() {
        double num = calculator.sum("4", 3);
        System.out.println("sumTestStringLong");
        assertEquals(num, 7);
    }

    @Test
    public void sumTestLongString() {
        double num = calculator.sum(5, "4");
        System.out.println("sumTestLongString");
        assertEquals(num, 9);
    }

    @Test
    public void sumTestShortString() {
        double num = calculator.sum(3, "4");
        System.out.println("sumTestShortString");
        assertEquals(num, 7);
    }

    @Test
    public void sumTestCharString() {
        double num = calculator.sum('3', "4");
        System.out.println("sumTestCharString");
        assertEquals(num, 7);
    }

    @Test
    public void sumTestByteString() {
        double num = calculator.sum(3, "4");
        System.out.println("sumTestByteString");
        assertEquals(num, 7);
    }

    @Test
    public void sumTestShortChar() {
        double num = calculator.sum(3, '4');
        System.out.println("sumTestShortChar");
        assertEquals(num, 7);
    }

    @Test
    public void sumTestShortByte() {
        double num = calculator.sum(4, 5);
        System.out.println("sumTestShortByte");
        assertEquals(num, 9);
    }

    @Test
    public void sumTestShortLong() {
        double num = calculator.sum(3, 3);
        System.out.println("sumTestShortLong");
        assertEquals(num, 6);
    }

    @Test
    public void sumTestLongShort() {
        double num = calculator.sum(3, 5);
        System.out.println("sumTestLongShort");
        assertEquals(num, 8);
    }

    @Test
    public void sumTestCharShort() {
        double num = calculator.sum('4', 5);
        System.out.println("sumTestCharShort");
        assertEquals(num, 9);
    }

    @Test
    public void sumTestByteShort() {
        double num = calculator.sum(5, 5);
        System.out.println("sumTestByteShort");
        assertEquals(num, 10);
    }

    @Test
    public void sumTestCharByte() {
        double num = calculator.sum('2', 4);
        System.out.println("sumTestCharByte");
        assertEquals(num, 6);
    }

    @Test
    public void sumTestCharLong() {
        double num = calculator.sum('2', 4);
        System.out.println("sumTestCharLong");
        assertEquals(num, 6);
    }

    @Test
    public void sumTestLongChar() {
        double num = calculator.sum(4, '1');
        System.out.println("sumTestLongChar");
        assertEquals(num, 5);
    }

    @Test
    public void sumTestByteChar() {
        double num = calculator.sum(4, '1');
        System.out.println("sumTestByteChar");
        assertEquals(num, 5);
    }
}
