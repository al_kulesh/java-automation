package tests;


import dzmitry.klokau.testing.Calculator;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.testng.Assert.assertEquals;

public class SomeReflectionTests {
    @Test
    public void sumReflectionTest() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method sumInstanceMethod
                = Calculator.class.getMethod("sum", int.class, int.class);

        Calculator operationsInstance = new Calculator();
        int result
                = (Integer) sumInstanceMethod.invoke(operationsInstance, 1, 3);

        assertEquals(result, 4);
    }

    @Test
    public void subtractionReflectionTest() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method sumInstanceMethod
                = Calculator.class.getMethod("subtraction", int.class, int.class);

        Calculator operationsInstance = new Calculator();
        int result
                = (Integer) sumInstanceMethod.invoke(operationsInstance, 5, 3);

        assertEquals(result, 2);
    }

    @Test
    public void multiplicationReflectionTest() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method sumInstanceMethod
                = Calculator.class.getMethod("multiplication", short.class, int.class);

        Calculator operationsInstance = new Calculator();
        double result
                = (Double) sumInstanceMethod.invoke(operationsInstance, (short) 6, 3);

        assertEquals(result, 18);
    }

    @Test
    public void divisionReflectionTest() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method sumInstanceMethod
                = Calculator.class.getMethod("division", short.class, int.class);

        Calculator operationsInstance = new Calculator();
        double result
                = (Double) sumInstanceMethod.invoke(operationsInstance, (short) 6, 3);

        assertEquals(result, 2);
    }
}