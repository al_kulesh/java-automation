package tests;

import dzmitry.klokau.testing.Calculator;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Listeners({PrimitiveTestsListener.class})
public class PrimitiveTestsMultiplication {
    Calculator calculator = new Calculator();

    @Test
    public void multiplicationTestShortInt() {
        double num = calculator.multiplication(4, 3);
        System.out.println("multiplicationTestShortInt");
        assertEquals(num, 12);
    }

    @Test
    public void multiplicationTestCharInt() {
        double num = calculator.multiplication('3', 3);
        System.out.println("multiplicationTestCharInt");
        assertEquals(num, 9);
    }

    @Test
    public void multiplicationTestByteInt() {
        double num = calculator.multiplication(3, 3);
        System.out.println("multiplicationTestByteInt");
        assertEquals(num, 9);
    }

    @Test
    public void multiplicationTestLongInt() {
        double num = calculator.multiplication(3, 3);
        System.out.println("multiplicationTestLongInt");
        assertEquals(num, 9);
    }

    @Test
    public void multiplicationTestDoubleFloat() {
        double num = calculator.multiplication(2.5, 2);
        System.out.println("multiplicationTestDoubleFloat");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestDoubleString() {
        double num = calculator.multiplication(2.5, "2");
        System.out.println("multiplicationTestDoubleString");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestDoubleShort() {
        double num = calculator.multiplication(2.5, 2);
        System.out.println("multiplicationTestDoubleShort");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestDoubleChar() {
        double num = calculator.multiplication(2.5, '2');
        System.out.println("multiplicationTestDoubleChar");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestDoubleByte() {
        double num = calculator.multiplication(2.5, 2);
        System.out.println("multiplicationTestDoubleByte");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestDoubleLong() {
        double num = calculator.multiplication(2.5, 2);
        System.out.println("multiplicationTestDoubleLong");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestLongDouble() {
        double num = calculator.multiplication(2, 2.5);
        System.out.println("multiplicationTestLongDouble");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestFloatDouble() {
        double num = calculator.multiplication(2, 2.5);
        System.out.println("multiplicationTestFloatDouble");
        assertEquals(num, 5);

    }

    @Test
    public void multiplicationTestStringDouble() {
        double num = calculator.multiplication("2", 2.5);
        System.out.println("multiplicationTestStringDouble");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestShortDouble() {
        double num = calculator.multiplication("2", 2.5);
        System.out.println("multiplicationTestShortDouble");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestCharDouble() {
        double num = calculator.multiplication('2', 2.5);
        System.out.println("multiplicationTestCharDouble");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestByteDouble() {
        double num = calculator.multiplication(2, 2.5);
        System.out.println("multiplicationTestByteDouble");
        assertEquals(num, 5);
    }

    @Test
    public void multiplicationTestFloatString() {
        double num = calculator.multiplication(2.5, "4");
        System.out.println("multiplicationTestFloatString");
        assertEquals(num, 10);
    }

    @Test
    public void multiplicationTestFloatShort() {
        double num = calculator.multiplication(2.5, 4);
        System.out.println("multiplicationTestFloatShort");
        assertEquals(num, 10);
    }

    @Test
    public void multiplicationTestFloatChar() {
        double num = calculator.multiplication(2.5, '4');
        System.out.println("multiplicationTestFloatChar");
        assertEquals(num, 10);
    }

    @Test
    public void multiplicationTestFloatByte() {
        double num = calculator.multiplication(2.5, 4);
        System.out.println("multiplicationTestFloatByte");
        assertEquals(num, 10);
    }

    @Test
    public void multiplicationTestFloatLong() {
        double num = calculator.multiplication(2.5, 4);
        System.out.println("multiplicationTestFloatLong");
        assertEquals(num, 10);
    }

    @Test
    public void multiplicationTestLongFloat() {
        double num = calculator.multiplication(4, 2.5);
        System.out.println("multiplicationTestLongFloat");
        assertEquals(num, 10);
    }

    @Test
    public void multiplicationTestStringFloat() {
        double num = calculator.multiplication("4", 2.5);
        System.out.println("multiplicationTestStringFloat");
        assertEquals(num, 10);
    }

    @Test
    public void multiplicationTestShortFloat() {
        double num = calculator.multiplication(4, 2.5);
        System.out.println("multiplicationTestShortFloat");
        assertEquals(num, 10);
    }

    @Test
    public void multiplicationTestCharFloat() {
        double num = calculator.multiplication('4', 2.5);
        System.out.println("multiplicationTestCharFloat");
        assertEquals(num, 10);
    }

    @Test
    public void multiplicationTestByteFloat() {
        double num = calculator.multiplication(4, 2.5);
        System.out.println("multiplicationTestByteFloat");
        assertEquals(num, 10);
    }

    @Test
    public void multiplicationTestStringShort() {
        double num = calculator.multiplication("4", 2);
        System.out.println("multiplicationTestStringShort");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestStringChar() {
        double num = calculator.multiplication("4", '2');
        System.out.println("multiplicationTestStringChar");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestStringByte() {
        double num = calculator.multiplication("4", 2);
        System.out.println("multiplicationTestStringByte");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestStringLong() {
        double num = calculator.multiplication("4", 2);
        System.out.println("multiplicationTestStringLong");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestLongString() {
        double num = calculator.multiplication(2, "4");
        System.out.println("multiplicationTestLongString");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestShortString() {
        double num = calculator.multiplication(2, "4");
        System.out.println("multiplicationTestShortString");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestCharString() {
        double num = calculator.multiplication('2', "4");
        System.out.println("multiplicationTestCharString");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestByteString() {
        double num = calculator.multiplication(2, "4");
        System.out.println("multiplicationTestByteString");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestShortChar() {
        double num = calculator.multiplication(2, '4');
        System.out.println("multiplicationTestShortChar");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestShortByte() {
        double num = calculator.multiplication(2, 4);
        System.out.println("multiplicationTestShortByte");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestShortLong() {
        double num = calculator.multiplication(2, 4);
        System.out.println("multiplicationTestShortLong");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestLongShort() {
        double num = calculator.multiplication(2, 4);
        System.out.println("multiplicationTestLongShort");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestCharShort() {
        double num = calculator.multiplication('2', 4);
        System.out.println("multiplicationTestCharShort");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestByteShort() {
        double num = calculator.multiplication(2, 4);
        System.out.println("multiplicationTestByteShort");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestCharByte() {
        double num = calculator.multiplication('4', 2);
        System.out.println("multiplicationTestCharByte");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestCharLong() {
        double num = calculator.multiplication('4', 2);
        System.out.println("multiplicationTestCharLong");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestLongChar() {
        double num = calculator.multiplication(4, '2');
        System.out.println("multiplicationTestLongChar");
        assertEquals(num, 8);
    }

    @Test
    public void multiplicationTestByteChar() {
        double num = calculator.multiplication(4, '2');
        System.out.println("multiplicationTestByteChar");
        assertEquals(num, 8);
    }
}
