package tests;

import dzmitry.klokau.testing.Calculator;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

@Listeners({PrimitiveTestsListener.class})
public class PrimitiveTestsDivision {
    Calculator calculator = new Calculator();
    @Test
    public void divisionTestShortInt(){
        double num = calculator.division(4,2);
        System.out.println("divisionTestShortInt");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestCharInt(){
        double num = calculator.division('4',2);
        System.out.println("divisionTestCharInt");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestByteInt(){
        double num = calculator.division(4,2);
        System.out.println("divisionTestByteInt");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestLongInt(){
        double num = calculator.division(4,2);
        System.out.println("divisionTestLongInt");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestDoubleFloat(){
        double num = calculator.division(4.2,2);
        System.out.println("divisionTestDoubleFloat");
        assertEquals(num, 2.1);
    }
    @Test
    public void divisionTestDoubleString(){
        double num = calculator.division(4.2,"2");
        System.out.println("divisionTestDoubleString");
        assertEquals(num, 2.1);
    }
    @Test
    public void divisionTestDoubleShort(){
        double num = calculator.division(4.2,2);
        System.out.println("divisionTestDoubleShort");
        assertEquals(num, 2.1);
    }
    @Test
    public void divisionTestDoubleChar(){
        double num = calculator.division(4.2,'2');
        System.out.println("divisionTestDoubleChar");
        assertEquals(num, 2.1);
    }
    @Test
    public void divisionTestDoubleByte(){
        double num = calculator.division(4.2,2);
        System.out.println("divisionTestDoubleByte");
        assertEquals(num, 2.1);
    }
    @Test
    public void divisionTestDoubleLong(){
        double num = calculator.division(4.2,2);
        System.out.println("divisionTestDoubleLong");
        assertEquals(num, 2.1);
    }
    @Test
    public void divisionvLongDouble(){
        double num = calculator.division(7,3.5);
        System.out.println("divisionTestLongDouble");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestFloatDouble(){
        double num = calculator.division(7,3.5);
        System.out.println("divisionTestFloatDouble");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestStringDouble(){
        double num = calculator.division("7",3.5);
        System.out.println("divisionTestStringDouble");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestShortDouble(){
        double num = calculator.division(7,3.5);
        System.out.println("divisionTestShortDouble");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestCharDouble(){
        double num = calculator.division('7',3.5);
        System.out.println("divisionTestCharDouble");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestByteDouble(){
        double num = calculator.division(7,3.5);
        System.out.println("divisionTestByteDouble");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestFloatString(){
        double num = calculator.division(7,"2");
        assertEquals(num, 3.5);
        System.out.println("divisionTestFloatString");
    }
    @Test
    public void divisionTestFloatShort(){
        double num = calculator.division(7,2);
        System.out.println("divisionTestFloatShort");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestFloatChar(){
        double num = calculator.division(7,'2');
        System.out.println("divisionTestFloatChar");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestFloatByte(){
        double num = calculator.division(7.5,2);
        System.out.println("divisionTestFloatByte");
        assertEquals(num, 3.75);
    }
    @Test
    public void divisionTestFloatLong(){
        double num = calculator.division(7.5,2);
        System.out.println("divisionTestFloatLong");
        assertEquals(num, 3.75);
    }
    @Test
    public void divisionTestLongFloat(){
        double num = calculator.division(7,3.5);
        System.out.println("divisionTestLongFloat");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestStringFloat(){
        double num = calculator.division("7",3.5);
        System.out.println("divisionTestStringFloat");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestShortFloat(){
        double num = calculator.division(7,3.5);
        System.out.println("divisionTestShortFloat");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestCharFloat(){
        double num = calculator.division('7',3.5);
        System.out.println("divisionTestCharFloat");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestByteFloat(){
        double num = calculator.division(7,3.5);
        System.out.println("divisionTestByteFloat");
        assertEquals(num, 2);
    }
    @Test
    public void divisionTestStringShort(){
        double num = calculator.division("7",2);
        System.out.println("divisionTestStringShort");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestStringChar(){
        double num = calculator.division("7",'2');
        System.out.println("divisionTestStringChar");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestStringByte(){
        double num = calculator.division("7",2);
        System.out.println("divisionTestStringByte");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestStringLong(){
        double num = calculator.division("7",2);
        System.out.println("divisionTestStringLong");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestLongString(){
        double num = calculator.division(7,"2");
        System.out.println("divisionTestLongString");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestShortString(){
        double num = calculator.division(7,"2");
        System.out.println("divisionTestShortString");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestCharString(){
        double num = calculator.division('7',"2");
        System.out.println("divisionTestCharString");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestByteString(){
        double num = calculator.division(7,"2");
        System.out.println("divisionTestByteString");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestShortChar(){
        double num = calculator.division(7,'2');
        System.out.println("divisionTestShortChar");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestShortByte(){
        double num = calculator.division(7,2);
        System.out.println("divisionTestShortByte");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestShortLong(){
        double num = calculator.division(7,2);
        System.out.println("divisionTestShortLong");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestLongShort(){
        double num = calculator.division(7,2);
        System.out.println("divisionTestLongShort");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestCharShort(){
        double num = calculator.division('7',2);
        System.out.println("divisionTestCharShort");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestByteShort(){
        double num = calculator.division(7,2);
        System.out.println("divisionTestCharShort");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestCharByte(){
        double num = calculator.division('7',2);
        System.out.println("divisionTestCharByte");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestCharLong(){
        double num = calculator.division('7',2);
        System.out.println("divisionTestCharLong");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestLongChar(){
        double num = calculator.division(7,'2');
        System.out.println("divisionTestLongChar");
        assertEquals(num, 3.5);
    }
    @Test
    public void divisionTestByteChar(){
        double num = calculator.division(7,'2');
        System.out.println("divisionTestByteChar");
        assertEquals(num, 3.5);
    }
}
