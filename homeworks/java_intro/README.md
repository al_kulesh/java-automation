# Задание
 
## Написать небольшое консольное приложение интернет-магазина

# Основные классы/пакеты
- [Main.java](WebShopApp/src/main/java/Main.java) - происходит загрузка данных для работы приложения и вход приложение
- [WebShopApp](WebShopApp) - пакет содержащий набор действий производимых в интернет-магазине
- [Shop.java](WebShopApp/src/main/java/Shop.java) - класс воссоздающий 
- [Product.java](WebShopApp/src/main/java/Product.java) - класс для отдельного обозначения продукта как объекта
- [Warehouse.java](WebShopApp/src/main/java/Warehouse.java) - класс для загрузки начального списка продуктов при запуске приложения
- [Credentials.java](WebShopApp/src/main/java/Credentials.java) - класс для отдельного обозначения учетных данных пользователей и загрузки начального их списка

# Скриншоты успешного выполнения
- [App enter](images/AppEnter.png)
- [Login enter admin](images/LoginEnterAdmin.png)
- [Adding a new product](images/AddingNewProduct.png)
- [Password enter admin](images/PasswordEnterAdmin.png)
- [Login enter customer](images/LoginEnterCustomer.png)
- [Password enter customer](images/PasswordEnterCustomer.png)
- [Choosing product](images/ChoosingProduct.png)
- [View all products](images/ViewAllProducts.png)

