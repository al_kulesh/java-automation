public class Product {
    private String productType;
    private String productName;
    private String productSpecifications;

    public Product(String productType, String productName, String productSpecifications) {
        this.productType = productType;
        this.productName = productName;
        this.productSpecifications = productSpecifications;
    }

    public Product() {
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpecifications() {
        return productSpecifications;
    }

    public void setProductSpecifications(String productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productType='" + productType + '\'' +
                ", productName='" + productName + '\'' +
                ", productSpecifications='" + productSpecifications + '\'' +
                '}';
    }
}