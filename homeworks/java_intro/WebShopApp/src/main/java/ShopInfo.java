import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ShopInfo {
    private Map<Integer, Product> smartPhones;
    private Map<Integer, Product> laptops;
    private Map<Integer, Product> tvSets;
    private List<Product> allProducts;
    private List<Product> cart;
    private List<Credentials> adminCredentials = new ArrayList<>();
    private List<Credentials> customerCredentials = new ArrayList<>();



    public ShopInfo() {
    }

    public Map<Integer, Product> getSmartPhones() {
        return smartPhones;
    }

    public void setSmartPhones(Map<Integer, Product> smartPhones) {
        this.smartPhones = smartPhones;
    }

    public Map<Integer, Product> getLaptops() {
        return laptops;
    }

    public void setLaptops(Map<Integer, Product> laptops) {
        this.laptops = laptops;
    }

    public Map<Integer, Product> getTvSets() {
        return tvSets;
    }

    public void setTvSets(Map<Integer, Product> tvSets) {
        this.tvSets = tvSets;
    }

    public List<Product> getAllProducts() {
        return allProducts;
    }

    public void setAllProducts(List<Product> allProducts) {
        this.allProducts = allProducts;
    }

    public List<Product> getCart() {
        return cart;
    }

    public void setCart(List<Product> cart) {
        this.cart = cart;
    }

    public List<Credentials> getAdminCredentials() {
        return adminCredentials;
    }

    public void setAdminCredentials(List<Credentials> adminCredentials) {
        this.adminCredentials = adminCredentials;
    }

    public List<Credentials> getCustomerCredentials() {
        return customerCredentials;
    }

    public void setCustomerCredentials(List<Credentials> customerCredentials) {
        this.customerCredentials = customerCredentials;
    }
}
