import java.util.*;
public class ShopActions {
    ShopInfo shopInfo = new ShopInfo();

    public ShopInfo getShopInfo() {
        return shopInfo;
    }

    public void setShopInfo(ShopInfo shopInfo) {
        this.shopInfo = shopInfo;
    }

    public static Scanner in = new Scanner(System.in);

    public void appEnter() {
        System.out.println("""
                Who would you like to do?
                 1. Administrate the shopActions
                 2. Purchase some items
                 3. Exit
                 """);
        int choice = in.nextInt();
        if (choice == 1) {
            enterAdminLogin();
            adminEnter();
        }
        if (choice == 2) {
            enterCustomerLogin();
            customerEnter();
        }
        if (choice == 3) {
            System.out.println("Thank you. Goodbye.");
        }
    }

    public void enterAdminLogin() {
        System.out.println("Enter login");
        String login = in.next();
        List<Credentials> adminCredentials = shopInfo.getAdminCredentials();
        Credentials credentials;
        try {
            credentials = adminCredentials
                    .stream()
                    .filter(e -> e.getLogin().equals(login))
                    .findAny()
                    .orElse(null);
            if (credentials.getLogin().equals(login)) {
                System.out.println("Your login is correct");
            }
        } catch (NoSuchElementException | NullPointerException e) {
            System.out.println("You have entered  wrong login");
            exit();
        }
        System.out.println("Enter password");
        String password = in.next();
        try {
            credentials = adminCredentials
                    .stream()
                    .filter(e -> e.getPassword().equals(password))
                    .findAny()
                    .orElse(null);
            if (credentials.getPassword().equals(password)) {
                System.out.println("Your password is correct");
            }
        } catch (NoSuchElementException | NullPointerException e) {
            System.out.println("You have entered wrong password");
            exit();
        }
    }

    public void enterCustomerLogin() {
        System.out.println("Enter login");
        String login = in.next();
        List<Credentials> customerCredentials = shopInfo.getCustomerCredentials();
        Credentials credentials;
        try {
            credentials = customerCredentials
                    .stream()
                    .filter(e -> e.getLogin().equals(login))
                    .findAny()
                    .orElse(null);
            if (credentials.getLogin().equals(login)) {
                System.out.println("Your login is correct");
            }
        } catch (NoSuchElementException | NullPointerException e) {
            System.out.println("You have entered wrong login");
            exit();
        }
        System.out.println("Enter password");
        String password = in.next();
        try {
            credentials = customerCredentials
                    .stream()
                    .filter(e -> e.getPassword().equals(password))
                    .findAny()
                    .orElse(null);
            if (credentials.getPassword().equals(password)) {
                System.out.println("Your password is correct");
            }
        } catch (NoSuchElementException | NullPointerException e) {
            System.out.println("You have entered wrong password");
            exit();
        }
    }


    public void adminEnter() {
        System.out.println("""
                What would type of product you would like to add?
                 1. Smartphones
                 2. Laptops
                 3. TV-sets
                 4. Return to main menu
                 5. Exit
                 """);
        int choice = in.nextInt();
        switch (choice) {
            case (1):
                addSmartphone();
                break;
            case (2):
                addLaptop();
                break;
            case (3):
                addTVset();
                break;
            case (4):
                appEnter();
                break;
            case (5):
                System.out.println("Thank you. Goodbye.");
                break;
            default:
                adminEnter();
                break;
        }
    }

    public void addSmartphone() {
        System.out.println("Enter position of the smartphone");
        Integer positionAdd = in.nextInt();
        System.out.println("Enter smartphone name");
        String productName = in.next();
        System.out.println("Enter smartphone's specifications");
        String productSpecifications = in.next();
        shopInfo.getSmartPhones().put(positionAdd, new Product("Smartphone", productName, productSpecifications));
        System.out.println("""
                Add another one?
                 1. Yes
                 2. No
                 """);
        int action = in.nextInt();
        switch (action) {
            case (1):
                addSmartphone();
                break;
            case (2):
                adminEnter();
                break;
            default:
                break;
        }
    }

    public void addLaptop() {
        System.out.println("Enter position of the laptop");
        Integer positionAdd = in.nextInt();
        System.out.println("Enter laptop name");
        String productName = in.next();
        System.out.println("Enter laptop's specifications");
        String productSpecifications = in.next();
        shopInfo.getLaptops().put(positionAdd, new Product("Laptop", productName, productSpecifications));
        System.out.println("""
                Add another one?
                 1. Yes
                 2. No
                 """);
        int action = in.nextInt();
        switch (action) {
            case (1):
                addLaptop();
                break;
            case (2):
                adminEnter();
                break;
            default:
                break;
        }
    }

    public void addTVset() {
        System.out.println("Enter position of the TV-set");
        Integer positionAdd = in.nextInt();
        System.out.println("Enter TV-set name");
        String productName = in.next();
        System.out.println("Enter TV-set's specifications");
        String productSpecifications = in.next();
        shopInfo.getTvSets().put(positionAdd, new Product("TV-Set", productName, productSpecifications));
        System.out.println("""
                Add another one?
                 1. Yes
                 2. No
                 """);
        int action = in.nextInt();
        switch (action) {
            case (1):
                addTVset();
                break;
            case (2):
                adminEnter();
                break;
            default:
                break;
        }
    }

    public void customerEnter() {
        System.out.println("""
                What would you like to choose?
                 1. Smartphones
                 2. Laptops
                 3. TV-sets
                 4. See all type of products
                 5. Exit
                 """);

        int action = in.nextInt();
        switch (action) {
            case (1):
                choosingSmartphone();
                break;
            case (2):
                choosingLaptop();
                break;
            case (3):
                choosingTvSet();
                break;
            case (4):
                seeAllProducts();
            case (5):
                choosingSmartphone();
                break;
            default:
                System.out.println("Thank you. Goodbye.");
                break;
        }
    }


    public void choosingSmartphone() {
        System.out.println("""
                What would you like to do next?
                 1. Choose some smartphone
                 2. Turn back to main catalogue
                 3. Exit
                 """);
        int action1 = in.nextInt();
        switch (action1) {
            case (1):
                System.out.println(shopInfo.getSmartPhones().toString());
                System.out.println("Choose a smartphone");
                int choice = in.nextInt();
                System.out.println("You have chosen this smartphone " + (shopInfo.getSmartPhones().get(choice)));
                System.out.println("""
                        What would you like to do next?
                         1. Put it in the cart
                         2. See other products
                         """);
                int action2 = in.nextInt();
                switch (action2) {
                    case (1):
                        shopInfo.getCart().add(shopInfo.getSmartPhones().get(choice));
                        System.out.println("You have added product into your cart");
                        System.out.println("""
                                Would you like to see your cart products?
                                1. Yes
                                2. No
                                """);
                        int action3 = in.nextInt();
                        if (action3 == 1) {
                            cart();
                        }
                        if (action3 == 2) {
                            choosingSmartphone();
                        }
                        break;
                    case (2):
                        choosingSmartphone();
                        break;
                }
                break;
            case (2):
                customerEnter();
                break;
            case (3):
                System.out.println("Thank you. Goodbye.");
                break;
        }
    }

    public void choosingLaptop() {
        System.out.println("""
                What would you like to do next?
                1. Choose some laptop
                2. Turn back to main catalogue
                3. Exit
                """);
        int action1 = in.nextInt();
        switch (action1) {
            case (1):
                System.out.println(shopInfo.getLaptops().toString());
                System.out.println("Choose a laptop");
                int choice = in.nextInt();
                System.out.println("You have chosen this laptop " + (shopInfo.getLaptops().get(choice)));
                System.out.println("""
                        What would you like to do next?
                         1. Put it in the cart
                         2. See other products
                         """);
                int action2 = in.nextInt();
                switch (action2) {
                    case (1):
                        shopInfo.getCart().add(shopInfo.getLaptops().get(choice));
                        System.out.println("You have added product into your cart");
                        System.out.println("""
                                Would you like to see your cart products?
                                1. Yes
                                2. No
                                """);
                        int action3 = in.nextInt();
                        if (action3 == 1) {
                            cart();
                        }
                        if (action3 == 2) {
                            choosingLaptop();
                        }
                        break;
                    case (2):
                        choosingLaptop();
                        break;
                }
                break;
            case (2):
                customerEnter();
                break;
            case (3):
                System.out.println("Thank you. Goodbye.");
                break;
        }
    }

    public void choosingTvSet() {
        System.out.println("""
                What would you like to do next?
                1. Choose some TV-Set
                2. Turn back to main catalogue
                3. Exit
                """);
        int action1 = in.nextInt();
        switch (action1) {
            case (1):
                System.out.println(shopInfo.getTvSets().toString());
                System.out.println("Choose a TV-Set");
                int choice = in.nextInt();
                System.out.println("You have chosen this TV-Set " + (shopInfo.getTvSets().get(choice)));
                System.out.println("""
                        What would you like to do next?
                         1. Put it in the cart
                         2. See other products
                         """);
                int action2 = in.nextInt();
                switch (action2) {
                    case (1):
                        shopInfo.getCart().add(shopInfo.getTvSets().get(choice));
                        System.out.println("You have added product into your cart");
                        System.out.println("""
                                Would you like to see your cart products?
                                1. Yes
                                2. No
                                """);
                        int action3 = in.nextInt();
                        if (action3 == 1) {
                            cart();
                        }
                        if (action3 == 2) {
                            choosingTvSet();
                        }
                        break;
                    case (2):
                        choosingTvSet();
                        break;
                }
                break;
            case (2):
                customerEnter();
                break;
            case (3):
                System.out.println("Thank you. Goodbye.");
                break;
        }
    }


    public void buyingAllProducts() {
        shopInfo.getSmartPhones().values().removeAll(shopInfo.getCart());
        shopInfo.getLaptops().values().removeAll(shopInfo.getCart());
        shopInfo.getTvSets().values().removeAll(shopInfo.getCart());
        System.out.println("You have bought these products: " + shopInfo.getCart().toString());
        System.out.println("""
                What would you like to next?
                 1. Choose some other products
                 2. Exit
                 """);
        int action = in.nextInt();
        switch (action) {
            case (1):
                customerEnter();
                break;
            case (2):
                System.out.println("Thank you. Goodbye.");
                break;
        }
    }

    public void deleteOneProductFromCart() {
        System.out.println("What product do you want to remove from your cart?");
        int delete = in.nextInt();
        shopInfo.getCart().remove(delete);
        System.out.println("You have removed " + shopInfo.getCart().get(delete) + " from your cart");
        cart();
    }

    public void deleteAllProductsFromCart() {
        shopInfo.getCart().removeAll(shopInfo.getCart());
        System.out.println("You have removed all products from your cart");
        System.out.println("""
                What would you like to next?
                 1. Choose some other products
                 2. Exit
                 """);
        int action = in.nextInt();
        switch (action) {
            case (1):
                customerEnter();
                break;
            case (2):
                System.out.println("Thank you. Goodbye.");
                break;
            default:
                deleteAllProductsFromCart();
                break;
        }
    }

    public void cart() {
        System.out.println(shopInfo.getCart().toString());
        System.out.println("""
                What would you like to do with products?
                 1. Buy all of them
                 2. Remove some products
                 3. Choose some other products
                 4. Exit
                 """);
        int action1 = in.nextInt();
        switch (action1) {
            case (1):
                buyingAllProducts();
                break;
            case (2):
                System.out.println("""
                        What products to remove?
                         1. One product
                         2. All of them
                         3. Back to your cart
                         """);
                int remove = in.nextInt();
                switch (remove) {
                    case (1):
                        deleteOneProductFromCart();
                        break;
                    case (2):
                        deleteAllProductsFromCart();
                        break;
                    case (3):
                        cart();
                        break;
                    default:
                        break;
                }
                break;
            case (3):
                customerEnter();
                break;
            case (4):
                System.out.println("Thank you. Goodbye.");
                break;
            default:
                cart();
                break;
        }
    }

    public void seeAllProducts() {
        System.out.println("Smartphones");
        System.out.println(shopInfo.getSmartPhones());
        System.out.println("Laptops");
        System.out.println(shopInfo.getLaptops());
        System.out.println("TV-sets");
        System.out.println(shopInfo.getTvSets());
        System.out.println("""
                What would you like to next?
                 1. Choose one type of products
                 2. Exit
                 """);

        int action = in.nextInt();
        switch (action) {
            case (1):
                customerEnter();
                break;
            case (2):
                System.out.println("Thank you. Goodbye.");
                break;
            default:
                seeAllProducts();
                break;
        }
    }

    public void exit() {
        System.out.println("Try to enter another time");
        System.exit(1);
    }
}
