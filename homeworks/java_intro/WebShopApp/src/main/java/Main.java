
import java.util.*;


public class Main {
    public static void main(String[] args) {
        ShopActions shopActions = new ShopActions();
        List<Product> allProducts = new ArrayList<>();
        List<Product> cart = new ArrayList<>();
        shopActions.getShopInfo().getAdminCredentials().add(new Credentials("admin1", "1111"));
        shopActions.getShopInfo().getAdminCredentials().add(new Credentials("admin2", "2222"));
        shopActions.getShopInfo().getCustomerCredentials().add(new Credentials("customer1", "1111"));
        shopActions.getShopInfo().getCustomerCredentials().add(new Credentials("customer2", "2222"));
        shopActions.getShopInfo().getCustomerCredentials().add(new Credentials("customer3", "3333"));
        shopActions.getShopInfo().setAllProducts(allProducts);
        shopActions.getShopInfo().setCart(cart);
        Map<Integer, Product> smartPhones = new HashMap<Integer, Product>() {{
            put(1, new Product("Smartphone", "Samsung S10", "Blue colour"));
            put(2, new Product("Smartphone", "Iphone X", "Green colour"));
            put(3, new Product("Smartphone", "Xiaomi Redmi 10", "White colour"));
        }};
        shopActions.getShopInfo().setSmartPhones(smartPhones);
        Map<Integer, Product> laptops = new HashMap<Integer, Product>() {{
            put(1, new Product("Laptop", "Dell", "Plastic body"));
            put(2, new Product("Laptop", "HP", "Aluminum body"));
            put(3, new Product("Laptop", "MacBook", "Aluminum body"));
        }};
        shopActions.getShopInfo().setLaptops(laptops);
        Map<Integer, Product> tvSets = new HashMap<Integer, Product>() {{
            put(1, new Product("TV-Set", "Samsung", "80 inch screen"));
            put(2, new Product("TV-Set", "Sony", "100 inch screen"));
            put(3, new Product("TV-Set", "LG", "76 inch screen"));
        }};
        shopActions.getShopInfo().setTvSets(tvSets);
        shopActions.appEnter();
    }
}
