package dbUtils;

import base.BaseTest;
import models.WarehouseProduct;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class WarehouseProductTest extends BaseTest {
    @Test
    public void addNewItem() throws SQLException {
        final String insertSql = "INSERT INTO warehouse_product (warehouse_product_id, product_quantity, product, warehouse) VALUES (?, ?, ?, ?);";

        WarehouseProduct warehouseProduct = new WarehouseProduct(9, "5", "9", "1");

        QueryRunner queryRunner = new QueryRunner();
        int insertedRowsCount = queryRunner.update(
                connection,
                insertSql,
                warehouseProduct.getWarehouse_product_id(), warehouseProduct.getProduct_quantity(), warehouseProduct.getProduct(), warehouseProduct.getWarehouse()
        );

        assertEquals(insertedRowsCount, 1, "Check that row inserted");

        printAllWarehousePositions();
    }

    @Test
    public void deleteItem() throws SQLException {
        final String deleteSql = "DELETE FROM warehouse_product WHERE product=?; ";

        System.out.println("Delete warehouse position");

        QueryRunner queryRunner = new QueryRunner();
        int deletedRowsCount = queryRunner.update(
                connection,
                deleteSql,
                "9"
        );

        assertEquals(deletedRowsCount, 1, "Check that row deleted");

        printAllWarehousePositions();
    }

    @Test
    public void updateItem() throws SQLException {
        final String updateSql = "UPDATE warehouse_product SET product_quantity=? WHERE product=?; ";

        System.out.println("Update warehouse position");
        QueryRunner queryRunner = new QueryRunner();
        int updatedRowsCount = queryRunner.update(
                connection,
                updateSql,
                "10", "9"
        );

        assertEquals(updatedRowsCount, 1, "Check that row updated");


        printAllWarehousePositions();
    }

    private void printAllWarehousePositions() throws SQLException {
        final String sql = "SELECT * FROM warehouse_product;";
        ResultSetHandler<List<WarehouseProduct>> handler = new BeanListHandler<>(WarehouseProduct.class);

        QueryRunner queryRunner = new QueryRunner();
        List<WarehouseProduct> warehouseList = queryRunner.query(connection, sql, handler);

        System.out.println(warehouseList);
    }
}
