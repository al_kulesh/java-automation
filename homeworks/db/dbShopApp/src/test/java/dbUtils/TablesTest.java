package dbUtils;

import base.BaseTest;
import models.Table;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.List;

public class TablesTest extends BaseTest {
    @Test
    public void test() throws SQLException {
        final String sql = "SELECT name FROM sqlite_master WHERE type=? ORDER BY name;";
        ResultSetHandler<List<Table>> handler = new BeanListHandler<>(Table.class);
        QueryRunner queryRunner = new QueryRunner();
        queryRunner.query(connection, sql, handler, "table");
        List<Table> tables = queryRunner.query(connection, sql, handler, "table");

        System.out.println(tables);
    }
}
