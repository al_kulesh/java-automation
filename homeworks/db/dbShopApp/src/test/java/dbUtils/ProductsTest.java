package dbUtils;

import base.BaseTest;
import models.Product;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;


public class ProductsTest extends BaseTest {
    @Test
    public void addNewItem() throws SQLException {
        final String insertSql = "INSERT INTO products (product_id, name, specifications , productType) VALUES (?, ?, ?, ?);";

        Product product = new Product(10,"BlackBerry", "Expensive", "1");

        QueryRunner queryRunner = new QueryRunner();
        int insertedRowsCount = queryRunner.update(
                connection,
                insertSql,
                product.getId(), product.getName(), product.getSpecifications(), product.getProductType()
        );

        assertEquals(insertedRowsCount, 1, "Check that row inserted");

        printAllProducts();
    }

    @Test
    public void deleteItem() throws SQLException {
        final String deleteSql = "DELETE FROM products WHERE specifications=?; ";

        System.out.println("Delete product");

        QueryRunner queryRunner = new QueryRunner();
        int deletedRowsCount = queryRunner.update(
                connection,
                deleteSql,
                "Expensive"
        );

        assertEquals(deletedRowsCount, 1, "Check that row deleted");

        printAllProducts();
    }

    @Test
    public void updateItem() throws SQLException {
        final String updateSql = "UPDATE products SET name=? WHERE specifications=?; ";

        System.out.println("Update product");
        QueryRunner queryRunner = new QueryRunner();
        int updatedRowsCount = queryRunner.update(
                connection,
                updateSql,
                "Motorolla", "Expensive"
        );

        assertEquals(updatedRowsCount, 1, "Check that row updated");


        printAllProducts();
    }

    private void printAllProducts() throws SQLException {
        final String sql = "SELECT * FROM products;";
        ResultSetHandler<List<Product>> handler = new BeanListHandler<>(Product.class);

        QueryRunner queryRunner = new QueryRunner();
        List<Product> productList = queryRunner.query(connection, sql, handler);

        System.out.println(productList);
    }
}
