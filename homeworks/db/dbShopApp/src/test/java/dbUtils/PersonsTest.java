package dbUtils;

import base.BaseTest;
import models.Credentials;
import models.Person;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class PersonsTest extends BaseTest {
    @Test
    public void addNewPerson() throws SQLException {
        final String insertSql = "INSERT INTO persons (first_name , last_name, credentials) VALUES (?, ?, ?);";

        Person person = new Person("Антоний", "Тизенгауз", "1");

        QueryRunner queryRunner = new QueryRunner();
        int insertedRowsCount = queryRunner.update(
                connection,
                insertSql,
                person.getFirst_name(),  person.getLast_name(), person.getCredentials()
        );

        assertEquals(insertedRowsCount, 1, "Check that row inserted");

        printAllPersons();
    }

    @Test
    public void deletePerson() throws SQLException {
        final String deleteSql = "DELETE FROM persons WHERE last_name=?; ";

        System.out.println("Delete person");

        QueryRunner queryRunner = new QueryRunner();
        int deletedRowsCount = queryRunner.update(
                connection,
                deleteSql,
                "Тизенгауз"
        );

        assertEquals(deletedRowsCount, 1, "Check that row deleted");

        printAllPersons();
    }

    @Test
    public void updatePerson() throws SQLException {
        final String updateSql = "UPDATE persons SET first_name=? WHERE last_name=?; ";

        System.out.println("Update person");
        QueryRunner queryRunner = new QueryRunner();
        int updatedRowsCount = queryRunner.update(
                connection,
                updateSql,
                "Франтишек", "Тизенгауз"
        );

        assertEquals(updatedRowsCount, 1, "Check that row updated");


        printAllPersons();
    }

    private void printAllPersons() throws SQLException {
        final String sql = "SELECT * FROM persons;";
        ResultSetHandler<List<Person>> handler = new BeanListHandler<>(Person.class);

        QueryRunner queryRunner = new QueryRunner();
        List<Person> personList = queryRunner.query(connection, sql, handler);

        System.out.println(personList);
    }
}
