package dbUtils;

import base.BaseTest;
import models.Cart;
import models.CartProduct;
import models.Person;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class CartProductTest extends BaseTest {
    @Test
    public void addNewItem() throws SQLException {
        final String insertSql = "INSERT INTO cart_product (cart_product_id, quantity, product, warehouse_position, cart) VALUES (?, ?, ?, ?, ?);";

        CartProduct cartProduct = new CartProduct(1, "1", "3", "3", "1");

        QueryRunner queryRunner = new QueryRunner();
        int insertedRowsCount = queryRunner.update(
                connection,
                insertSql,
                cartProduct.getCart_product_id(), cartProduct.getQuantity(), cartProduct.getProduct(), cartProduct.getWarehouse_position(), cartProduct.getCart()
        );

        assertEquals(insertedRowsCount, 1, "Check that row inserted");

        printAllCartPositions();
    }

    @Test
    public void deleteItem() throws SQLException {
        final String deleteSql = "DELETE FROM cart_product WHERE product=?; ";

        System.out.println("Delete cart position");

        QueryRunner queryRunner = new QueryRunner();
        int deletedRowsCount = queryRunner.update(
                connection,
                deleteSql,
                "3"
        );

        assertEquals(deletedRowsCount, 1, "Check that row deleted");

        printAllCartPositions();
    }

    @Test
    public void updateItem() throws SQLException {
        final String updateSql = "UPDATE cart_product SET quantity=? WHERE product=?; ";

        System.out.println("Update cart position");
        QueryRunner queryRunner = new QueryRunner();
        int updatedRowsCount = queryRunner.update(
                connection,
                updateSql,
                "2", "3"
        );

        assertEquals(updatedRowsCount, 1, "Check that row updated");


        printAllCartPositions();
    }

    private void printAllCartPositions() throws SQLException {
        final String sql = "SELECT * FROM cart_product;";
        ResultSetHandler<List<CartProduct>> handler = new BeanListHandler<>(CartProduct.class);

        QueryRunner queryRunner = new QueryRunner();
        List<CartProduct> cartProductsList = queryRunner.query(connection, sql, handler);

        System.out.println(cartProductsList);
    }
}
