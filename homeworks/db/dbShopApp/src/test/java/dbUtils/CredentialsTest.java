package dbUtils;

import base.BaseTest;
import models.Credentials;
import models.Person;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class CredentialsTest extends BaseTest {
    @Test
    public void addNewItem() throws SQLException {
        final String insertSql = "INSERT INTO credentials (personsrole, login , password) VALUES (?, ?, ?);";

        Credentials credentials = new Credentials("ADMIN", "admin3", "4444");

        QueryRunner queryRunner = new QueryRunner();
        int insertedRowsCount = queryRunner.update(
                connection,
                insertSql,
                credentials.getPersonsrole(), credentials.getLogin(), credentials.getPassword()
        );

        assertEquals(insertedRowsCount, 1, "Check that row inserted");

        printAllCredentials();
    }

    @Test
    public void deleteItem() throws SQLException {
        final String deleteSql = "DELETE FROM credentials WHERE login=?; ";

        System.out.println("Delete credentials");

        QueryRunner queryRunner = new QueryRunner();
        int deletedRowsCount = queryRunner.update(
                connection,
                deleteSql,
                "admin3"
        );

        assertEquals(deletedRowsCount, 1, "Check that row deleted");

        printAllCredentials();
    }

    @Test
    public void updateItem() throws SQLException {
        final String updateSql = "UPDATE credentials SET password=? WHERE login=?; ";

        System.out.println("Update credentials");
        QueryRunner queryRunner = new QueryRunner();
        int updatedRowsCount = queryRunner.update(
                connection,
                updateSql,
                "5555", "admin3"
        );

        assertEquals(updatedRowsCount, 1, "Check that row updated");


        printAllCredentials();
    }

    private void printAllCredentials() throws SQLException {
        final String sql = "SELECT * FROM credentials;";
        ResultSetHandler<List<Credentials>> handler = new BeanListHandler<>(Credentials.class);

        QueryRunner queryRunner = new QueryRunner();
        List<Credentials> credenitialsList = queryRunner.query(connection, sql, handler);

        System.out.println(credenitialsList);
    }
}
