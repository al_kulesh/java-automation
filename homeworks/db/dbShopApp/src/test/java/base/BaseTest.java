package base;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseTest {
    protected Connection connection;

    @BeforeMethod
    public void createConnection () throws SQLException {
        final String projectDir = System.getProperty("user.dir");
        final String connectionString = "jdbc:sqlite:" + projectDir + "/db/shopApp.db";
        connection = DriverManager.getConnection(connectionString);
    }
    @AfterMethod
    public void closeConnection() throws SQLException {
        if(connection != null){
            connection.close();
        }
    }
}
