package handlers;

import models.Product;
import models.ProductType;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.util.HashMap;
import java.util.Map;

public class ProductTypeHandler extends BeanHandler<ProductType> {
    public ProductTypeHandler() {
        super(ProductType.class, new BasicRowProcessor(new BeanProcessor(mapColumstoFields())));
    }

    public static Map<String,String> mapColumstoFields(){
        Map<String,String> columnsToFieldsMap = new HashMap<>();
        columnsToFieldsMap.put("product_type_id", "product_type_id");
        columnsToFieldsMap.put("type", "type");
        return columnsToFieldsMap;
    }
}
