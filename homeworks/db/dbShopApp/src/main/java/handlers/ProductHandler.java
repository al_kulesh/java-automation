package handlers;

import models.Product;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;


import java.util.HashMap;
import java.util.Map;

public class ProductHandler extends BeanHandler<Product> {
    public ProductHandler() {
        super(Product.class, new BasicRowProcessor(new BeanProcessor(mapColumstoFields())));
    }

    public static Map<String,String> mapColumstoFields(){
        Map<String,String> columnsToFieldsMap = new HashMap<>();

        columnsToFieldsMap.put("product_id", "id");
        columnsToFieldsMap.put("name", "name");
        columnsToFieldsMap.put("specifications", "specifications");
        columnsToFieldsMap.put("type", "type");
        return columnsToFieldsMap;
    }
}
