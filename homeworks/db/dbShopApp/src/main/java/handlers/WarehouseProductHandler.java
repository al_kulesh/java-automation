package handlers;

import models.WarehouseProduct;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.util.HashMap;
import java.util.Map;

public class WarehouseProductHandler extends BeanHandler<WarehouseProduct> {
    public WarehouseProductHandler() {
        super(WarehouseProduct.class, new BasicRowProcessor(new BeanProcessor(mapColumstoFields())));
    }

    public static Map<String, String> mapColumstoFields() {
        Map<String, String> columnsToFieldsMap = new HashMap<>();
        columnsToFieldsMap.put("warehouse_product_id", "warehouse_product_id");
        columnsToFieldsMap.put("product_quantity", "quantity");
        columnsToFieldsMap.put("product", "product");
        columnsToFieldsMap.put("warehouse", "warehouse");
        return columnsToFieldsMap;
    }
}
