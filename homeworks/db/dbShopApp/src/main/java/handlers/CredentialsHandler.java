package handlers;

import models.Credentials;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.util.HashMap;
import java.util.Map;

public class CredentialsHandler extends BeanHandler<Credentials> {
    public CredentialsHandler() {
        super(Credentials.class, new BasicRowProcessor(new BeanProcessor(mapColumstoFields())));
    }

    public static Map<String, String> mapColumstoFields() {
        Map<String, String> columnsToFieldsMap = new HashMap<>();
        columnsToFieldsMap.put("credentials_id", "credentials_id");
        columnsToFieldsMap.put("personsrole", "personsrole");
        columnsToFieldsMap.put("login", "login");
        columnsToFieldsMap.put("password", "password");

        return columnsToFieldsMap;
    }
}
