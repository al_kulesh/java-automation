package models;

public class Credentials {
    private int credentials_id;
    private String personsrole;
    private String login;
    private String password;

    public Credentials(String personsrole, String login, String password) {
        this.personsrole = personsrole;
        this.login = login;
        this.password = password;
    }

    public Credentials() {
    }

    public int getCredentials_id() {
        return credentials_id;
    }

    public void setCredentials_id(int credentials_id) {
        this.credentials_id = credentials_id;
    }

    public String getPersonsrole() {
        return personsrole;
    }

    public void setPersonsrole(String personsrole) {
        this.personsrole = personsrole;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Credentials{" +
                "id=" + credentials_id +
                ", personsrole='" + personsrole + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
