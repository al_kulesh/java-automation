package models;

public class WarehouseProduct {
    private int warehouse_product_id;
    private String product_quantity;
    private String product;
    private String warehouse;

    public WarehouseProduct(int warehouse_product_id, String product_quantity, String product, String warehouse) {
        this.warehouse_product_id = warehouse_product_id;
        this.product_quantity = product_quantity;
        this.product = product;
        this.warehouse = warehouse;
    }

    public WarehouseProduct() {
    }

    public int getWarehouse_product_id() {
        return warehouse_product_id;
    }

    public void setWarehouse_product_id(int warehouse_product_id) {
        this.warehouse_product_id = warehouse_product_id;
    }

    public String getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(String product_quantity) {
        this.product_quantity = product_quantity;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    @Override
    public String toString() {
        return "Warehouse_products{" +
                "warehouse_product_id=" + warehouse_product_id +
                ", quantity='" + product_quantity + '\'' +
                ", product='" + product + '\'' +
                ", warehouse='" + warehouse + '\'' +
                '}';
    }
}
