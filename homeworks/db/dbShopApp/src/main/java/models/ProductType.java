package models;

public class ProductType {
    private int product_type_id;
    private String type;

    public ProductType(int product_type_id, String type) {
        this.product_type_id = product_type_id;
        this.type = type;
    }

    public ProductType() {
    }

    public int getProduct_type_id() {
        return product_type_id;
    }

    public void setProduct_type_id(int product_type_id) {
        this.product_type_id = product_type_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ProductType{" +
                "product_type_id=" + product_type_id +
                ", type='" + type + '\'' +
                '}';
    }
}
