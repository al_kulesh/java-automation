package models;

public class CartProduct {
    private int cart_product_id;
    private String quantity;
    private String product;
    private String warehouse_position;
    private String cart;

    public CartProduct(int cart_product_id, String quantity, String product, String warehouse_position, String cart) {
        this.cart_product_id = cart_product_id;
        this.quantity = quantity;
        this.product = product;
        this.warehouse_position = warehouse_position;
        this.cart = cart;
    }

    public CartProduct() {
    }

    public int getCart_product_id() {
        return cart_product_id;
    }

    public void setCart_product_id(int cart_product_id) {
        this.cart_product_id = cart_product_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getWarehouse_position() {
        return warehouse_position;
    }

    public void setWarehouse_position(String warehouse_position) {
        this.warehouse_position = warehouse_position;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }

    @Override
    public String toString() {
        return "Cart_product{" +
                "cart_product_id=" + cart_product_id +
                ", quantity='" + quantity + '\'' +
                ", product='" + product + '\'' +
                ", warehouse_position='" + warehouse_position + '\'' +
                ", cart='" + cart + '\'' +
                '}';
    }
}
