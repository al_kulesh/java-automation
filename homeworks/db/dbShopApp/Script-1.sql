CREATE TABLE products (
	product_id TEXT(1) NOT NULL PRIMARY KEY,
	productType TEXT(255) NOT NULL,
    productName TEXT(255) NOT NULL,
    productSpecifications TEXT(255) NOT NULL
);
CREATE TABLE credentials (
	credentials_id INTEGER NOT NULL PRIMARY KEY,
	personsrole TEXT(255) NOT NULL,
	login TEXT(255) NOT NULL,
	password TEXT(255) NOT NULL
);
CREATE TABLE warehouse (
	warehouse_id INTEGER NOT NULL PRIMARY KEY,
	product_quantity TEXT(255) NOT NULL,
	product TEXT(255) NOT NULL,
	FOREIGN KEY(product) REFERENCES products(product_id)
);

CREATE TABLE persons (
	person_id INTEGER NOT NULL PRIMARY KEY,
	first_name TEXT(255) NOT NULL,
	last_name TEXT(255) NOT NULL,
	credentials TEXT(255) NOT NULL,
	FOREIGN KEY(credentials) REFERENCES credentials(credentials_id)	
);

CREATE TABLE cart (
	cart_id INTEGER NOT NULL PRIMARY KEY,
	product_quantity TEXT(255) NOT NULL,
	product TEXT(255) NOT NULL,
	person TEXT(255) NOT NULL,
	warehouse_position TEXT(255) NOT NULL,
	FOREIGN KEY(person) REFERENCES persons(person_id),
	FOREIGN KEY(product) REFERENCES products(product_id),
	FOREIGN KEY(warehouse_position) REFERENCES warehouse(warehouse_id)
);


INSERT INTO products (product_id, productType, productName , productSpecifications) VALUES (1,"Smartphone", "Samsung S10", "Blue colour");
INSERT INTO products (product_id, productType, productName , productSpecifications) VALUES (2,"Smartphone", "Iphone X", "Green colour");
INSERT INTO products (product_id, productType, productName , productSpecifications) VALUES (3,"Smartphone", "Xiaomi Redmi 10", "White colour");
INSERT INTO products (product_id, productType, productName , productSpecifications) VALUES (4,"Laptop", "Dell", "Plastic body");
INSERT INTO products (product_id, productType, productName , productSpecifications) VALUES (5,"Laptop", "HP", "Aluminum body");
INSERT INTO products (product_id, productType, productName , productSpecifications) VALUES (6,"Laptop", "MacBook", "Aluminum body");
INSERT INTO products (product_id, productType, productName , productSpecifications) VALUES (7,"TV-Set", "Samsung", "80 inch screen");
INSERT INTO products (product_id, productType, productName , productSpecifications) VALUES (8,"TV-Set", "Sony", "100 inch screen");
INSERT INTO products (product_id, productType, productName , productSpecifications) VALUES (9,"TV-Set", "LG", "76 inch screen");


INSERT INTO credentials (personsrole, login, password) VALUES ("ADMIN", "admin1", "1111");
INSERT INTO credentials (personsrole, login, password) VALUES ("ADMIN", "admin2", "2222");
INSERT INTO credentials (personsrole, login, password) VALUES ("CUSTOMER", "customer1", "1111");
INSERT INTO credentials (personsrole, login, password) VALUES ("CUSTOMER", "customer2", "2222");
INSERT INTO credentials (personsrole, login, password) VALUES ("CUSTOMER", "customer3", "3333");

INSERT INTO warehouse (warehouse_id, product_quantity, product) VALUES (1, "4", "1");
INSERT INTO warehouse (warehouse_id, product_quantity, product) VALUES (2, "5", "2");
INSERT INTO warehouse (warehouse_id, product_quantity, product) VALUES (3, "4", "3");
INSERT INTO warehouse (warehouse_id, product_quantity, product) VALUES (4, "2", "4");
INSERT INTO warehouse (warehouse_id, product_quantity, product) VALUES (5, "1", "5");
INSERT INTO warehouse (warehouse_id, product_quantity, product) VALUES (6, "2", "6");
INSERT INTO warehouse (warehouse_id, product_quantity, product) VALUES (7, "7", "7");
INSERT INTO warehouse (warehouse_id, product_quantity, product) VALUES (8, "4", "8");

