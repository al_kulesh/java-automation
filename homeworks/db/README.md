# Задание
 
## Создать базу данных для хранения информации приложения разработанного в рамках задания к первому модулю

# Основные классы/пакеты
- [dbShopApp](dbShopApp) - пакет содержащий модели и тесты для работы с базой данных
- [Table.java](dbShopApp/src/main/java/models/Table.java) - модель для работы с таблицами в целом
- [Product.java](dbShopApp/src/main/java/models/Product.java) - модель для работы с таблицей products
- [Person.java](dbShopApp/src/main/java/models/Person.java) - модель для работы с таблицей persons
- [Credentials.java](dbShopApp/src/main/java/models/Credentials.java) - модель для работы с таблицей credentials
- [Cart.java](dbShopApp/src/main/java/models/Cart.java) - модель для работы с таблицей cart
- [Warehouse.java](dbShopApp/src/main/java/models/Warehouse.java) - модель для работы с таблицей warehouse
- [JdbcConnectionTest.java](dbShopApp/src/test/java/connection/JdbcConnectionTest.java) - тест на проверку соединения с базой данных	
- [TablesTest.java](dbShopApp/src/test/java/dbUtils/TablesTest.java) - тест на проверку наличия таблиц
- [ProductsTest.java](dbShopApp/src/test/java/dbUtils/ProductsTest.java) - тест на проверку добавления/удаления/обновления записей таблицы products
- [PersonsTest.java](dbShopApp/src/test/java/dbUtils/PersonsTest.java) - тест на проверку добавления/удаления/обновления записей таблицы persons
- [CredentialsTest.java](dbShopApp/src/test/java/dbUtils/CredentialsTest.java) - тест на проверку добавления/удаления/обновления записей таблицы credentials
- [CartTest.java](dbShopApp/src/test/java/dbUtils/CartTest.java) - тест на проверку добавления/удаления/обновления записей таблицы cart
- [WarehouseTest.java](dbShopApp/src/test/java/dbUtils/WarehouseTest.java) - тест на проверку добавления/удаления/обновления записей таблицы warehouse
- [Script-1.sql](dbShopApp/Script-1.sql) - скрипт, который был использован для создания таблиц в базе данных
- [shopApp.db](dbShopApp/db/shopApp.db) - база данных


# Скриншоты успешного выполнения
- [Tables list](images/TablesTest.png)
- [Product add test](images/ProductTestAddItem.png)
- [Product update test](images/ProductTestUpdateItem.png)
- [Product delete test](images/ProductTestDeleteItem.png)
- [Person add test](images/PersonTestAddPer.png)
- [Person update test](images/PersonTestUpdatePer.png)
- [Person delete test](images/PersonTestDeletePer.png)
- [Credentials add test](images/CredentialsTestAddItem.png)
- [Credentials update test](images/CredentialsTestUpdateItem.png)
- [Credentials delete test](images/CredentialsTestDeleteItem.png)
- [Cart position add test](images/CartTestAddItem.png)
- [Cart position update test](images/CartTestUpdateItem.png)
- [Cart position delete test](images/CartTestDeleteItem.png)
- [Warehouse add test](images/WarehouseTestAddItem.png)
- [Warehouse update test](images/WarehouseTestUpdateItem.png)
- [Warehouse delete test](images/WarehouseTestDeleteItem.png)
- [JDBC connection test](images/JDBCconnectionTest.png)
