# Задание
 
## Реализовать архитектуру для работы с базой данных и XML файлом небольшого консольного приложения интернет-магазина


# Основные классы/пакеты

### Основные пакеты		
- [xmlDbWebShop](xmlDbWebShop) - пакет содержащий модели, архитектуру, и тесты, для работы с базой данных и xml-файлами
- [xml](xmlDbWebShop/xml) - репозиторий содержащий основные xml-файлы

### Основные классы
- [Table.java](xmlDbWebShop/src/main/java/models/Table.java) - модель для работы с таблицами в целом
- [Product.java](xmlDbWebShop/src/main/java/models/Product.java) - модель для работы с таблицей products
- [ProductType.java](xmlDbWebShop/src/main/java/models/ProductType.java) - модель для работы с таблицей product_type
- [Person.java](xmlDbWebShop/src/main/java/models/Person.java) - модель для работы с таблицей persons
- [Credentials.java](xmlDbWebShop/src/main/java/models/Credentials.java) - модель для работы с таблицей credentials
- [Cart.java](xmlDbWebShop/src/main/java/models/Cart.java) - модель для работы с таблицей cart
- [CartProduct.java](xmlDbWebShop/src/main/java/models/CartProduct.java) - модель для работы с таблицей cart_product
- [Warehouse.java](xmlDbWebShop/src/main/java/models/Warehouse.java) - модель для работы с таблицей warehouse
- [WarehouseProduct.java](xmlDbWebShop/src/main/java/models/WarehouseProduct.java) - модель для работы с таблицей warehouse_product	
- [ProductDaoImpl.java](xmlDbWebShop/src/main/java/dao/impl/ProductDaoImpl.java) - модель Product класса из архитектуры для работы с xml-файлами
- [ProductTypeDaoImpl.java](xmlDbWebShop/src/main/java/dao/impl/ProductTypeDaoImpl.java) - модель ProductType класса из архитектуры для работы с xml-файлами
- [PersonDaoImpl.java](xmlDbWebShop/src/main/java/dao/impl/PersonDaoImpl.java) - модель Person класса из архитектуры для работы с xml-файлами
- [CredentialsDaoImpl.java](xmlDbWebShop/src/main/java/dao/impl/CredentialsDaoImpl.java) - модель Credentials  класса из архитектуры для работы с xml-файлами
- [CartDaoImpl.java](xmlDbWebShop/src/main/java/dao/impl/CartDaoImpl.java) - модель Cart  класса из архитектуры для работы с xml-файлами
- [CartProductDaoImpl.java](xmlDbWebShop/src/main/java/dao/impl/CartDaoImpl.java) - модель CartProduct  класса из архитектуры для работы с xml-файлами
- [WarehouseDaoImpl.java](xmlDbWebShop/src/main/java/dao/impl/WarehouseDaoImpl.java) - модель Warehouse  класса из архитектуры для работы с xml-файлами
- [WarehouseProductDaoImpl.java](xmlDbWebShop/src/main/java/dao/impl/WarehouseProductDaoImpl.java) - модель WarehouseProduct класса из архитектуры для работы с xml-файлами

### Основные тесты
- [JdbcConnectionTest.java](xmlDbWebShop/src/test/java/connection/JdbcConnectionTest.java) - тест на проверку соединения с базой данных	
- [TablesTest.java](xmlDbWebShop/src/test/java/dbXmlUtils/TablesTest.java) - тест на проверку наличия таблиц
- [ProductsTest.java](xmlDbWebShop/src/test/java/dbXmlUtils/ProductsTest.java) - тест на проверку добавления/удаления/обновления записей таблицы products и файла products_dest.xml
- [ProductsTest.java](xmlDbWebShop/src/test/java/dbXmlUtils/ProductsTest.java) - тест на проверку добавления/удаления/обновления записей таблицы products и файла products_dest.xml
- [PersonsTest.java](xmlDbWebShop/src/test/java/dbXmlUtils/PersonsTest.java) - тест на проверку добавления/удаления/обновления записей таблицы persons и файла persons_dest.xml
- [CredentialsTest.java](xmlDbWebShop/src/test/java/dbXmlUtils/CredentialsTest.java) - тест на проверку добавления/удаления/обновления записей таблицы credentials и файла credentials_list_dest.xml
- [CartTest.java](xmlDbWebShop/src/test/java/dbXmlUtils/CartTest.java) - тест на проверку добавления/удаления/обновления записей таблицы cart и файла cart_list.xml
- [CartProductTest.java](xmlDbWebShop/src/test/java/dbXmlUtils/CartProductTest.java) - тест на проверку добавления/удаления/обновления записей таблицы cart_product и файла cart_product_list_dest.xml
- [WarehouseTest.java](xmlDbWebShop/src/test/java/dbXmlUtils/WarehouseTest.java) - тест на проверку добавления/удаления/обновления записей таблицы warehouse и файла warehouse_list_dest.xml
- [WarehouseProductTest.java](xmlDbWebShop/src/test/java/dbXmlUtils/WarehouseProductTest.java) - тест на проверку добавления/удаления/обновления записей таблицы warehouse_product и файла warehouse_product_list_dest.xml

### Основные базы данных
- [Script-1.sql](xmlDbWebShop/Script-1.sql) - скрипт, который был использован для создания таблиц в базе данных
- [shopApp.db](xmlDbWebShop/db/shopApp.db) - база данных


# Скриншоты успешного выполнения
- [Product add to DB test](images/ProductAddDB.png)
- [Product update in DB test](images/ProductUpdateDB.png)
- [Product delete from DB test](images/ProductDeleteDB.png)
- [Product add to XML-file test](images/ProductAddXML.png)
- [Product update in XML-file test](images/ProductUpdateXML.png)
- [Product delete from XML-file test](images/ProductDeleteXML.png)
- [Product type add to DB test](images/ProductTypeAddDB.png)
- [Product type update in DB test](images/ProductTypeUpdateDB.png)
- [Product type delete from DB test](images/ProductTypeDeleteDB.png)
- [Product type add to XML-file test](images/ProductTypeAddXML.png)
- [Product type update in XML-file test](images/ProductTypeUpdateXML.png)
- [Product type delete from XML-file test](images/ProductTypeDeleteXML.png)
- [Person add to DB test](images/PersonAddDB.png)
- [Person update in DB test](images/PersonUpdateDB.png)
- [Person delete from DB test](images/PersonDeleteDB.png)
- [Person add to XML-file test](images/PersonAddXML.png)
- [Person update in XML-file test](images/PersonUpdateXML.png)
- [Person delete from XML-file test](images/PersonDeleteXML.png)
- [Credentials add to DB test](images/CredentialsAddDB.png)
- [Credentials update in DB test](images/CredentialsUpdateDB.png)
- [Credentials delete from DB test](images/CredentialsDeleteDB.png)
- [Credentials add to XML-file test](images/CredentialsAddXML.png)
- [Credentials update XML-file DB test](images/CredentialsUpdateXML.png)
- [Credentials delete XML-file DB test](images/CredentialsDeleteXML.png)
- [Cart add to DB test](images/CartAddDB.png)
- [Cart update in DB test](images/CartUpdateDB.png)
- [Cart delete from DB test](images/CartDeleteDB.png)
- [Cart add to XML-file test](images/CartAddXML.png)
- [Cart update in XML-file test](images/CartUpdateXML.png)
- [Cart delete from XML-file test](images/CartDeleteXML.png)
- [Cart product add to DB test](images/CartProductAddDB.png)
- [Cart product update in DB test](images/CartProductUpdateDB.png)
- [Cart product delete from DB test](images/CartProductDeleteDB.png)
- [Cart product add to XML-file test](images/CartProductAddXML.png)
- [Cart product update in XML-file test](images/CartProductUpdateXML.png)
- [Cart product delete from XML-file test](images/CartProductDeleteXML.png)
- [Warehouse add to DB test](images/WarehouseAddDB.png)
- [Warehouse update in DB test](images/WarehouseUpdateDB.png)
- [Warehouse delete from DB test](images/WarehouseDeleteDB.png)
- [Warehouse product add to DB test](images/WarehouseProductAddDB.png)
- [Warehouse product update in DB test](images/WarehouseProductUpdateDB.png)
- [Warehouse product delete from DB test](images/WarehouseProductDeleteDB.png)
