CREATE TABLE products (
	product_id TEXT(1) NOT NULL PRIMARY KEY,
    name TEXT(255) NOT NULL,
    specifications TEXT(255) NOT NULL,
    productType TEXT(255) NOT NULL,
	FOREIGN KEY(productType) REFERENCES product_type(product_type_id)
);

CREATE TABLE product_type (
	product_type_id INTEGER NOT NULL PRIMARY KEY,
	productType TEXT(255) NOT NULL 
);

CREATE TABLE credentials (
	credentials_id INTEGER NOT NULL PRIMARY KEY,
	personsrole TEXT(255) NOT NULL,
	login TEXT(255) NOT NULL,
	password TEXT(255) NOT NULL
);

CREATE TABLE warehouse (
	warehouse_id INTEGER NOT NULL PRIMARY KEY,
	name TEXT(255) NOT NULL
);

CREATE TABLE warehouse_product (
	warehouse_product_id INTEGER NOT NULL PRIMARY KEY,
	product_quantity TEXT(255) NOT NULL,
	product TEXT(255) NOT NULL,
	warehouse TEXT(255) NOT NULL,
	FOREIGN KEY(product) REFERENCES products(product_id),
	FOREIGN KEY(warehouse) REFERENCES products(warehouse_id)
);


CREATE TABLE persons (
	person_id INTEGER NOT NULL PRIMARY KEY,
	first_name TEXT(255) NOT NULL,
	last_name TEXT(255) NOT NULL,
	credentials TEXT(255) NOT NULL,
	FOREIGN KEY(credentials) REFERENCES credentials(credentials_id)	
);

CREATE TABLE cart_product (
	cart_product_id INTEGER NOT NULL PRIMARY KEY,
	quantity TEXT(255) NOT NULL,
	product TEXT(255) NOT NULL,
	warehouse_position TEXT(255) NOT NULL,
	cart TEXT(255) NOT NULL,
	FOREIGN KEY(product) REFERENCES products(product_id),
	FOREIGN KEY(warehouse_position) REFERENCES warehouse_product(warehouse_product_id)
	FOREIGN KEY(cart) REFERENCES cart(cart_id)
);

CREATE TABLE cart (
	cart_id INTEGER NOT NULL PRIMARY KEY,
	person TEXT(255) NOT NULL
);


INSERT INTO products (product_id, name, specifications, productType) VALUES (1, "Samsung S10", "Blue colour", "1");
INSERT INTO products (product_id, name, specifications, productType) VALUES (2, "Iphone X", "Green colour", "1");
INSERT INTO products (product_id, name, specifications, productType) VALUES (3, "Xiaomi Redmi 10", "White colour", "1");
INSERT INTO products (product_id, name, specifications, productType) VALUES (4, "Dell", "Plastic body", "2");
INSERT INTO products (product_id, name, specifications, productType) VALUES (5, "HP", "Aluminum body", "2");
INSERT INTO products (product_id, name, specifications, productType) VALUES (6, "MacBook", "Aluminum body", "2");
INSERT INTO products (product_id, name, specifications, productType) VALUES (7, "Samsung", "80 inch screen", "3");
INSERT INTO products (product_id, name, specifications, productType) VALUES (8, "Sony", "100 inch screen", "3");
INSERT INTO products (product_id, name, specifications, productType) VALUES (9, "LG", "76 inch screen", "3");

INSERT INTO product_type (product_type_id, productType) VALUES (1, "Smartphone");
INSERT INTO product_type (product_type_id, productType) VALUES (2, "Laptop");
INSERT INTO product_type (product_type_id, productType) VALUES (3, "TV-Set");

INSERT INTO persons (first_name , last_name, credentials) VALUES ("��������", "�������", "1");
INSERT INTO persons (first_name , last_name, credentials) VALUES ("���", "������", "2");
INSERT INTO persons (first_name , last_name, credentials) VALUES ("����", "������", "3");
INSERT INTO persons (first_name , last_name, credentials) VALUES ("������", "����������", "4");
INSERT INTO persons (first_name , last_name, credentials) VALUES ("���������", "�����", "5");

INSERT INTO credentials (personsrole, login, password) VALUES ("ADMIN", "admin1", "1111");
INSERT INTO credentials (personsrole, login, password) VALUES ("ADMIN", "admin2", "2222");
INSERT INTO credentials (personsrole, login, password) VALUES ("CUSTOMER", "customer1", "1111");
INSERT INTO credentials (personsrole, login, password) VALUES ("CUSTOMER", "customer2", "2222");
INSERT INTO credentials (personsrole, login, password) VALUES ("CUSTOMER", "customer3", "3333");

INSERT INTO warehouse_product (warehouse_product_id, product_quantity, product, warehouse) VALUES (1, "4", "1", "1");
INSERT INTO warehouse_product (warehouse_product_id, product_quantity, product, warehouse) VALUES (2, "5", "2", "1");
INSERT INTO warehouse_product (warehouse_product_id, product_quantity, product, warehouse) VALUES (3, "4", "3", "1");
INSERT INTO warehouse_product (warehouse_product_id, product_quantity, product, warehouse) VALUES (4, "2", "4", "1");
INSERT INTO warehouse_product (warehouse_product_id, product_quantity, product, warehouse) VALUES (5, "1", "5", "1");
INSERT INTO warehouse_product (warehouse_product_id, product_quantity, product, warehouse) VALUES (6, "2", "6", "1");
INSERT INTO warehouse_product (warehouse_product_id, product_quantity, product, warehouse) VALUES (7, "7", "7", "1");
INSERT INTO warehouse_product (warehouse_product_id, product_quantity, product, warehouse) VALUES (8, "4", "8", "1");

INSERT INTO warehouse (warehouse_id, name) VALUES (1, "Main");

INSERT INTO cart (cart_id, person) VALUES (1, "3");
INSERT INTO cart (cart_id, person) VALUES (2, "4");
INSERT INTO cart (cart_id, person) VALUES (3, "5");

