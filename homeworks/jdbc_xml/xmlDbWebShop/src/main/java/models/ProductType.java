package models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "producttype")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductType {
    private int product_type_id;
    private String productType;

    public ProductType(int product_type_id, String productType) {
        this.product_type_id = product_type_id;
        this.productType = productType;
    }

    public ProductType() {
    }

    public int getProduct_type_id() {
        return product_type_id;
    }

    public void setProduct_type_id(int product_type_id) {
        this.product_type_id = product_type_id;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    @Override
    public String toString() {
        return "ProductType{" +
                "product_type_id=" + product_type_id +
                ", type='" + productType + '\'' +
                '}';
    }
}
