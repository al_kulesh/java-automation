package models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "warehouse")
@XmlAccessorType(XmlAccessType.FIELD)
public class Warehouse {
    private int warehouse_id;
    private String name;

    public Warehouse(int warehouse_id, String name) {
        this.warehouse_id = warehouse_id;
        this.name = name;
    }

    public Warehouse() {
    }

    public int getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(int warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "warehouse_id=" + warehouse_id +
                ", name='" + name + '\'' +
                '}';
    }
}
