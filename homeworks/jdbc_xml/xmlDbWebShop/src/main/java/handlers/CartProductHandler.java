package handlers;

import models.CartProduct;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.util.HashMap;
import java.util.Map;

public class CartProductHandler extends BeanHandler<CartProduct> {
    public CartProductHandler() {
        super(CartProduct.class, new BasicRowProcessor(new BeanProcessor(mapColumstoFields())));
    }

    public static Map<String, String> mapColumstoFields() {
        Map<String, String> columnsToFieldsMap = new HashMap<>();
        columnsToFieldsMap.put("cart_product_id","cart_product_id");
        columnsToFieldsMap.put("quantity", "quantity");
        columnsToFieldsMap.put("product", "product");
        columnsToFieldsMap.put("warehouse_position", "warehouse_position");
        columnsToFieldsMap.put("cart","cart");
        return columnsToFieldsMap;
    }
}
