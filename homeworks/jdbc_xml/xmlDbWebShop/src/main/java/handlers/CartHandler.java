package handlers;

import models.Cart;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.util.HashMap;
import java.util.Map;


public class CartHandler extends BeanHandler<Cart> {
    public CartHandler() {
        super(Cart.class, new BasicRowProcessor(new BeanProcessor(mapColumstoFields())));
    }

    public static Map<String, String> mapColumstoFields() {
        Map<String, String> columnsToFieldsMap = new HashMap<>();
        columnsToFieldsMap.put("cart_id","cart_id");
        columnsToFieldsMap.put("person", "person");
        return columnsToFieldsMap;
    }
}
