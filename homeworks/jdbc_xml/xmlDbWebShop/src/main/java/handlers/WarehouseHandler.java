package handlers;

import models.Warehouse;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.util.HashMap;
import java.util.Map;

public class WarehouseHandler extends BeanHandler<Warehouse> {
    public WarehouseHandler() {
        super(Warehouse.class, new BasicRowProcessor(new BeanProcessor(mapColumstoFields())));
    }

    public static Map<String, String> mapColumstoFields() {
        Map<String, String> columnsToFieldsMap = new HashMap<>();
        columnsToFieldsMap.put("warehouse_id", "warehouse_id");
        columnsToFieldsMap.put("name", "name");
        return columnsToFieldsMap;
    }
}
