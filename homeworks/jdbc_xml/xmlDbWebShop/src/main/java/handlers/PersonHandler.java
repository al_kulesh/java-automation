package handlers;

import models.Person;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.util.HashMap;
import java.util.Map;

public class PersonHandler extends BeanHandler<Person> {
    public PersonHandler() {
        super(Person.class, new BasicRowProcessor(new BeanProcessor(mapColumstoFields())));
    }

    public static Map<String, String> mapColumstoFields() {
        Map<String, String> columnsToFieldsMap = new HashMap<>();
        columnsToFieldsMap.put("person_id", "person_id");
        columnsToFieldsMap.put("personsrole", "personsrole");
        columnsToFieldsMap.put("last_name", "last_name");
        columnsToFieldsMap.put("credentials", "credentials");
        return columnsToFieldsMap;
    }
}
