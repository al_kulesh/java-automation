package dao;

import models.Cart;
import models.CartProduct;

import java.util.List;

public interface CartDao {
    public List<Cart> getAllCarts();
    public Cart getCart(int num);
    public void addCart(Cart cart);
    public void updateCart(Cart cart);
    public void deleteCart(Cart cart);
}
