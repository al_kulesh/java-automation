package dao;

import models.Product;
import models.ProductType;

import java.util.List;

public interface ProductTypeDao {
    public List<ProductType> getAllProductTypes();
    public ProductType getProductType(int num);
    public void addProductType(ProductType productType);
    public void updateProductType(ProductType productType);
    public void deleteProductType(ProductType productType);
}
