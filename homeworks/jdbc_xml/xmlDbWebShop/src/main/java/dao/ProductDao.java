package dao;

import models.Product;

import java.util.List;

public interface ProductDao {
    public List<Product> getAllProducts();
    public Product getProduct(int num);
    public void addProduct(Product product);
    public void updateProduct(Product product);
    public void deleteProduct(Product product);
}
