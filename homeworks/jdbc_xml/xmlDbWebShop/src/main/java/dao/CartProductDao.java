package dao;

import models.CartProduct;
import models.Product;

import java.util.List;

public interface CartProductDao {
    public List<CartProduct> getAllCartProduct();
    public CartProduct getCartProduct(int num);
    public void addCartProduct(CartProduct cartProduct);
    public void updateCartProduct(CartProduct cartProduct);
    public void deleteCartProduct(CartProduct cartProduct);
}
