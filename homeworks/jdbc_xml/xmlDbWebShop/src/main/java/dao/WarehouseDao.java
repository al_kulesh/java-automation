package dao;


import models.Warehouse;

import java.util.List;

public interface WarehouseDao {
    public List<Warehouse> getAllWarehouses();
    public Warehouse getWarehouse(int num);
    public void addWarehouse(Warehouse warehouse);
    public void updateWarehouse(Warehouse warehouse);
    public void deleteWarehouse(Warehouse warehouse);
}
