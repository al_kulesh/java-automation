package dao;

import models.CartProduct;
import models.Credentials;

import java.util.List;

public interface CredentialsDao {
    public List<Credentials> getAllCredentials();
    public Credentials getCredentials(int num);
    public void addCredentials(Credentials credentials);
    public void updateCredentials(Credentials credentials);
    public void deleteCredentials(Credentials credentials);
}
