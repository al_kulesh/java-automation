package dao;

import models.Credentials;
import models.Person;

import java.util.List;

public interface PersonDao {
    public List<Person> getAllPersons();
    public Person getPerson(int num);
    public void addPerson(Person person);
    public void updatePerson(Person person);
    public void deletePerson(Person person);
}
