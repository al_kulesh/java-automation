package dao.impl;

import dao.CartProductDao;
import models.CartProduct;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name="cartproductlist")
@XmlAccessorType(XmlAccessType.FIELD)
public class CartProductDaoImpl implements CartProductDao {

    @XmlElementWrapper(name="cartproducts")
    @XmlElement(name = "cartproduct")
    List<CartProduct> cartProducts;

    public CartProductDaoImpl() {
    }

    @Override
    public List<CartProduct> getAllCartProduct() {
        return cartProducts;
    }

    @Override
    public CartProduct getCartProduct(int num) {
        return cartProducts.get(num - 1);
    }

    @Override
    public void addCartProduct(CartProduct cartProduct) {
        cartProducts.add(cartProduct);
    }


    @Override
    public void updateCartProduct(CartProduct cartProduct) {
        cartProducts.get(cartProduct.getCart_product_id() - 1).setQuantity(cartProduct.getQuantity());
    }

    @Override
    public void deleteCartProduct(CartProduct cartProduct) {
        cartProducts.remove(cartProduct.getCart_product_id() - 1);
    }

    @Override
    public String toString() {
        return "CartProductDaoImpl{" +
                "cartProducts=" + cartProducts +
                '}';
    }
}
