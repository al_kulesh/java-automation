package dao.impl;

import dao.ProductDao;
import models.Product;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name="productlist")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductDaoImpl implements ProductDao {

    @XmlElementWrapper(name="products")
    @XmlElement(name = "product")
    List<Product> products;

    public ProductDaoImpl(){

    }

    @Override
    public List<Product> getAllProducts() {
        return products;
    }

    @Override
    public Product getProduct(int num) {
        return products.get(num - 1);
    }

    @Override
    public void addProduct(Product product){
        products.add(product);
    }

    @Override
    public void updateProduct(Product product) {
        products.get(product.getProduct_id() - 1).setName(product.getName());
    }

    @Override
    public void deleteProduct(Product product) {
        products.remove(product.getProduct_id() - 1);
    }

    @Override
    public String toString() {
        return "ProductDaoImpl{" +
                "products=" + products +
                '}';
    }
}
