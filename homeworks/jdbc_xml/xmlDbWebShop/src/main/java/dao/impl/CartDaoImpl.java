package dao.impl;

import dao.CartDao;
import models.Cart;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name="cartlist")
@XmlAccessorType(XmlAccessType.FIELD)
public class CartDaoImpl implements CartDao {

    @XmlElementWrapper(name="carts")
    @XmlElement(name = "cart")
    List<Cart> carts;

    public CartDaoImpl() {
    }

    @Override
    public List<Cart> getAllCarts() {
        return carts;
    }

    @Override
    public Cart getCart(int num) {
        return carts.get(num - 1);
    }

    @Override
    public void addCart(Cart cart) {
        carts.add(cart);
    }

    @Override
    public void updateCart(Cart cart) {
        carts.get(cart.getCart_id() - 1).setPerson(cart.getPerson());
    }

    @Override
    public void deleteCart(Cart cart) {
        carts.remove(cart.getCart_id() - 1);
    }

    @Override
    public String toString() {
        return "CartDaoImpl{" +
                "carts=" + carts +
                '}';
    }
}
