package dao.impl;

import dao.ProductTypeDao;
import models.ProductType;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name="producttypelist")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductTypeDaoImpl implements ProductTypeDao {

    @XmlElementWrapper(name="producttypes")
    @XmlElement(name = "producttype")
    List<ProductType> productTypes;

    @Override
    public List<ProductType> getAllProductTypes() {
        return productTypes;
    }

    @Override
    public ProductType getProductType(int num) {
        return productTypes.get(num - 1);
    }

    @Override
    public void addProductType(ProductType productType) {
        productTypes.add(productType);
    }

    @Override
    public void updateProductType(ProductType productType) {
        productTypes.get(productType.getProduct_type_id() - 1).setProductType(productType.getProductType());
    }

    @Override
    public void deleteProductType(ProductType productType) {
        productTypes.remove(productType.getProduct_type_id() - 1);
    }

    @Override
    public String toString() {
        return "ProductTypeDaoImpl{" +
                "productTypes=" + productTypes +
                '}';
    }
}
