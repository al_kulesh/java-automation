package dao.impl;

import dao.CredentialsDao;
import models.Credentials;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name="credentialslist")
@XmlAccessorType(XmlAccessType.FIELD)
public class CredentialsDaoImpl implements CredentialsDao {

    @XmlElementWrapper(name="allcredentials")
    @XmlElement(name = "credentials")
    List<Credentials> allcredentials;

    public CredentialsDaoImpl() {
    }

    @Override
    public List<Credentials> getAllCredentials() {
        return allcredentials;
    }

    @Override
    public Credentials getCredentials(int num) {
        return allcredentials.get(num - 1);
    }

    @Override
    public void addCredentials(Credentials credentials) {
        allcredentials.add(credentials);
    }

    @Override
    public void updateCredentials(Credentials credentials) {
        allcredentials.get(credentials.getCredentials_id() - 1).setPassword(credentials.getPassword());
    }

    @Override
    public void deleteCredentials(Credentials credentials) {
        allcredentials.remove(credentials.getCredentials_id() - 1);
    }

    @Override
    public String toString() {
        return "CredentialsDaoImpl{" +
                "credentialsList=" + allcredentials +
                '}';
    }
}
