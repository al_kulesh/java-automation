package dao.impl;

import dao.WarehouseProductDao;
import models.WarehouseProduct;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name="warehouseproductlist")
@XmlAccessorType(XmlAccessType.FIELD)
public class WarehouseProductDaoImpl implements WarehouseProductDao {

    @XmlElementWrapper(name="warehouseproducts")
    @XmlElement(name = "warehouseproduct")
    List<WarehouseProduct> warehouseProducts;

    public WarehouseProductDaoImpl() {
    }

    @Override
    public List<WarehouseProduct> getAllWarehouseProducts() {
        return warehouseProducts;
    }

    @Override
    public WarehouseProduct getWarehouseProduct(int num) {
        return warehouseProducts.get(num - 1);
    }

    @Override
    public void addWarehouseProduct(WarehouseProduct warehouseProduct) {
        warehouseProducts.add(warehouseProduct);
    }


    @Override
    public void updateWarehouseProduct(WarehouseProduct warehouseProduct) {
        warehouseProducts.get(warehouseProduct.getWarehouse_product_id() - 1).setProduct_quantity(warehouseProduct.getProduct_quantity());
    }

    @Override
    public void deleteWarehouseProduct(WarehouseProduct warehouseProduct) {
        warehouseProducts.remove(warehouseProduct.getWarehouse_product_id() - 1);
    }

    @Override
    public String toString() {
        return "WarehouseProductDaoImpl{" +
                "warehouseProducts=" + warehouseProducts +
                '}';
    }
}
