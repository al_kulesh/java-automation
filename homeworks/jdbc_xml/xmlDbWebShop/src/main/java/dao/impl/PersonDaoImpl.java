package dao.impl;

import dao.PersonDao;
import models.Person;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name="personlist")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonDaoImpl implements PersonDao {

    @XmlElementWrapper(name="persons")
    @XmlElement(name = "person")
    List<Person> persons;

    public PersonDaoImpl(){

    }

    @Override
    public List<Person> getAllPersons() {
        return persons;
    }


    @Override
    public Person getPerson(int num) {
        return persons.get(num - 1);
    }

    @Override
    public void addPerson(Person person){
        persons.add(person);
    }

    @Override
    public void updatePerson(Person person) {
        persons.get(person.getPerson_id() - 1).setFirst_name(person.getFirst_name());
    }

    @Override
    public void deletePerson(Person person) {
        persons.remove(person.getPerson_id() - 1);
    }

    @Override
    public String toString() {
        return "PersonDaoImpl{" +
                "persons=" + persons +
                '}';
    }
}
