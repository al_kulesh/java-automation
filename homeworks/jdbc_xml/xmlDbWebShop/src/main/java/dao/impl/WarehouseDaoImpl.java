package dao.impl;

import dao.WarehouseDao;
import models.Warehouse;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name="warehouselist")
@XmlAccessorType(XmlAccessType.FIELD)
public class WarehouseDaoImpl  implements WarehouseDao {

    @XmlElementWrapper(name="warehouses")
    @XmlElement(name = "warehouse")
    List<Warehouse> warehouses;

    @Override
    public List<Warehouse> getAllWarehouses() {
        return warehouses;
    }

    @Override
    public Warehouse getWarehouse(int num) {
        return warehouses.get(num - 1);
    }

    @Override
    public void addWarehouse(Warehouse warehouse) {
        warehouses.add(warehouse);
    }

    @Override
    public void updateWarehouse(Warehouse warehouse) {
        warehouses.get(warehouse.getWarehouse_id() - 1).setName(warehouse.getName());
    }

    @Override
    public void deleteWarehouse(Warehouse warehouse) {
        warehouses.remove(warehouse.getWarehouse_id() - 1);
    }

    @Override
    public String toString() {
        return "WarehouseDaoImpl{" +
                "warehouses=" + warehouses +
                '}';
    }
}
