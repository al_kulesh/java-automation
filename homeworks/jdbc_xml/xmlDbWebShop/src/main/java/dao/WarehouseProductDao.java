package dao;

import models.WarehouseProduct;

import java.util.List;

public interface WarehouseProductDao {
    public List<WarehouseProduct> getAllWarehouseProducts();
    public WarehouseProduct getWarehouseProduct(int num);
    public void addWarehouseProduct(WarehouseProduct warehouseProduct);
    public void updateWarehouseProduct(WarehouseProduct warehouseProduct);
    public void deleteWarehouseProduct(WarehouseProduct warehouseProduct);
}
