package dbXmlUtils;

import base.BaseTest;
import dao.impl.CartDaoImpl;
import models.Cart;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;
import parsers.XmlParser;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class CartTest extends BaseTest {

    public static final boolean isDB = false;

    @Test
    public void addNewItem() throws SQLException {
        if(isDB == true) {
            final String insertSql = "INSERT INTO cart (cart_id, person) VALUES (?, ?);";

            Cart cart = new Cart(4, "3");

            QueryRunner queryRunner = new QueryRunner();
            int insertedRowsCount = queryRunner.update(
                    connection,
                    insertSql,
                    cart.getCart_id(), cart.getPerson()
            );

            assertEquals(insertedRowsCount, 1, "Check that row inserted");

            printAllCarts();
        } else {
            File sourceFile = new File(System.getProperty("user.dir") + "/xml/cart_list.xml");
            File destFile = new File(System.getProperty("user.dir") + "/xml/cart_list_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/cart_list.xsd");
            CartDaoImpl cartDaoImpl = (CartDaoImpl) XmlParser.deserialize(sourceFile, schemaFile, CartDaoImpl.class);
            Cart cart = new Cart();
            cart.setCart_id(4);
            cart.setPerson("3");
            cartDaoImpl.addCart(cart);
            XmlParser.serialize(cartDaoImpl, destFile);
            assertEquals(cart, cartDaoImpl.getCart(4));
            Cart addedCart = cartDaoImpl.getAllCarts()
                    .stream()
                    .filter(e -> e.equals(cart))
                    .findAny()
                    .orElse(null);
            assertEquals(addedCart, cart);
            System.out.println(cartDaoImpl);
        }
    }

    @Test
    public void deleteItem() throws SQLException {
        if(isDB == true) {
            final String deleteSql = "DELETE FROM cart WHERE cart_id=?; ";

            System.out.println("Delete cart");

            QueryRunner queryRunner = new QueryRunner();
            int deletedRowsCount = queryRunner.update(
                    connection,
                    deleteSql,
                    4
            );

            assertEquals(deletedRowsCount, 1, "Check that row deleted");

            printAllCarts();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/cart_list_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/cart_list.xsd");
            CartDaoImpl cartDaoImpl = (CartDaoImpl) XmlParser.deserialize(destFile, schemaFile, CartDaoImpl.class);

            Cart cart = new Cart();
            cart.setCart_id(4);
            cart.setPerson("5");
            cartDaoImpl.deleteCart(cart);
            assertEquals(cartDaoImpl.getAllCarts().size(), 3);
            XmlParser.serialize(cartDaoImpl, destFile);
            Cart deletedCart = cartDaoImpl.getAllCarts()
                    .stream()
                    .filter(e -> e.equals(cart))
                    .findAny()
                    .orElse(null);
            assertNotEquals(deletedCart, cart);
            System.out.println(cartDaoImpl);
        }
    }

    @Test
    public void updateItem() throws SQLException {
        if(isDB == true) {
            final String updateSql = "UPDATE cart SET person=? WHERE cart_id=?; ";

            System.out.println("Update cart");
            QueryRunner queryRunner = new QueryRunner();
            int updatedRowsCount = queryRunner.update(
                    connection,
                    updateSql,
                    "5", 4
            );

            assertEquals(updatedRowsCount, 1, "Check that row updated");


            printAllCarts();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/cart_list_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/cart_list.xsd");
            CartDaoImpl cartDaoImpl = (CartDaoImpl) XmlParser.deserialize(destFile, schemaFile, CartDaoImpl.class);

            Cart cart = cartDaoImpl.getCart(4);
            cart.setPerson("5");
            cartDaoImpl.updateCart(cart);
            XmlParser.serialize(cartDaoImpl, destFile);
            Cart updatedCart = cartDaoImpl.getAllCarts()
                    .stream()
                    .filter(e -> e.equals(cart))
                    .findAny()
                    .orElse(null);
            assertEquals(updatedCart, cart);
            System.out.println(cartDaoImpl);
        }
    }

    private void printAllCarts() throws SQLException {
        final String sql = "SELECT * FROM cart;";
        ResultSetHandler<List<Cart>> handler = new BeanListHandler<>(Cart.class);

        QueryRunner queryRunner = new QueryRunner();
        List<Cart> cartList = queryRunner.query(connection, sql, handler);

        System.out.println(cartList);
    }
}
