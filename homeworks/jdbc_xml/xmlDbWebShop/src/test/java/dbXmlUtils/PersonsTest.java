package dbXmlUtils;

import base.BaseTest;
import dao.impl.PersonDaoImpl;
import models.Person;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;
import parsers.XmlParser;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class PersonsTest extends BaseTest {

    public static final boolean isDB = false;

    @Test
    public void addNewPerson() throws SQLException {
        if(isDB == true) {
            final String insertSql = "INSERT INTO persons (first_name , last_name, credentials) VALUES (?, ?, ?);";

            Person person = new Person("Антоний", "Тизенгауз", "1");

            QueryRunner queryRunner = new QueryRunner();
            int insertedRowsCount = queryRunner.update(
                    connection,
                    insertSql,
                    person.getFirst_name(), person.getLast_name(), person.getCredentials()
            );

            assertEquals(insertedRowsCount, 1, "Check that row inserted");

            printAllPersons();
        } else{
            File sourceFile = new File(System.getProperty("user.dir") + "/xml/persons.xml");
            File destFile = new File(System.getProperty("user.dir") + "/xml/persons_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/persons.xsd");
            PersonDaoImpl personDaoImpl = (PersonDaoImpl) XmlParser.deserialize(sourceFile, schemaFile, PersonDaoImpl.class);
            Person person = new Person();
            person.setPerson_id(6);
            person.setFirst_name("Антоний");
            person.setLast_name("Тизенгауз");
            person.setCredentials("1");
            personDaoImpl.addPerson(person);
            XmlParser.serialize(personDaoImpl, destFile);
            assertEquals(person, personDaoImpl.getPerson(6));
            Person addedPerson = personDaoImpl.getAllPersons()
                    .stream()
                    .filter(e -> e.equals(person))
                    .findAny()
                    .orElse(null);
            assertEquals(addedPerson, person);
            System.out.println(personDaoImpl);
        }

    }

    @Test
    public void deletePerson() throws SQLException {
        if(isDB == true) {
            final String deleteSql = "DELETE FROM persons WHERE last_name=?; ";

            System.out.println("Delete person");

            QueryRunner queryRunner = new QueryRunner();
            int deletedRowsCount = queryRunner.update(
                    connection,
                    deleteSql,
                    "Тизенгауз"
            );

            assertEquals(deletedRowsCount, 1, "Check that row deleted");

            printAllPersons();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/persons_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/persons.xsd");
            PersonDaoImpl personDaoImpl = (PersonDaoImpl) XmlParser.deserialize(destFile, schemaFile, PersonDaoImpl.class);

            Person person = new Person();
            person.setPerson_id(6);
            person.setFirst_name("Франтишек");
            person.setLast_name("Тизенгауз");
            person.setCredentials("1");
            personDaoImpl.deletePerson(person);
            assertEquals(personDaoImpl.getAllPersons().size(), 5);
            XmlParser.serialize(personDaoImpl, destFile);
            Person deletedPerson = personDaoImpl.getAllPersons()
                    .stream()
                    .filter(e -> e.equals(person))
                    .findAny()
                    .orElse(null);
            assertNotEquals(deletedPerson, person);
            System.out.println(personDaoImpl);
        }

    }

    @Test
    public void updatePerson() throws SQLException {
        if(isDB == true) {
            final String updateSql = "UPDATE persons SET first_name=? WHERE last_name=?; ";

            System.out.println("Update person");
            QueryRunner queryRunner = new QueryRunner();
            int updatedRowsCount = queryRunner.update(
                    connection,
                    updateSql,
                    "Франтишек", "Тизенгауз"
            );

            assertEquals(updatedRowsCount, 1, "Check that row updated");


            printAllPersons();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/persons_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/persons.xsd");
            PersonDaoImpl personDaoImpl = (PersonDaoImpl) XmlParser.deserialize(destFile, schemaFile, PersonDaoImpl.class);

            Person person = personDaoImpl.getPerson(6);
            person.setFirst_name("Франтишек");
            personDaoImpl.updatePerson(person);
            XmlParser.serialize(personDaoImpl, destFile);
            Person updatedPerson = personDaoImpl.getAllPersons()
                    .stream()
                    .filter(e -> e.equals(person))
                    .findAny()
                    .orElse(null);
            assertEquals(updatedPerson, person);
            System.out.println(personDaoImpl);
        }
    }

    private void printAllPersons() throws SQLException {
        final String sql = "SELECT * FROM persons;";
        ResultSetHandler<List<Person>> handler = new BeanListHandler<>(Person.class);

        QueryRunner queryRunner = new QueryRunner();
        List<Person> personList = queryRunner.query(connection, sql, handler);

        System.out.println(personList);
    }
}
