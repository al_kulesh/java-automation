package dbXmlUtils;

import base.BaseTest;
import dao.impl.CredentialsDaoImpl;
import models.Credentials;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;
import parsers.XmlParser;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class CredentialsTest extends BaseTest {

    public static final boolean isDB = false;

    @Test
    public void addNewItem() throws SQLException {
        if (isDB == true) {

            final String insertSql = "INSERT INTO credentials (personsrole, login , password) VALUES (?, ?, ?);";

            Credentials credentials = new Credentials("ADMIN", "admin3", "4444");

            QueryRunner queryRunner = new QueryRunner();
            int insertedRowsCount = queryRunner.update(
                    connection,
                    insertSql,
                    credentials.getPersonsrole(), credentials.getLogin(), credentials.getPassword()
            );

            assertEquals(insertedRowsCount, 1, "Check that row inserted");

            printAllCredentials();
        } else {
            File sourceFile = new File(System.getProperty("user.dir") + "/xml/credentials_list.xml");
            File destFile = new File(System.getProperty("user.dir") + "/xml/credentials_list_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/credentials_list.xsd");
            CredentialsDaoImpl credentialsDaoImpl = (CredentialsDaoImpl) XmlParser.deserialize(sourceFile, schemaFile, CredentialsDaoImpl.class);
            Credentials credentials = new Credentials();
            credentials.setCredentials_id(6);
            credentials.setPersonsrole("ADMIN");
            credentials.setLogin("admin3");
            credentials.setPassword("4444");
            credentialsDaoImpl.addCredentials(credentials);
            XmlParser.serialize(credentialsDaoImpl, destFile);
            assertEquals(credentials, credentialsDaoImpl.getCredentials(6));
            Credentials addedCredentials = credentialsDaoImpl.getAllCredentials()
                    .stream()
                    .filter(e -> e.equals(credentials))
                    .findAny()
                    .orElse(null);
            assertEquals(addedCredentials, credentials);
            System.out.println(credentialsDaoImpl);
        }
    }

    @Test
    public void deleteItem() throws SQLException {
        if (isDB == true) {
            final String deleteSql = "DELETE FROM credentials WHERE login=?; ";

            System.out.println("Delete credentials");

            QueryRunner queryRunner = new QueryRunner();
            int deletedRowsCount = queryRunner.update(
                    connection,
                    deleteSql,
                    "admin3"
            );

            assertEquals(deletedRowsCount, 1, "Check that row deleted");

            printAllCredentials();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/credentials_list_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/credentials_list.xsd");
            CredentialsDaoImpl credentialsDaoImpl = (CredentialsDaoImpl) XmlParser.deserialize(destFile, schemaFile, CredentialsDaoImpl.class);

            Credentials credentials = new Credentials();
            credentials.setCredentials_id(6);
            credentials.setPersonsrole("ADMIN");
            credentials.setLogin("admin3");
            credentials.setPassword("5555");
            credentialsDaoImpl.deleteCredentials(credentials);
            assertEquals(credentialsDaoImpl.getAllCredentials().size(), 5);
            XmlParser.serialize(credentialsDaoImpl, destFile);
            Credentials deletedCredentials = credentialsDaoImpl.getAllCredentials()
                    .stream()
                    .filter(e -> e.equals(credentials))
                    .findAny()
                    .orElse(null);
            assertNotEquals(deletedCredentials, credentials);
            System.out.println(credentialsDaoImpl);
        }

    }

    @Test
    public void updateItem() throws SQLException {
        if (isDB == true) {
            final String updateSql = "UPDATE credentials SET password=? WHERE login=?; ";

            System.out.println("Update credentials");
            QueryRunner queryRunner = new QueryRunner();
            int updatedRowsCount = queryRunner.update(
                    connection,
                    updateSql,
                    "5555", "admin3"
            );

            assertEquals(updatedRowsCount, 1, "Check that row updated");


            printAllCredentials();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/credentials_list_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/credentials_list.xsd");
            CredentialsDaoImpl credentialsDaoImpl = (CredentialsDaoImpl) XmlParser.deserialize(destFile, schemaFile, CredentialsDaoImpl.class);

            Credentials credentials = credentialsDaoImpl.getCredentials(6);
            credentials.setPassword("5555");
            credentialsDaoImpl.updateCredentials(credentials);
            XmlParser.serialize(credentialsDaoImpl, destFile);
            Credentials updatedCredentials = credentialsDaoImpl.getAllCredentials()
                    .stream()
                    .filter(e -> e.equals(credentials))
                    .findAny()
                    .orElse(null);
            assertEquals(updatedCredentials, credentials);
            System.out.println(credentialsDaoImpl);
        }
    }

    private void printAllCredentials() throws SQLException {
        final String sql = "SELECT * FROM credentials;";
        ResultSetHandler<List<Credentials>> handler = new BeanListHandler<>(Credentials.class);

        QueryRunner queryRunner = new QueryRunner();
        List<Credentials> credenitialsList = queryRunner.query(connection, sql, handler);

        System.out.println(credenitialsList);
    }
}
