package dbXmlUtils;

import base.BaseTest;
import dao.impl.ProductTypeDaoImpl;
import models.ProductType;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;
import parsers.XmlParser;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class ProductTypeTest extends BaseTest {

    public static final boolean isDB = false;

    @Test
    public void addNewItem() throws SQLException {
        if(isDB == true) {
            final String insertSql = "INSERT INTO product_type (product_type_id, productType) VALUES (?, ?);";

            ProductType productType = new ProductType(4,"PC");

            QueryRunner queryRunner = new QueryRunner();
            int insertedRowsCount = queryRunner.update(
                    connection,
                    insertSql,
                    productType.getProduct_type_id(), productType.getProductType()
            );

            assertEquals(insertedRowsCount, 1, "Check that row inserted");

            printAllProductTypes();
        } else {
            File sourceFile = new File(System.getProperty("user.dir") + "/xml/product_type_list.xml");
            File destFile = new File(System.getProperty("user.dir") + "/xml/product_type_list_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/product_type_list.xsd");
            ProductTypeDaoImpl productTypeDaoImpl = (ProductTypeDaoImpl) XmlParser.deserialize(sourceFile, schemaFile, ProductTypeDaoImpl.class);
            ProductType productType = new ProductType();
            productType.setProduct_type_id(4);
            productType.setProductType("PC");
            productTypeDaoImpl.addProductType(productType);
            XmlParser.serialize(productTypeDaoImpl, destFile);
            assertEquals(productType, productTypeDaoImpl.getProductType(4));
            ProductType addedProductType = productTypeDaoImpl.getAllProductTypes()
                    .stream()
                    .filter(e -> e.equals(productType))
                    .findAny()
                    .orElse(null);
            assertEquals(addedProductType, productType);
            System.out.println(productTypeDaoImpl);
        }
    }

    @Test
    public void deleteItem() throws SQLException {
        if(isDB == true) {
            final String deleteSql = "DELETE FROM product_type WHERE productType=?; ";

            System.out.println("Delete product type");

            QueryRunner queryRunner = new QueryRunner();
            int deletedRowsCount = queryRunner.update(
                    connection,
                    deleteSql,
                    "Charger"
            );

            assertEquals(deletedRowsCount, 1, "Check that row deleted");

            printAllProductTypes();
        } else {
            File destFile = new File(System.getProperty("user.dir") + "/xml/product_type_list_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/product_type_list.xsd");
            ProductTypeDaoImpl productTypeDaoImpl = (ProductTypeDaoImpl) XmlParser.deserialize(destFile, schemaFile, ProductTypeDaoImpl.class);

            ProductType productType = new ProductType();
            productType.setProduct_type_id(4);
            productType.setProductType("Charger");
            productTypeDaoImpl.deleteProductType(productType);
            assertEquals(productTypeDaoImpl.getAllProductTypes().size(), 3);
            XmlParser.serialize(productTypeDaoImpl, destFile);
            ProductType deletedProductType = productTypeDaoImpl.getAllProductTypes()
                    .stream()
                    .filter(e -> e.equals(productType))
                    .findAny()
                    .orElse(null);
            assertNotEquals(deletedProductType, productType);
            System.out.println(productTypeDaoImpl);
        }
    }

    @Test
    public void updateItem() throws SQLException {
        if(isDB == true) {
            final String updateSql = "UPDATE product_type SET productType=? WHERE product_type_id=?; ";

            System.out.println("Update cart position");
            QueryRunner queryRunner = new QueryRunner();
            int updatedRowsCount = queryRunner.update(
                    connection,
                    updateSql,
                    "Charger", 4
            );

            assertEquals(updatedRowsCount, 1, "Check that row updated");


            printAllProductTypes();
        } else {
            File destFile = new File(System.getProperty("user.dir") + "/xml/product_type_list_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/product_type_list.xsd");
            ProductTypeDaoImpl productTypeDaoImpl = (ProductTypeDaoImpl) XmlParser.deserialize(destFile, schemaFile, ProductTypeDaoImpl.class);

            ProductType productType = productTypeDaoImpl.getProductType(4);
            productType.setProductType("Charger");
            productTypeDaoImpl.updateProductType(productType);
            XmlParser.serialize(productTypeDaoImpl, destFile);
            ProductType updatedProductType = productTypeDaoImpl.getAllProductTypes()
                    .stream()
                    .filter(e -> e.equals(productType))
                    .findAny()
                    .orElse(null);
            assertEquals(updatedProductType, productType);
            System.out.println(productTypeDaoImpl);
        }
    }

    private void printAllProductTypes() throws SQLException {
        final String sql = "SELECT * FROM product_type;";
        ResultSetHandler<List<ProductType>> handler = new BeanListHandler<>(ProductType.class);

        QueryRunner queryRunner = new QueryRunner();
        List<ProductType> productTypeList = queryRunner.query(connection, sql, handler);

        System.out.println(productTypeList);
    }
}
