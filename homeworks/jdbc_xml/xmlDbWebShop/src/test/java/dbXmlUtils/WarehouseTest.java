package dbXmlUtils;

import base.BaseTest;
import dao.impl.WarehouseDaoImpl;
import models.Warehouse;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;
import parsers.XmlParser;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class WarehouseTest extends BaseTest {

    public static final boolean isDB = false;

    @Test
    public void addNewItem() throws SQLException {
        if(isDB == true) {
            final String insertSql = "INSERT INTO warehouse (warehouse_id, name) VALUES (?, ?);";

            Warehouse warehouse = new Warehouse(2, "Second");

            QueryRunner queryRunner = new QueryRunner();
            int insertedRowsCount = queryRunner.update(
                    connection,
                    insertSql,
                    warehouse.getWarehouse_id(), warehouse.getName()
            );

            assertEquals(insertedRowsCount, 1, "Check that row inserted");

            printAllWarehouses();
        } else {
            File sourceFile = new File(System.getProperty("user.dir") + "/xml/warehouse_list.xml");
            File destFile = new File(System.getProperty("user.dir") + "/xml/warehouse_list_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/warehouse_list.xsd");
            WarehouseDaoImpl warehouseDaoImpl = (WarehouseDaoImpl) XmlParser.deserialize(sourceFile, schemaFile, WarehouseDaoImpl.class);
            Warehouse warehouse = new Warehouse();
            warehouse.setWarehouse_id(2);
            warehouse.setName("Second");
            warehouseDaoImpl.addWarehouse(warehouse);
            XmlParser.serialize(warehouseDaoImpl, destFile);
            assertEquals(warehouse, warehouseDaoImpl.getWarehouse(2));
            Warehouse addedWarehouse = warehouseDaoImpl.getAllWarehouses()
                    .stream()
                    .filter(e -> e.equals(warehouse))
                    .findAny()
                    .orElse(null);
            assertEquals(addedWarehouse, warehouse);
            System.out.println(warehouseDaoImpl);
        }
    }

    @Test
    public void deleteItem() throws SQLException {
        if(isDB == true) {
            final String deleteSql = "DELETE FROM warehouse WHERE name=?; ";

            System.out.println("Delete warehouse");

            QueryRunner queryRunner = new QueryRunner();
            int deletedRowsCount = queryRunner.update(
                    connection,
                    deleteSql,
                    "First"
            );

            assertEquals(deletedRowsCount, 1, "Check that row deleted");

            printAllWarehouses();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/warehouse_list_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/warehouse_list.xsd");
            WarehouseDaoImpl warehouseDaoImpl = (WarehouseDaoImpl) XmlParser.deserialize(destFile, schemaFile, WarehouseDaoImpl.class);

            Warehouse warehouse = new Warehouse();
            warehouse.setWarehouse_id(2);
            warehouse.setName("First");
            warehouseDaoImpl.deleteWarehouse(warehouse);
            assertEquals(warehouseDaoImpl.getAllWarehouses().size(), 1);
            XmlParser.serialize(warehouseDaoImpl, destFile);
            Warehouse deletedWarehouse = warehouseDaoImpl.getAllWarehouses()
                    .stream()
                    .filter(e -> e.equals(warehouse))
                    .findAny()
                    .orElse(null);
            assertNotEquals(deletedWarehouse, warehouse);
            System.out.println(warehouseDaoImpl);
        }
    }

    @Test
    public void updateItem() throws SQLException {
        if(isDB == true) {
            final String updateSql = "UPDATE warehouse SET name=? WHERE warehouse_id=?; ";

            System.out.println("Update warehouse");
            QueryRunner queryRunner = new QueryRunner();
            int updatedRowsCount = queryRunner.update(
                    connection,
                    updateSql,
                    "First", 2
            );

            assertEquals(updatedRowsCount, 1, "Check that row updated");


            printAllWarehouses();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/warehouse_list_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/warehouse_list.xsd");
            WarehouseDaoImpl warehouseDaoImpl = (WarehouseDaoImpl) XmlParser.deserialize(destFile, schemaFile, WarehouseDaoImpl.class);

            Warehouse warehouse = warehouseDaoImpl.getWarehouse(2);
            warehouse.setName("First");
            warehouseDaoImpl.updateWarehouse(warehouse);
            XmlParser.serialize(warehouseDaoImpl, destFile);
            Warehouse updatedWarehouse = warehouseDaoImpl.getAllWarehouses()
                    .stream()
                    .filter(e -> e.equals(warehouse))
                    .findAny()
                    .orElse(null);
            assertEquals(updatedWarehouse, warehouse);
            System.out.println(warehouseDaoImpl);
        }
    }

    private void printAllWarehouses() throws SQLException {
        final String sql = "SELECT * FROM warehouse;";
        ResultSetHandler<List<Warehouse>> handler = new BeanListHandler<>(Warehouse.class);

        QueryRunner queryRunner = new QueryRunner();
        List<Warehouse> warehouses = queryRunner.query(connection, sql, handler);

        System.out.println(warehouses);
    }
}
