package dbXmlUtils;

import base.BaseTest;
import dao.impl.WarehouseProductDaoImpl;
import models.WarehouseProduct;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;
import parsers.XmlParser;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class WarehouseProductTest extends BaseTest {

    public static final boolean isDB = false;

    @Test
    public void addNewItem() throws SQLException {
        if(isDB == true) {
            final String insertSql = "INSERT INTO warehouse_product (warehouse_product_id, product_quantity, product, warehouse) VALUES (?, ?, ?, ?);";

            WarehouseProduct warehouseProduct = new WarehouseProduct(9, "5", "9", "1");

            QueryRunner queryRunner = new QueryRunner();
            int insertedRowsCount = queryRunner.update(
                    connection,
                    insertSql,
                    warehouseProduct.getWarehouse_product_id(), warehouseProduct.getProduct_quantity(), warehouseProduct.getProduct(), warehouseProduct.getWarehouse()
            );

            assertEquals(insertedRowsCount, 1, "Check that row inserted");

            printAllWarehousePositions();
        } else {
            File sourceFile = new File(System.getProperty("user.dir") + "/xml/warehouse_product_list.xml");
            File destFile = new File(System.getProperty("user.dir") + "/xml/warehouse_product_list_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/warehouse_product_list.xsd");
            WarehouseProductDaoImpl warehouseProductDaoImpl = (WarehouseProductDaoImpl) XmlParser.deserialize(sourceFile, schemaFile, WarehouseProductDaoImpl.class);
            WarehouseProduct warehouseProduct = new WarehouseProduct();
            warehouseProduct.setWarehouse_product_id(9);
            warehouseProduct.setProduct_quantity("5");
            warehouseProduct.setProduct("9");
            warehouseProduct.setWarehouse("1");
            warehouseProductDaoImpl.addWarehouseProduct(warehouseProduct);
            XmlParser.serialize(warehouseProductDaoImpl, destFile);
            assertEquals(warehouseProduct, warehouseProductDaoImpl.getWarehouseProduct(9));
            WarehouseProduct addedWarehouseProduct = warehouseProductDaoImpl.getAllWarehouseProducts()
                    .stream()
                    .filter(e -> e.equals(warehouseProduct))
                    .findAny()
                    .orElse(null);
            assertEquals(addedWarehouseProduct, warehouseProduct);
            System.out.println(warehouseProductDaoImpl);
        }
    }

    @Test
    public void deleteItem() throws SQLException {
        if(isDB == true) {
            final String deleteSql = "DELETE FROM warehouse_product WHERE product=?; ";

            System.out.println("Delete warehouse position");

            QueryRunner queryRunner = new QueryRunner();
            int deletedRowsCount = queryRunner.update(
                    connection,
                    deleteSql,
                    "9"
            );

            assertEquals(deletedRowsCount, 1, "Check that row deleted");

            printAllWarehousePositions();
        } else {
            File destFile = new File(System.getProperty("user.dir") + "/xml/warehouse_product_list_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/warehouse_product_list.xsd");
            WarehouseProductDaoImpl warehouseProductDaoImpl = (WarehouseProductDaoImpl) XmlParser.deserialize(destFile, schemaFile, WarehouseProductDaoImpl.class);

            WarehouseProduct warehouseProduct = new WarehouseProduct();
            warehouseProduct.setWarehouse_product_id(9);
            warehouseProduct.setProduct_quantity("10");
            warehouseProduct.setProduct("9");
            warehouseProduct.setWarehouse("1");
            warehouseProductDaoImpl.deleteWarehouseProduct(warehouseProduct);
            assertEquals(warehouseProductDaoImpl.getAllWarehouseProducts().size(), 8);
            XmlParser.serialize(warehouseProductDaoImpl, destFile);
            WarehouseProduct deletedWarehouseProduct = warehouseProductDaoImpl.getAllWarehouseProducts()
                    .stream()
                    .filter(e -> e.equals(warehouseProduct))
                    .findAny()
                    .orElse(null);
            assertNotEquals(deletedWarehouseProduct, warehouseProduct);
            System.out.println(warehouseProductDaoImpl);
        }
    }

    @Test
    public void updateItem() throws SQLException {
        if(isDB == true) {
            final String updateSql = "UPDATE warehouse_product SET product_quantity=? WHERE product=?; ";

            System.out.println("Update warehouse position");
            QueryRunner queryRunner = new QueryRunner();
            int updatedRowsCount = queryRunner.update(
                    connection,
                    updateSql,
                    "10", "9"
            );

            assertEquals(updatedRowsCount, 1, "Check that row updated");


            printAllWarehousePositions();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/warehouse_product_list_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/warehouse_product_list.xsd");
            WarehouseProductDaoImpl warehouseProductDaoImpl = (WarehouseProductDaoImpl) XmlParser.deserialize(destFile, schemaFile, WarehouseProductDaoImpl.class);

            WarehouseProduct warehouseProduct = warehouseProductDaoImpl.getWarehouseProduct(9);
            warehouseProduct.setProduct_quantity("10");
            warehouseProductDaoImpl.updateWarehouseProduct(warehouseProduct);
            XmlParser.serialize(warehouseProductDaoImpl, destFile);
            WarehouseProduct updatedWarehouseProduct = warehouseProductDaoImpl.getAllWarehouseProducts()
                    .stream()
                    .filter(e -> e.equals(warehouseProduct))
                    .findAny()
                    .orElse(null);
            assertEquals(updatedWarehouseProduct, warehouseProduct);
            System.out.println(warehouseProductDaoImpl);
        }
    }

    private void printAllWarehousePositions() throws SQLException {
        final String sql = "SELECT * FROM warehouse_product;";
        ResultSetHandler<List<WarehouseProduct>> handler = new BeanListHandler<>(WarehouseProduct.class);

        QueryRunner queryRunner = new QueryRunner();
        List<WarehouseProduct> warehouseList = queryRunner.query(connection, sql, handler);

        System.out.println(warehouseList);
    }
}
