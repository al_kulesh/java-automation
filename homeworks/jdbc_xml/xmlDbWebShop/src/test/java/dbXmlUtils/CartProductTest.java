package dbXmlUtils;

import base.BaseTest;
import dao.impl.CartProductDaoImpl;
import models.CartProduct;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;
import parsers.XmlParser;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class CartProductTest extends BaseTest {

    public static final boolean isDB = false;

    @Test
    public void addNewItem() throws SQLException {
        if(isDB == true) {
            final String insertSql = "INSERT INTO cart_product (cart_product_id, quantity, product, warehouse_position, cart) VALUES (?, ?, ?, ?, ?);";

            CartProduct cartProduct = new CartProduct(1, "1", "3", "3", "1");

            QueryRunner queryRunner = new QueryRunner();
            int insertedRowsCount = queryRunner.update(
                    connection,
                    insertSql,
                    cartProduct.getCart_product_id(), cartProduct.getQuantity(), cartProduct.getProduct(), cartProduct.getWarehouse_position(), cartProduct.getCart()
            );

            assertEquals(insertedRowsCount, 1, "Check that row inserted");

            printAllCartPositions();
        } else {
            File sourceFile = new File(System.getProperty("user.dir") + "/xml/cart_product_list.xml");
            File destFile = new File(System.getProperty("user.dir") + "/xml/cart_product_list_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/cart_product_list.xsd");
            CartProductDaoImpl cartProductDaoImpl = (CartProductDaoImpl) XmlParser.deserialize(sourceFile, schemaFile, CartProductDaoImpl.class);
            CartProduct cartProduct = new CartProduct();
            cartProduct.setCart_product_id(1);
            cartProduct.setQuantity("1");
            cartProduct.setProduct("3");
            cartProduct.setWarehouse_position("3");
            cartProduct.setCart("1");
            cartProductDaoImpl.addCartProduct(cartProduct);
            XmlParser.serialize(cartProductDaoImpl, destFile);
            assertEquals(cartProduct, cartProductDaoImpl.getCartProduct(1));
            CartProduct addedCartProduct = cartProductDaoImpl.getAllCartProduct()
                    .stream()
                    .filter(e -> e.equals(cartProduct))
                    .findAny()
                    .orElse(null);
            assertEquals(addedCartProduct, cartProduct);
            System.out.println(cartProductDaoImpl);
        }
    }

    @Test
    public void deleteItem() throws SQLException {
        if(isDB == true) {
            final String deleteSql = "DELETE FROM cart_product WHERE product=?; ";

            System.out.println("Delete cart position");

            QueryRunner queryRunner = new QueryRunner();
            int deletedRowsCount = queryRunner.update(
                    connection,
                    deleteSql,
                    "3"
            );

            assertEquals(deletedRowsCount, 1, "Check that row deleted");

            printAllCartPositions();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/cart_product_list_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/cart_product_list.xsd");
            CartProductDaoImpl cartProductDaoImpl = (CartProductDaoImpl) XmlParser.deserialize(destFile, schemaFile, CartProductDaoImpl.class);

            CartProduct cartProduct = new CartProduct();
            cartProduct.setCart_product_id(1);
            cartProduct.setQuantity("2");
            cartProduct.setProduct("3");
            cartProduct.setWarehouse_position("3");
            cartProduct.setCart("1");
            cartProductDaoImpl.deleteCartProduct(cartProduct);
            assertEquals(cartProductDaoImpl.getAllCartProduct().size(), 0);
            XmlParser.serialize(cartProductDaoImpl, destFile);
            CartProduct deletedCartProduct = cartProductDaoImpl.getAllCartProduct()
                    .stream()
                    .filter(e -> e.equals(cartProduct))
                    .findAny()
                    .orElse(null);
            assertNotEquals(deletedCartProduct, cartProduct);
            System.out.println(cartProductDaoImpl);
        }
    }

    @Test
    public void updateItem() throws SQLException {
        if(isDB == true) {
            final String updateSql = "UPDATE cart_product SET quantity=? WHERE product=?; ";

            System.out.println("Update cart position");
            QueryRunner queryRunner = new QueryRunner();
            int updatedRowsCount = queryRunner.update(
                    connection,
                    updateSql,
                    "2", "3"
            );

            assertEquals(updatedRowsCount, 1, "Check that row updated");


            printAllCartPositions();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/cart_product_list_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/cart_product_list.xsd");
            CartProductDaoImpl cartProductDaoImpl = (CartProductDaoImpl) XmlParser.deserialize(destFile, schemaFile, CartProductDaoImpl.class);

            CartProduct cartProduct = cartProductDaoImpl.getCartProduct(1);
            cartProduct.setQuantity("2");
            cartProductDaoImpl.updateCartProduct(cartProduct);
            XmlParser.serialize(cartProductDaoImpl, destFile);
            CartProduct updatedCartProduct = cartProductDaoImpl.getAllCartProduct()
                    .stream()
                    .filter(e -> e.equals(cartProduct))
                    .findAny()
                    .orElse(null);
            assertEquals(updatedCartProduct, cartProduct);
            System.out.println(cartProductDaoImpl);
        }
    }

    private void printAllCartPositions() throws SQLException {
        final String sql = "SELECT * FROM cart_product;";
        ResultSetHandler<List<CartProduct>> handler = new BeanListHandler<>(CartProduct.class);

        QueryRunner queryRunner = new QueryRunner();
        List<CartProduct> cartProductsList = queryRunner.query(connection, sql, handler);

        System.out.println(cartProductsList);
    }
}
