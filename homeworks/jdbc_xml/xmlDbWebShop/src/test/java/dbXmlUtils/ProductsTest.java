package dbXmlUtils;


import base.BaseTest;
import dao.impl.ProductDaoImpl;
import models.Product;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.testng.annotations.Test;
import parsers.XmlParser;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;


public class ProductsTest extends BaseTest {

    public static final boolean isDB = false;

    @Test
    public void addNewItem() throws SQLException {
        if (isDB == true) {

            final String insertSql = "INSERT INTO products (product_id, name, specifications , productType) VALUES (?, ?, ?, ?);";

            Product product = new Product(10, "BlackBerry", "Expensive", "1");

            QueryRunner queryRunner = new QueryRunner();
            int insertedRowsCount = queryRunner.update(
                    connection,
                    insertSql,
                    product.getProduct_id(), product.getName(), product.getSpecifications(), product.getProductType()
            );

            assertEquals(insertedRowsCount, 1, "Check that row inserted");

            printAllProducts();

        } else {
            File sourceFile = new File(System.getProperty("user.dir") + "/xml/products.xml");
            File destFile = new File(System.getProperty("user.dir") + "/xml/products_dest.xml");
            File schemaFile = new File(System.getProperty("user.dir") + "/xml/products.xsd");
            ProductDaoImpl productDaoImpl = (ProductDaoImpl) XmlParser.deserialize(sourceFile, schemaFile, ProductDaoImpl.class);
            Product product = new Product();
            product.setProduct_id(10);
            product.setName("BlackBerry");
            product.setSpecifications("Expensive");
            product.setProductType("1");
            productDaoImpl.addProduct(product);
            XmlParser.serialize(productDaoImpl, destFile);
            assertEquals(product, productDaoImpl.getProduct(10));
            Product addedProduct = productDaoImpl.getAllProducts()
                    .stream()
                    .filter(e -> e.equals(product))
                    .findAny()
                    .orElse(null);
            assertEquals(addedProduct, product);
            System.out.println(productDaoImpl);
        }
    }

    @Test
    public void deleteItem() throws SQLException {
        if (isDB == true) {
            final String deleteSql = "DELETE FROM products WHERE specifications=?; ";

            System.out.println("Delete product");

            QueryRunner queryRunner = new QueryRunner();
            int deletedRowsCount = queryRunner.update(
                    connection,
                    deleteSql,
                    "Expensive"
            );

            assertEquals(deletedRowsCount, 1, "Check that row deleted");

            printAllProducts();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/products_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/products.xsd");
            ProductDaoImpl productDaoImpl = (ProductDaoImpl) XmlParser.deserialize(destFile, schemaFile, ProductDaoImpl.class);

            System.out.println(productDaoImpl.toString());
            Product product = new Product();
            product.setProduct_id(10);
            product.setName("Motorolla");
            product.setSpecifications("Expensive");
            product.setProductType("1");
            productDaoImpl.deleteProduct(product);
            assertEquals(productDaoImpl.getAllProducts().size(), 9);
            XmlParser.serialize(productDaoImpl, destFile);
            Product deletedProduct = productDaoImpl.getAllProducts()
                    .stream()
                    .filter(e -> e.equals(product))
                    .findAny()
                    .orElse(null);
            assertNotEquals(deletedProduct, product);
        }

    }

    @Test
    public void updateItem() throws SQLException {
        if (isDB == true) {
            final String updateSql = "UPDATE products SET name=? WHERE specifications=?; ";

            System.out.println("Update product");
            QueryRunner queryRunner = new QueryRunner();
            int updatedRowsCount = queryRunner.update(
                    connection,
                    updateSql,
                    "Motorolla", "Expensive"
            );

            assertEquals(updatedRowsCount, 1, "Check that row updated");


            printAllProducts();
        } else {
            final File destFile = new File(System.getProperty("user.dir") + "/xml/products_dest.xml");
            final File schemaFile = new File(System.getProperty("user.dir") + "/xml/products.xsd");
            ProductDaoImpl productDaoImpl = (ProductDaoImpl) XmlParser.deserialize(destFile, schemaFile, ProductDaoImpl.class);
            Product product = productDaoImpl.getProduct(10);
            product.setName("Motorolla");
            productDaoImpl.updateProduct(product);
            XmlParser.serialize(productDaoImpl, destFile);
            Product updatedProduct = productDaoImpl.getAllProducts()
                    .stream()
                    .filter(e -> e.equals(product))
                    .findAny()
                    .orElse(null);
            assertEquals(updatedProduct, product);
            System.out.println(productDaoImpl);
        }

    }

    private void printAllProducts() throws SQLException {
            final String sql = "SELECT * FROM products;";
            ResultSetHandler<List<Product>> handler = new BeanListHandler<>(Product.class);

            QueryRunner queryRunner = new QueryRunner();
            List<Product> productList = queryRunner.query(connection, sql, handler);

            System.out.println(productList);
    }


}
