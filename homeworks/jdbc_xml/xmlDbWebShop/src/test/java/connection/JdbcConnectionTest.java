package connection;

import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class JdbcConnectionTest {
    @Test
    public void test()  {
        Connection connection = null;
        final String projectDir = System.getProperty("user.dir");
        final String connectionString = "jdbc:sqlite:" + projectDir + "/db/shopApp.db";
        try {
            connection = DriverManager.getConnection(connectionString);
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException throwables){
                    throwables.printStackTrace();
                }
            }
        }
    }
}
