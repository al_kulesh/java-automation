import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamAPIExam {
    public static void main(String[] args) {
        String[] arrayOfStrings = new String[]{"str1", "str2", "str12", "str3"};

        List<String> listOfStrings = Arrays.stream(arrayOfStrings)
                .filter(x -> x.contains("1"))
                .collect(Collectors.toList());

        System.out.println(listOfStrings);
        IntStream.of(120, 410, 85, 43, 456)
                .filter(x -> x < 300)
                .map(x -> x + 11)
                .limit(3)
                .forEach(System.out::println);

    }
}
