public class Inheritance {
    protected int value = 0;

    public void setValue(int value){
        if(checkValue(value)){
            this.value = value;
        }
    }

    public int getValue(){
        return this.value;
    }

    protected boolean checkValue(int value){
        return value > 0;
    }
}
