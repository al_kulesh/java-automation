public class TheadLocal {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void main(String[] args){
        Runnable runnable = () -> {
            threadLocal.set(Thread.currentThread().getId());

            threadLocal.get();
        };
    }
}
