public class Polymorphism {
    public static class Flammable{
        public void burn(){
            System.out.println("Burn");
        }
    }
    public static class FlammableA extends Flammable{

    }
    public static class FlammableB extends Flammable{

    }
    public static class FlammableC extends Flammable{

    }

    public static void main(String[] args) {
        FlammableA a = new FlammableA();
        FlammableB b = new FlammableB();
        FlammableC c = new FlammableC();

        a.burn();
        b.burn();
        c.burn();

        someFunction(a);
        someFunction(b);
        someFunction(c);
    }
    public static void someFunction(Flammable flammable){
        flammable.burn();
    }
}
